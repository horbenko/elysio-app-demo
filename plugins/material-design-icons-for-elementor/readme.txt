﻿=== Material Design Icons for Elementor ===
Contributors: photon2020
Donate link: https://www.paypal.me/olenabartoshchak
Tags: elementor, material design icons, elementor icons
Requires at least: 4.7
Tested up to: 5.4
Requires PHP: 5.6
Stable tag: 1.1.0
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html

Material Design Icons for Elementor - adds Google Material Design Icons into Elementor Icons control

== Description ==
Material Design Icons for Elementor - adds Google Material Design Icons into Elementor Icons control

== Donate ==
Enjoy using *Material Design Icons for Elementor*? [**Please consider making a donation**](https://www.paypal.me/olenabartoshchak) to support the project's continued development.

== Credits ==
* Icon font: Material Design icons https://design.google.com/icons/, (C) Google, [Apache 2.0](http://www.apache.org/licenses/)

== Changelog ==

= 1.1.0 =
* Added the Outlined icon style
* Updated icons library

= 1.0.1 =
* Tested on WP 5.4

= 1.0.0 =
* Initial Public Release