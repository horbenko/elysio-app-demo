<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


function elysio_custom_widgets_init(){
  register_widget( 'Elysio_Recent_Posts_Widget' );
  register_widget( 'Elysio_Socials_Links_Widget' );
}
