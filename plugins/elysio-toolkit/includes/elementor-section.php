<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

function elysio_add_elementor_widget_categories( $elements_manager ) {
    $elements_manager->add_category(
        'elysio-elements',
        [
            'title' => __( 'Elysio Elements', 'elysio-toolkit' ),
            'icon' => 'font',
        ]
    );
}
add_action( 'elementor/elements/categories_registered', 'elysio_add_elementor_widget_categories' );