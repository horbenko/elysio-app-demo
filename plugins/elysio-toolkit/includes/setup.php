<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


function elysio_button_colors(){
	return [
		//'default'  => __( 'Default', 'elysio' ),
		'primary'  => __( 'Primary', 'elysio' ),
		'secondary' => __( 'Secondary', 'elysio' ),
		'blue' => __( 'Blue Gradient', 'elysio' ),
		'green' => __( 'Green Gradient', 'elysio' ),
		'purple' => __( 'Purple Gradient', 'elysio' ),
		'white' => __( 'White', 'elysio' ),
		'custom' => __( 'Custom', 'elysio' ),
	];
}



if ( ! function_exists( 'elysio_elements' ) ) {
  function elysio_elements()
  {
    require_once ELYSIO_PATH . 'elements/button.php';
    require_once ELYSIO_PATH . 'elements/buttons-two.php';
    require_once ELYSIO_PATH . 'elements/image-gallery.php';
    require_once ELYSIO_PATH . 'elements/showcase-image.php';
    require_once ELYSIO_PATH . 'elements/showcase-toggle.php';
    require_once ELYSIO_PATH . 'elements/pricing-table.php';
    require_once ELYSIO_PATH . 'elements/contact-form.php';
    require_once ELYSIO_PATH . 'elements/google-map.php';
  }
}

// Include Custom Widgets
add_filter( 'elementor/widgets/widgets_registered', 'elysio_elements' );


require_once ELYSIO_PATH . 'includes/elementor-section.php';




// When plugin is installed for the first time, set global elementor settings.

if ( ! function_exists( 'elysio_so_widgets_bundle_setup_elementor_settings' ) ) {
    function elysio_so_widgets_bundle_setup_elementor_settings()
    {
        // // Disable color schemes
        $elementor_disable_color_schemes = get_option('elementor_disable_color_schemes');
        if (empty($elementor_disable_color_schemes)) {
            update_option('elementor_disable_color_schemes', 'yes');
        }
        // Disable typography schemes
        $elementor_disable_typography_schemes = get_option('elementor_disable_typography_schemes');
        if (empty($elementor_disable_typography_schemes)) {
            update_option('elementor_disable_typography_schemes', 'yes');
        }
        // Disable global lightbox by default.
        update_option('elementor_global_image_lightbox', '');
        // Check for our custom post type, if it's not included, include it.
        $elementor_cpt_support = get_option('elementor_cpt_support');
        if (empty($elementor_cpt_support)) {
            $elementor_cpt_support = array();
        }
        if (!in_array("page", $elementor_cpt_support)) {
            array_push($elementor_cpt_support,"page");
            update_option('elementor_cpt_support', $elementor_cpt_support);
        }
        if (!in_array("post", $elementor_cpt_support)) {
            array_push($elementor_cpt_support,"post");
            update_option('elementor_cpt_support', $elementor_cpt_support);
        }
    }
}


// Enable SVG
// function cc_mime_types($mimes) {
//   $mimes['svg'] = 'image/svg+xml';
//   return $mimes;
// }
// add_filter('upload_mimes', 'cc_mime_types');


// function fix_svg_thumb_display() {
//   echo '
//     td.media-icon img[src$=".svg"], img[src$=".svg"].attachment-post-thumbnail { 
//       width: 100% !important; 
//       height: auto !important; 
//     }
//   ';
// }
// add_action('admin_head', 'fix_svg_thumb_display');
// // #END Enable SVG 