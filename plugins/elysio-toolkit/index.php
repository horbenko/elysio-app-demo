<?php
/*
 * Plugin Name: Elysio Toolkit
 * Description: Toolkits for Elysio Theme. A widget pack for the Elementor Page Builder and more.
 * Version: 1.1
 * Plugin URI: elysio.top
 * Author: Elysio Team
 * Author URI: elysio.top
 * Text Domain: elysio-toolkit
 * Domain Path: /languages
 * License: GPL v3
 */

//if( !function_exists( 'add_action' ) ){
  //die( "Hi there! I'm just a plugin, not much I can do when called directly." );
//}


// Setup
define( 'ELYSIO_VERSION', '1.0' );
define( 'ELYSIO__FILE__', __FILE__ );
define( 'ELYSIO_PLUGIN_BASE', plugin_basename( ELYSIO__FILE__ ) );
define( 'ELYSIO_URL', plugins_url( '/', ELYSIO__FILE__ ) );
define( 'ELYSIO_PATH', plugin_dir_path( ELYSIO__FILE__ ) );
define( 'ELYSIO_ASSETS_URL', ELYSIO_URL . 'assets/' );

define( 'ELYSIO_PLUGIN_URL', __FILE__ );


// Elementor
if(defined('ELEMENTOR_PATH')){
// Run Setup
    require_once ELYSIO_PATH . 'includes/setup.php';
}


// Includes
include( ELYSIO_PATH . 'includes/activate.php' );
include( ELYSIO_PATH . 'includes/widgets.php' );
include( ELYSIO_PATH . 'widgets/recent-posts.php' );
include( ELYSIO_PATH . 'widgets/socials-links.php' );


// Hooks
register_activation_hook( __FILE__, 'elysio_activate_plugin' );
add_action( 'widgets_init', 'elysio_custom_widgets_init' );