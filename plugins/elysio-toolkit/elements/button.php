<?php

/**
 * Elysio Button
 * @author: Artem Horebenko
 * @version: 1.0
 *
 */

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Elysio_Widget_Button extends Widget_Base {

	public function get_name() { return 'elysio-button'; }
	public function get_title() { return __( 'Elysio Button', 'elysio-toolkit' ); }
	public function get_icon() { return 'eicon-button'; }
	public function get_categories() { return [ 'elysio-elements' ]; }

	protected function _register_controls() {

		/* Content */
		$this->start_controls_section(
			'button_content_section',
			[
				'label' => __( 'Button Content', 'elysio-toolkit' ),
			]
		);

		$this->add_control(
			'button_title',
			[
				'label' => __( 'Title', 'elysio-toolkit' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Default title', 'elysio-toolkit' ),
				'placeholder' => __( 'Type button title here', 'elysio-toolkit' ),
			]
		);

		$this->add_control(
			'button_link',
			[
				'label' => __( 'Link', 'elysio-toolkit' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'elysio-toolkit' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);

		$this->add_control(
			'button_alignment',
			[
				'label' => __( 'Alignment', 'elysio-toolkit' ),
				'type' => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'elysio-toolkit' ),
						'icon' => 'eicon-text-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'elysio-toolkit' ),
						'icon' => 'eicon-text-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'elysio-toolkit' ),
						'icon' => 'eicon-text-align-right',
					],
					'justify' => [
						'title' => __( 'Justified', 'elysio-toolkit' ),
						'icon' => 'eicon-text-align-justify',
					],
				],
				'selectors' => [
					'{{WRAPPER}}' => '{{VALUE}}',
				],
				'selectors_dictionary' => [
					'left' => 'text-align: left',
					'center' => 'text-align: center',
					'right' => 'text-align: right',
					'justify' => 'text-align: center',
				],
				'default' => 'left',
			]
		);

		/*$this->add_control(
			'button_size',
			[
				'label' => __( 'Size', 'elysio-toolkit' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'default',
				'options' => [
					'default'  => __( 'Extra Small', 'elysio-toolkit' ),
					'solid'  => __( 'Small', 'elysio-toolkit' ),
					'dashed' => __( 'Medium', 'elysio-toolkit' ),
					'dotted' => __( 'Large', 'elysio-toolkit' ),
					'double' => __( 'Extra Large', 'elysio-toolkit' ),
				],
			]
		);*/

		$this->add_control(
			'button_icon',
			[
				'label' => __( 'Icon', 'text-domain' ),
				'type' => \Elementor\Controls_Manager::ICONS,
				'default' => [
					'value' => 'fas fa-star',
					'library' => 'solid',
				],
			]
		);

		$this->add_control(
			'button_icon_position',
			[
				'label' => __( 'Icon Position', 'elysio-toolkit' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'before',
				'options' => [
					'before'  => __( 'Before', 'elysio-toolkit' ),
					'after' => __( 'After', 'elysio-toolkit' ),
				],
			]
		);

		$this->add_control(
			'button_icon_spacing',
			[
				'label' => __( 'Icon Spacing', 'elysio-toolkit' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 8,
				],
				'selectors' => [
					'{{WRAPPER}} .button-icon-before' => 'margin-right: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .button-icon-after' => 'margin-left: {{SIZE}}{{UNIT}};',
				],
			]
		);
 
		$this->add_control(
			'button_id_divider',
			[
				'type' => \Elementor\Controls_Manager::DIVIDER,
			]
		);

		$this->add_control(
			'button_id',
			[
				'label' => __( 'Button ID', 'elysio-toolkit' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'description'	=> __( 'Please make sure the ID is unique and not used elsewhere on the page this form is displayed. This field allows A-z 0-9 & underscore chars without spaces.', 'elysio-toolkit' ),
			]
		);

		$this->end_controls_section();




		$this->start_controls_section(
			'button_style_section',
			[
				'label' => __( 'Button Style', 'elysio-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'button_typography',
				'label' => __( 'Typography', 'elysio-toolkit' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .elysio-button',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'button_text_shadow',
				'label' => __( 'Text Shadow', 'elysio-toolkit' ),
				'selector' => '{{WRAPPER}} .elysio-button',
			]
		);



		$this->add_control(
			'button_color_scheme',
			[
				'label' => __( 'Color Scheme', 'elysio-toolkit' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'custom',
				'options' => elysio_button_colors(),
			]
		);

		$this->start_controls_tabs(
			'style_tabs'
		);

		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => __( 'Normal', 'plugin-name' ),
				'condition' => [ 'button_color_scheme' => 'custom' ],
			]
		);

		$this->add_control(
			'normal_color',
			[
				'label' => __( 'Text Color', 'elysio-toolkit' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .elysio-button' => 'color: {{VALUE}}',
				],
				'condition' => [ 'button_color_scheme' => 'custom' ],
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Background::get_type(),
			[
				'name' => 'normal_background',
				'label' => __( 'Background', 'elysio-toolkit' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .elysio-button',
				'condition' => [ 'button_color_scheme' => 'custom' ],
			]
		);

		$this->add_control(
			'normal_border_color',
			[
				'label' => __( 'Border Color', 'elysio-toolkit' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .elysio-button' => 'border-color: {{VALUE}}',
				],
				'condition' => [ 'button_color_scheme' => 'custom' ],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => __( 'Hover', 'plugin-name' ),
				'condition' => [ 'button_color_scheme' => 'custom' ],
			]
		);

		$this->add_control(
			'hover_color',
			[
				'label' => __( 'Text Color', 'elysio-toolkit' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .elysio-button:hover' => 'color: {{VALUE}}',
				],
				'condition' => [ 'button_color_scheme' => 'custom' ],
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Background::get_type(),
			[
				'name' => 'hover_background',
				'label' => __( 'Background', 'elysio-toolkit' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .elysio-button:hover',
				'condition' => [ 'button_color_scheme' => 'custom' ],
			]
		);

		$this->add_control(
			'hover_border_color',
			[
				'label' => __( 'Border Color', 'elysio-toolkit' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .elysio-button:hover' => 'border-color: {{VALUE}}',
				],
				'condition' => [ 'button_color_scheme' => 'custom' ],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->add_control(
			'button_border_diveder',
			[
				'type' => \Elementor\Controls_Manager::DIVIDER,
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Border::get_type(),
			[
				'name' => 'button_border',
				'label' => __( 'Border', 'elysio-toolkit' ),
				'selector' => '{{WRAPPER}} .elysio-button',
			]
		);

		$this->add_responsive_control(
			'button_botder_radius',
			[
				'label' => __( 'Border Radius', 'elysio-toolkit' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .elysio-button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'button_box_shadow',
				'label' => __( 'Box Shadow', 'elysio-toolkit' ),
				'selector' => '{{WRAPPER}} .elysio-button',
			]
		);

		$this->add_control(
			'button_padding_diveder',
			[
				'type' => \Elementor\Controls_Manager::DIVIDER,
			]
		);

		$this->add_responsive_control(
			'button_padding',
			[
				'label' => __( 'Padding', 'elysio-toolkit' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .elysio-button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();



	}

	protected function render() {
		$settings = $this->get_settings();
	
		$target = $settings['button_link']['is_external'] ? ' target="_blank"' : '';
		$nofollow = $settings['button_link']['nofollow'] ? ' rel="nofollow"' : '';

		$elysio_button_classes = 'elysio-button';
		if( $settings['button_alignment'] == 'justify' ){
			$elysio_button_classes .= ' w-100';
		}
		$elysio_button_classes .= ' button-color-scheme-' . $settings['button_color_scheme'];

		echo '<a class="' . $elysio_button_classes . '" href="' . $settings['button_link']['url'] . '"' . $target . $nofollow . '>';

		if( $settings['button_icon_position'] == 'before' ){
			?>
			<span class="button-icon button-icon-before"><?php \Elementor\Icons_Manager::render_icon( $settings['button_icon'], [ 'aria-hidden' => 'true' ] ); ?></span>
			<?php
			echo $settings['button_title'];
		}
		if( $settings['button_icon_position'] == 'after' ){
			echo $settings['button_title'];
			?>
			<span class="button-icon button-icon-after"><?php \Elementor\Icons_Manager::render_icon( $settings['button_icon'], [ 'aria-hidden' => 'true' ] ); ?></span>
			<?php
		}
		echo '</a>';

	}
	protected function _content_template() {}
}
Plugin::instance()->widgets_manager->register_widget_type( new Elysio_Widget_Button() );
