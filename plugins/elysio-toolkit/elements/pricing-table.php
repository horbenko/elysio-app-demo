<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Elysio_Widget_Pricing_Table extends Widget_Base {

	public function get_name() {
		return 'elysio-pricing';
	}

	public function get_title() {
		return __( 'Pricing', 'elysio-widget-pack' );
	}

	public function get_icon() {
		return 'eicon-price-table';
	}

	public function get_categories() {
		return [ 'elysio-elements' ];
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_pricing',
			[
				'label' => __( 'Pricing Table', 'elysio-toolkit' ),
			]
		);


		$this->add_control(
			'pricing',
			[
				'label' => __( 'Pricing Table', 'elysio-toolkit' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => [
					[
						'name' => 'price_col_title',
						'label' => __( 'Title', 'elysio-toolkit' ),
						'type' => Controls_Manager::TEXT,
						'placeholder' => __( 'Price 1', 'elysio-toolkit' ),
						'label_block' => true,
						'default' => __( 'Title' , 'elysio-toolkit' ),
					],
					[
						'name' => 'price_col_sub_title',
						'label' => __( 'Sub Title', 'elysio-toolkit' ),
						'type' => Controls_Manager::TEXT,
						'placeholder' => __( 'Sub Title', 'elysio-toolkit' ),
						'label_block' => true,
					],
					[
						'name' => 'price_col_price',
						'label' => __( 'Price', 'elysio-toolkit' ),
						'type' => Controls_Manager::TEXT,
						'default' => __( '$99', 'elysio-toolkit' ),
						'placeholder' => __( '$99', 'elysio-toolkit' ),
						'label_block' => true,
					],
					[
						'name' => 'price_col_text',
						'label' => __( 'Price text', 'elysio-toolkit' ),
						'type' => Controls_Manager::TEXT,
						'placeholder' => __( '/each', 'elysio-toolkit' ),
						'label_block' => true,
					],
					[
						'name' => 'price_col_description',
						'label' => __( 'Description', 'elysio-toolkit' ),
						'type' => Controls_Manager::TEXTAREA,
						'placeholder' => __( "Maecenas tristique\nUllamcorper mauris\nElementum tortor\nClass aptent", 'elysio-toolkit' ),
						'label_block' => true,
						'default' => __( "Maecenas tristique\nUllamcorper mauris\nElementum tortor\nClass aptent", 'elysio-toolkit' ),
					],
					[
						'name' => 'price_col_button_1_show',
						'label' => __( 'Button', 'elysio-toolkit' ),
						'type' => Controls_Manager::SWITCHER,
						'label_on' => __( 'Show', 'elysio-toolkit' ),
						'label_off' => __( 'Hide', 'elysio-toolkit' ),
						'return_value' => 'yes',
						'separator' => 'before',
						'default' => 'yes'
					],
					[
						'name' => 'price_col_button_1_text',
						'label' => __( 'Button Text', 'elysio-toolkit' ),
						'type' => Controls_Manager::TEXT,
						'placeholder' => __( 'BUTTON TEXT', 'elysio-toolkit' ),
						'default' => __( 'BUTTON TEXT', 'elysio-toolkit' ),
						'conditions' => [
							'terms' => [
								[
									'name' => 'price_col_button_1_show',
									'operator' => '==',
									'value' => 'yes',
								],
							],
						],
					],
					[
						'name' => 'price_col_button_1_link',
						'label' => __( 'Button Link', 'elysio-toolkit' ),
						'type' => Controls_Manager::URL,
						'placeholder' => __( 'http://your-link.com', 'elysio-toolkit' ),
						//'default' => __( 'http://your-link.com', 'elysio-toolkit' ),
						'conditions' => [
							'terms' => [
								[
									'name' => 'price_col_button_1_show',
									'operator' => '==',
									'value' => 'yes',
								],
							],
						],
					],
					/*[
						'name' => 'price_col_button_2_show',
						'label' => __( 'Button 2', 'elysio-toolkit' ),
						'type' => Controls_Manager::SWITCHER,
						'label_on' => __( 'Yes', 'elysio-toolkit' ),
						'label_off' => __( 'No', 'elysio-toolkit' ),
						'return_value' => 'yes',
						//'default' => '',
						'separator' => 'before',
					],
					[
						'name' => 'price_col_button_2_text',
						'label' => __( 'Button 2 Text', 'elysio-toolkit' ),
						'type' => Controls_Manager::TEXT,
						//'default' => __( 'Click Here', 'elysio-toolkit' ),
						'conditions' => [
							'terms' => [
								[
									'name' => 'price_col_button_2_show',
									'operator' => '==',
									'value' => 'yes',
								],
							],
						],
					],
					[
						'name' => 'price_col_button_2_link',
						'label' => __( 'Button 2 Link', 'elysio-toolkit' ),
						'type' => Controls_Manager::URL,
						'placeholder' => __( 'http://your-link.com', 'elysio-toolkit' ),
						'conditions' => [
							'terms' => [
								[
									'name' => 'price_col_button_2_show',
									'operator' => '==',
									'value' => 'yes',
								],
							],
						],
					],*/
					[
						'name' => 'price_col_featured',
						'label' => __( 'Featured', 'elysio-toolkit' ),
						'type' => Controls_Manager::SWITCHER,
						'label_on' => __( 'Yes', 'elysio-toolkit' ),
						'label_off' => __( 'No', 'elysio-toolkit' ),
						'return_value' => 'yes',
						//'default' => '',
						'separator' => 'before',
					],
					/*[
						'name' => 'price_col_background',
						'label' => __( 'Background Color', 'elysio-toolkit' ),
						'type' => Controls_Manager::COLOR,
						//'default' => '#FFF',
						'selectors' => [
							'{{WRAPPER}} {{CURRENT_ITEM}}' => 'background-color: {{VALUE}}',
						],
					],*/
				],
				'default' => [
					[
						'price_col_title' => __( 'Basic', 'elysio-toolkit' ),
						'price_col_price' => __( '$29.99', 'elysio-toolkit' ),
						'price_col_description' => __( "3D Interior Design\nCGIs\nAnimation\nVirtual Reality\nReal time", 'elysio-toolkit' ),
						'price_col_button_1_show' => __( 'yes', 'elysio-toolkit' ),
						'price_col_button_1_text' => __( 'Buy now', 'elysio-toolkit' ),
						'price_col_button_1_link' => __( '#', 'elysio-toolkit' ),
					],
					[
						'price_col_title' => __( 'Standart', 'elysio-toolkit' ),
						'price_col_price' => __( '$49.99', 'elysio-toolkit' ),
						'price_col_description' => __( "3D Interior Design\nCGIs\nAnimation\nVirtual Reality\nReal time", 'elysio-toolkit' ),
						'price_col_button_1_show' => __( 'yes', 'elysio-toolkit' ),
						'price_col_button_1_text' => __( 'Buy now', 'elysio-toolkit' ),
						'price_col_button_1_link' => __( '#', 'elysio-toolkit' ),
					],
					[
						'price_col_title' => __( 'Premium', 'elysio-toolkit' ),
						'price_col_price' => __( '$99.99', 'elysio-toolkit' ),
						'price_col_description' => __( "3D Interior Design\nCGIs\nAnimation\nVirtual Reality\nReal time", 'elysio-toolkit' ),
						'price_col_button_1_show' => __( 'yes', 'elysio-toolkit' ),
						'price_col_button_1_text' => __( 'Buy now', 'elysio-toolkit' ),
						'price_col_button_1_link' => __( '#', 'elysio-toolkit' ),
						'price_col_featured' => __( 'yes', 'elysio-toolkit' ),
					],

				],
				'title_field' => '{{{ price_col_title }}}',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_content',
			[
				'label' => __( 'Content', 'elysio-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		// $this->add_control(
		// 	'item_padding',
		// 	[
		// 		'label' => __( 'Card padding', 'plugin-domain' ),
		// 		'type' => Controls_Manager::DIMENSIONS,
		// 		'size_units' => [ 'px', '%', 'em' ],
		// 		'default'	=> [
		// 			'top' => '32',
		// 			'right' => '48',
		// 			'bottom' => '48',
		// 			'left' => '48',
		// 			'isLinked'	=> false,
		// 			'unit'	=> 'px'
		// 		],
		// 		'selectors' => [
		// 			'{{WRAPPER}} .elysio-pricing-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
		// 		],
		// 	]
		// );


		$this->add_group_control(
			\Elementor\Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'item_box_shadow',
				'label' => __( 'Box Shadow', 'plugin-domain' ),
				'selector' => '{{WRAPPER}} .elysio-pricing-wrap',
			]
		);

		$this->add_control(
			'item_background',
			[
				'label' => __( 'Card Background', 'elysio-toolkit' ),
				'type' => Controls_Manager::COLOR,
				'default' => 'transparent',
				'selectors' => [
					'{{WRAPPER}} .elysio-pricing-wrap' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'line_color',
			[
				'label' => __( 'Line Color', 'elysio-toolkit' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#d6d6d6',
				'selectors' => [
					'{{WRAPPER}} .elysio-pricing-content:before' => 'border-color: {{VALUE}};',
				],
			]
		);

		

		$this->add_control(
			'primary_color',
			[
				'label' => __( 'Primary Color', 'elysio-toolkit' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .elysio-pricing-title' => 'color: {{VALUE}};',
					'{{WRAPPER}} .elysio-pricing-cost' => 'color: {{VALUE}};',
					'{{WRAPPER}} .elysio-pricing-features i' => 'color: {{VALUE}};',
					'{{WRAPPER}} .elysio-pricing-column.elysio-highlight .elysio-pricing-header' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'icon_color',
			[
				'label' => __( 'Icon Color', 'elysio-toolkit' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .elysio-pricing-features i' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'description_color',
			[
				'label' => __( 'Description Color', 'elysio-toolkit' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .elysio-pricing-features' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();







		$this->start_controls_section(
			'featured_style_section',
			[
				'label' => __( 'Features Style', 'elysio-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'title_color_highlight',
			[
				'label' => __( 'Title Color', 'elysio-toolkit' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#fff',
				'selectors' => [
					'{{WRAPPER}} .elysio-highlight .elysio-pricing-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'title_background_highlight',
			[
				'label' => __( 'Highlight Background', 'elysio-toolkit' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elysio-highlight .elysio-pricing-header' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();







		$this->start_controls_section(
			'button_style_section',
			[
				'label' => __( 'Button Style', 'elysio-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'button_typography',
				'label' => __( 'Typography', 'plugin-domain' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .elysio-button',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'button_text_shadow',
				'label' => __( 'Text Shadow', 'plugin-domain' ),
				'selector' => '{{WRAPPER}} .elysio-button',
			]
		);

		$this->add_control(
			'button_color_scheme',
			[
				'label' => __( 'Color Scheme', 'elysio' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'custom',
				'options' => elysio_button_colors(),
			]
		);

		$this->start_controls_tabs(
			'style_tabs'
		);

		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => __( 'Normal', 'plugin-name' ),
				'condition' => [ 'button_color_scheme' => 'custom' ],
			]
		);

		$this->add_control(
			'normal_color',
			[
				'label' => __( 'Text Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default'	=> '#fff',
				'selectors' => [
					'{{WRAPPER}} .elysio-button' => 'color: {{VALUE}}',
				],
				'condition' => [ 'button_color_scheme' => 'custom' ],
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Background::get_type(),
			[
				'name' => 'normal_background',
				'label' => __( 'Background', 'plugin-domain' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .elysio-button',
				'condition' => [ 'button_color_scheme' => 'custom' ],
			]
		);

		$this->add_control(
			'normal_border_color',
			[
				'label' => __( 'Border Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'default'	=> 'transparent',
				'selectors' => [
					'{{WRAPPER}} .elysio-button' => 'border-color: {{VALUE}}',
				],
				'condition' => [ 'button_color_scheme' => 'custom' ],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => __( 'Hover', 'plugin-name' ),
				'condition' => [ 'button_color_scheme' => 'custom' ],
			]
		);

		$this->add_control(
			'hover_color',
			[
				'label' => __( 'Text Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elysio-button:hover' => 'color: {{VALUE}}',
				],
				'condition' => [ 'button_color_scheme' => 'custom' ],
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Background::get_type(),
			[
				'name' => 'hover_background',
				'label' => __( 'Background', 'plugin-domain' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .elysio-button:hover',
				'condition' => [ 'button_color_scheme' => 'custom' ],
			]
		);

		$this->add_control(
			'hover_border_color',
			[
				'label' => __( 'Border Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elysio-button:hover' => 'border-color: {{VALUE}}',
				],
				'condition' => [ 'button_color_scheme' => 'custom' ],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->add_control(
			'button_border_diveder',
			[
				'type' => \Elementor\Controls_Manager::DIVIDER,
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Border::get_type(),
			[
				'name' => 'button_border',
				'label' => __( 'Border', 'plugin-domain' ),
				'selector' => '{{WRAPPER}} .elysio-button',
				'condition' => [ 'button_color_scheme' => 'custom' ],
			]
		);

		$this->add_responsive_control(
			'button_botder_radius',
			[
				'label' => __( 'Border Radius', 'plugin-domain' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .elysio-button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'button_box_shadow',
				'label' => __( 'Box Shadow', 'plugin-domain' ),
				'selector' => '{{WRAPPER}} .wrapper',
			]
		);

		$this->add_control(
			'button_padding_diveder',
			[
				'type' => \Elementor\Controls_Manager::DIVIDER,
			]
		);

		$this->add_responsive_control(
			'button_padding',
			[
				'label' => __( 'Padding', 'plugin-domain' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .elysio-button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();


	}

	protected function render() {
		$settings = $this->get_settings();

		if ( empty( $settings['pricing'] ) ) {
			return;
		}

		$column_class = ' col-12 col-md-12 col-lg';

		?>
		<div class="elysio-pricing-table container">
			<div class="row">

				<?php $el_counter=0; foreach( $settings['pricing'] as $column ) { ?>

					<?php ++$el_counter; ?>

					<div class="elementor-repeater-item-<?php echo esc_attr( $column['_id'] ) ?> elysio-pricing-column<?php echo( esc_attr(isset( $column['price_col_featured']) ) && $column['price_col_featured'] == 'yes' ? ' elysio-highlight' : '' ); echo esc_attr( $column_class ); ?>">
						<div class="elysio-pricing-wrap">

							<div class="elysio-pricing-header">
								<?php if ( isset( $column['price_col_title'] ) && ! empty( $column['price_col_title'] ) ) : ?>
									<div class="elysio-pricing-title"><?php echo esc_html( $column['price_col_title'] ); ?></div>
								<?php endif; ?>

								<?php if ( isset( $column['price_col_sub_title'] ) && ! empty( $column['price_col_sub_title'] ) ) : ?>
									<div class="elysio-pricing-sub-title"><?php echo esc_html( $column['price_col_sub_title'] ); ?></div>
								<?php endif; ?>
							 </div>

							 <div class="elysio-pricing-content">

								<?php if ( ( isset( $column['price_col_price'] ) && ! empty( $column['price_col_price'] ) ) || ( isset( $column['price_col_price'] ) && ! empty( $column['price_col_price'] ) ) ) : ?>
									<div class="elysio-pricing-cost">
										<?php echo esc_html( $column['price_col_price'] ); ?><span><?php echo esc_html( $column['price_col_text'] ); ?></span>
									</div>
								<?php endif; ?>

								<?php if ( isset( $column['price_col_description'] ) && ! empty( $column['price_col_description'] ) ) : ?>
									<div class="elysio-pricing-features">
										<ul>
											<?php echo '<li><i aria-hidden="true" class="fas fa-check"></i>' . str_replace( array( "\r", "\n\n", "\n" ), array( '', "\n", "</li>\n<li><i aria-hidden='true' class='fas fa-check'></i>" ), trim( wp_kses_post( preg_replace('~<p>(.*?)</p>~is', '$1',$column['price_col_description'] )), "\n\r" ) ) . '</li>'; ?>
										</ul>
									</div>
								<?php endif; ?>


								<?php

								// Button URL 1
								if ( empty( $column['price_col_button_1_link']['url'] ) ) { $column['price_col_button_1_link']['url'] = '#'; };

								if ( ! empty( $column['price_col_button_1_link']['url'] ) ) {
									$this->add_render_attribute( 'btn-1-link-'.$el_counter, 'href', esc_url( $column['price_col_button_1_link']['url'] ) );

									if ( ! empty( $column['price_col_button_1_link']['is_external'] ) ) {
										$this->add_render_attribute( 'btn-1-link-'.$el_counter, 'target', '_blank' );
									}
								}

								// Button URL 2
								/*if ( empty( $column['price_col_button_2_link']['url'] ) ) { $column['price_col_button_2_link']['url'] = '#'; };

								if ( ! empty( $column['price_col_button_2_link']['url'] ) ) {
									$this->add_render_attribute( 'btn-2-link-'.$el_counter, 'href', esc_url( $column['price_col_button_2_link']['url'] ) );

									if ( ! empty( $column['price_col_button_2_link']['is_external'] ) ) {
										$this->add_render_attribute( 'btn-2-link-'.$el_counter, 'target', '_blank' );
									}
								}*/

								$elysio_button_classes = 'elysio-button';
								$elysio_button_classes .= ' button-color-scheme-' . $settings['button_color_scheme'];

								?>

								<div class="elysio-btn-wrap">
									<?php if ( ! empty( $column['price_col_button_1_text'] ) || ! empty( $column['price_col_button_2_text']) || ! empty($button_1_image) || ! empty( $button_2_image )  ) : ?>
										<?php if ( isset( $column['price_col_button_1_show'] ) && $column['price_col_button_1_show'] == 'yes' ) : ?>
											<?php if ( ! empty( $column['price_col_button_1_text'] ) ) : ?>
												<a class="<?php echo $elysio_button_classes; ?>" <?php echo $this->get_render_attribute_string( 'btn-1-link-'.$el_counter ); ?>>
													<?php if ( ! empty( $column['price_col_button_1_text'] ) ) : ?>
														<?php echo esc_html( $column['price_col_button_1_text'] ); ?>
													<?php endif; ?>
												</a>
											<?php endif; ?>
										<?php endif; ?>
										<!-- <?php if ( isset( $column['price_col_button_2_show'] ) && $column['price_col_button_2_show'] == 'yes' ) : ?>
											<?php if ( isset($button_2_image) && ! empty( $button_2_image ) ) : ?>
												<?php if ( ! empty( $column['price_col_button_2_link']['url'] ) ) : ?>
													<a <?php echo $this->get_render_attribute_string( 'btn-2-link-'.$el_counter ); ?>>
														<?php echo wp_kses_post( $button_2_image ); ?>
													</a>
												<?php else : ?>
													<?php echo wp_kses_post( $button_2_image ); ?>
												<?php endif; ?>
											<?php elseif ( ! empty( $column['price_col_button_2_text'] ) ) : ?>
												<a class="btn btn-secondary" <?php echo $this->get_render_attribute_string( 'btn-2-link-'.$el_counter ); ?>>
													<?php if ( ! empty( $column['price_col_button_2_text'] ) ) : ?>
														<?php echo esc_html( $column['price_col_button_2_text'] ); ?>
													<?php endif; ?>
												</a>
											<?php endif; ?>
										<?php endif; ?> -->
									<?php endif; ?>
								</div>
							</div>

						</div>

					</div>

				<?php } ?>
			</div>
		</div>

		<?php

	}

	protected function _content_template() {}
}

Plugin::instance()->widgets_manager->register_widget_type( new Elysio_Widget_Pricing_Table() );