<?php

/**
 * Elysio Showcase
 * @author: Artem Horebenko
 * @version: 1.0
 *
 */

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Elysio_Widget_Toggle_Showcase extends Widget_Base {

	public function get_name() { return 'elysio-toggle-showcase'; }
	public function get_title() { return __( 'Toggle Showcase', 'elysio-toolkit' ); }
	public function get_icon() { return 'eicon-toggle'; }
	public function get_categories() { return [ 'elysio-elements' ]; }

	protected function _register_controls() {

		/* Content */
		$this->start_controls_section(
			'showcase_content_section',
			[
				'label' => __( 'Content', 'elysio-toolkit' ),
			]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'list_icon',
			[
				'label' => __( 'List Item Icon', 'text-domain' ),
				'type' => \Elementor\Controls_Manager::ICONS,
				'default' => [
					'value' => 'fas fa-star',
					'library' => 'solid',
				],
			]
		);

		$repeater->add_control(
			'list_title', [
				'label' => __( 'List Item Title', 'elysio-toolkit' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'List Title' , 'elysio-toolkit' ),
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'list_image',
			[
				'label' => __( 'Choose Image', 'elysio-toolkit' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);

		$this->add_control(
			'list',
			[
				'label' => __( 'Repeater List', 'elysio-toolkit' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'list_title' => __( 'Title #1', 'elysio-toolkit' ),
						'list_icon' => [
							'value' => 'fas fa-star',
							'library' => 'solid',
						],
						'list_image' => \Elementor\Utils::get_placeholder_image_src(),
					],
					[
						'list_title' => __( 'Title #2', 'elysio-toolkit' ),
						'list_icon' => [
							'value' => 'fas fa-star',
							'library' => 'solid',
						],
						'list_image' => \Elementor\Utils::get_placeholder_image_src(),
					],
				],
				'title_field' => '{{{ list_title }}}',
			]
		);


		$this->add_control(
			'additional_hr',
			[
				'type' => \Elementor\Controls_Manager::DIVIDER,
			]
		);


		$this->add_control(
			'fade_anim',
			[
				'label' => __( 'Fade animation', 'elysio-toolkit' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Yes', 'your-plugin' ),
				'label_off' => __( 'No', 'your-plugin' ),
				'return_value' => 'yes',
				'default' => 'no',
			]
		);



		$this->end_controls_section();




		$this->start_controls_section(
			'showcase_style_section',
			[
				'label' => __( 'Style', 'elysio-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'button_typography',
				'label' => __( 'Typography', 'elysio-toolkit' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .ests-item',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'button_text_shadow',
				'label' => __( 'Text Shadow', 'elysio-toolkit' ),
				'selector' => '{{WRAPPER}} .ests-item',
			]
		);

		$this->start_controls_tabs(
			'style_tabs'
		);

		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => __( 'Normal', 'plugin-name' ),
			]
		);

		$this->add_control(
			'normal_color',
			[
				'label' => __( 'Text Color', 'elysio-toolkit' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_3,
				],
				'selectors' => [
					'{{WRAPPER}} .ests-item' => 'color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => __( 'Hover', 'plugin-name' ),
			]
		);

		$this->add_control(
			'hover_color',
			[
				'label' => __( 'Text Color', 'elysio-toolkit' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_4,
				],
				'selectors' => [
					'{{WRAPPER}} .ests-item:hover' => 'color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'style_active_tab',
			[
				'label' => __( 'Active', 'plugin-name' ),
			]
		);

		$this->add_control(
			'active_color',
			[
				'label' => __( 'Text Color', 'elysio-toolkit' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_4,
				],
				'selectors' => [
					'{{WRAPPER}} .ests-item.active' => 'color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();


		$this->end_controls_tabs();

		$this->add_control(
			'text_indent_hr',
			[
				'type' => \Elementor\Controls_Manager::DIVIDER,
			]
		);

		$this->add_control(
			'text_indent',
			[
				'label' => __( 'Text Indent', 'elysio-toolkit' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 8,
				],
				'selectors' => [
					'{{WRAPPER}} .ests-item-icon' => 'margin-right: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'space_between',
			[
				'label' => __( 'Space Between', 'elysio-toolkit' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 48,
				],
				'selectors' => [
					'{{WRAPPER}} .ests-item + .ests-item' => 'margin-top: {{SIZE}}{{UNIT}};',
				],
			]
		);


		$this->end_controls_section();


		// TODO: width for columns, left/right, 50/50, 8/4, 4/8 etc.


	}

protected function render() {
	$settings = $this->get_settings();

	// Elysio Showcase Tabs on Side = ESTS

	$ests_id = wp_unique_id();
	$fade_anim = '';
	if ( 'yes' === $settings['fade_anim'] ) {
		$fade_anim = ' fade ';
	}
	?>
<div class="elysio-showcase-toggle ests row">
	<div class="col-sm-6 offset-md-4 col-md-4">
		<?php
		$active_first = ' show active ';
		if ( $settings['list'] ) {
			echo '<div class="ests-content tab-content" id="ests-tabs-'.$ests_id.'">';
			foreach (  $settings['list'] as $item ) {
				echo '<div 
						class="tab-pane '. $fade_anim . $active_first .' ests-item-' . $item['_id'] . '" 
						id="ests-pills-' . $item['_id'] . '" 
						role="tabpanel" 
						aria-labelledby="ests-pills-' . $item['_id'] . '-tab">';
				$active_first = '';
					echo wp_get_attachment_image( $item['list_image']['id'], 'full' );
				echo '</div>';
			}
			echo '</div>';
			}
		?>
	</div>
	<div class="col-sm-6 col-md-4">
	<?php
	$active_first = ' active ';
	$selected_first = true;
	if ( $settings['list'] ) {
		echo '<div class="ests-pills nav flex-column h-100 justify-content-center" id="ests-pills-'.$ests_id.'" role="tablist" aria-orientation="vertical">';
		foreach (  $settings['list'] as $item ) {
			echo '<div 
					class="ests-item nav-link ests-item-' . $item['_id'] . $active_first .'" 
					id="ests-pills-' . $item['_id'] . '-tab" 
					data-toggle="pill" 
					href="#ests-pills-' . $item['_id'] . '" 
					role="tab" aria-controls="ests-tabs-'.$ests_id.'" 
					aria-selected="' . $selected_first . '">';
			$active_first = '';
			$selected_first = false;
				echo '<span class="ests-item-icon">';
				\Elementor\Icons_Manager::render_icon( $item['list_icon'], [ 'aria-hidden' => 'true' ] );
				echo '</span>';
				echo '<span class="ests-item-title">';
				echo $item['list_title'];
				echo '</span>';
			echo '</div>';
		}
		echo '</div>';
	}
	?>
	</div>
</div>
<?php
}
protected function _content_template() {}

}
Plugin::instance()->widgets_manager->register_widget_type( new Elysio_Widget_Toggle_Showcase() );
