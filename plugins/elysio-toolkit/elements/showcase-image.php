<?php

/**
 * Elysio App Showcase #01
 * @author: Artem Horebenko
 * @version: 1.0
 *
 */

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Elysio_Widget_Image_Showcase extends Widget_Base {

	public function get_name() { return 'elysio-image-showcase'; }
	public function get_title() { return __( 'Image Showcase', 'elysio-toolkit' ); }
	public function get_icon() { return 'eicon-info-box'; }
	public function get_categories() { return [ 'elysio-elements' ]; }

	protected function _register_controls() {

		/* Content */
		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'elysio-toolkit' ),
			]
		);

		// $this->add_control(
		// 	'device_mockup',
		// 	[
		// 		'label' => __( 'Show Device Mockup', 'plugin-domain' ),
		// 		'type' => \Elementor\Controls_Manager::SELECT,
		// 		'default' => 'none',
		// 		'options' => [
		// 			'none' => __( 'No', 'plugin-domain' ),
		// 			'iPhone11-black' => __( 'iPhone 11 Black', 'plugin-domain' ),
		// 			'iPhone11-white' => __( 'iPhone 11 White', 'plugin-domain' ),
		// 			'iPhone11-green' => __( 'iPhone 11 Green', 'plugin-domain' ),
		// 			'iPhone11-purple' => __( 'iPhone 11 Purple', 'plugin-domain' ),
		// 			'iPhone11-yellow' => __( 'iPhone 11 Yellow', 'plugin-domain' ),
		// 			'iPhone11-red' => __( 'iPhone 11 Red', 'plugin-domain' ),
		// 		],
		// 	]
		// );

		$this->add_control(
			'screenshoot_image',
			[
				'label' => __( 'Choose Image', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);

		$this->add_responsive_control(
			'image_width',
			[
				'label' => __( 'Image Max. Width', 'plugin-domain' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 260,
				],
				'selectors' => [
					'{{WRAPPER}} .screenshoot-image' => 'max-width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'decoration_element',
			[
				'label' => __( 'Decoration Element', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'none',
				'options' => [
					'none' => __( 'No', 'plugin-domain' ),
					'big_gradient_circle' => __( 'Big Gradient Circle', 'plugin-domain' ),
				],
			]
		);

		$this->add_responsive_control(
			'decoration_element_width',
			[
				'label' => __( 'Decoration Element Max. Width', 'plugin-domain' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 420,
				],
				'condition' => [ 'decoration_element!' => 'none' ],
				'selectors' => [
					'{{WRAPPER}} .decoration-element-wrap' => 'max-width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'decoration_background',
			[
				'label' => __( 'Decoration Background', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'none',
				'options' => [
					'none' => __( 'No', 'plugin-domain' ),
					'dotes' => __( 'Dotes', 'plugin-domain' ),
					//'custom' => __( 'Custom', 'plugin-domain' ),
				],
			]
		);

		$this->end_controls_section();




		$this->start_controls_section(
			'showcase_style_section',
			[
				'label' => __( 'Style', 'elysio-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);


		$this->add_control(
			'decoration_element_color_scheme',
			[
				'label' => __( 'Decoration Element Color', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'blue',
				'options' => [
					'blue'  => __( 'Blue', 'plugin-domain' ),
					'green' => __( 'Green', 'plugin-domain' ),
					'purple' => __( 'Purple', 'plugin-domain' ),
					'custom' => __( 'Custom', 'plugin-domain' ),
				],
				'condition' => [ 'decoration_element!' => 'none' ],
			]
		);


		$this->add_group_control(
			\Elementor\Group_Control_Background::get_type(),
			[
				'name' => 'decoration_element_background',
				'label_block'	=> true,
				'label' => __( 'Decoration Element Background', 'plugin-domain' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .big-gradient-circle',
				'condition' => [ 'decoration_element!' => 'none', 'decoration_element_color_scheme' => 'custom' ],
			]
		);


		$this->add_responsive_control(
			'decoration_background_padding',
			[
				'label' => __( 'Decoration Background Offset', 'plugin-domain' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				//'condition' => [ 'decoration_element_background!' => 'none' ],
				'selectors' => [
					'{{WRAPPER}} .decoration-background-wrap' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();


	}

protected function render() {
	$settings = $this->get_settings();

	$wrap_class = 'elysio-app-showcase__01';
	$wrap_class .= ' decoration-element-color-scheme-' . $settings['decoration_element_color_scheme'];

?>

<div class="<?php echo $wrap_class; ?>">


	<?php
	if( $settings['decoration_background'] == 'dotes'){
		echo "<div class='decoration-background-wrap'><div class='decoration-background decoration-background--dotes'></div></div>";
	}
	if( $settings['decoration_element'] == 'big_gradient_circle'){
		echo "<div class='decoration-element-wrap'><div class='big-gradient-circle'></div></div>";
	}
	echo '<img class="screenshoot-image" src="' . $settings['screenshoot_image']['url'] . '">'; 
	?>


</div>

<?php
	//echo "<img src='" . ELYSIO_URL . "assets/images/iPhone11-white.png' />" ;


}
protected function _content_template() {}

}
Plugin::instance()->widgets_manager->register_widget_type( new Elysio_Widget_Image_Showcase() );
