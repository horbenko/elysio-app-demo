<?php

/**
 * Elysio Contact Form Element 
 * @author: Artem Horebenko
 * @version: 1.0
 *
 */

// Nice to have:
// - defaults
// - no forms exist
// - more layouts [sqade, etc, creative]
// - primary color
// - floating labels
// - notices, response
// - button animation [ripple, creative, etc.]
// - popup?

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Elysio_Widget_Contact_Form extends Widget_Base {

		public function get_name() { return 'elysio-contact-form'; }

		public function get_title() { return 'Contact Form 7'; }

		public function get_icon() { return 'eicon-form-horizontal'; }

		public function get_categories() { return [ 'elysio-elements' ]; }
	
		private function get_contact_form_list() {
			$args = array('post_type' => 'wpcf7_contact_form', 'posts_per_page' => -1);
			$rs = array();
			if( $data = get_posts($args)){
				foreach($data as $key){
					$id = $key->ID;
					$name = $key->post_title;
					$rs[$id] = $name;
				}
			}else{
				// $rs['0'] = esc_html__('No Contact Form found', 'text-domanin');
			}
			return $rs;
		}

		protected function _register_controls() {
			$this->start_controls_section( 
					'section_content', [ 'label' => __( 'Contactf Form 7', 'elysio-toolkit' ), ] 
			);
			$this->add_control(
				'ctf7',
				[
				'label'		=> __( 'Contactf Form 7', 'elysio-toolkit' ),
				'description'		=> 'Choose from list or <a href="'. site_url('/wp-admin/admin.php?page=wpcf7-new') . '" target="_blank">New Contact Form</a>.',
				'type'    => Controls_Manager::SELECT,
				'label_block' => true,
				'multiple'    => true,
				'default' => '',
				'options' => $this->get_contact_form_list()
				]
			);
			$this->add_responsive_control(
				'contactform_text_align',
				[
					'label' => __( 'Text Align', 'elysio-toolkit' ),
					'type' => Controls_Manager::CHOOSE,
					'label_block' => false,
					'options' => [
						'left' => [
							'title' => __( 'Left', 'elysio-toolkit' ),
							'icon' => 'fa fa-align-left',
						],
						'center' => [
							'title' => __( 'Center', 'elysio-toolkit' ),
							'icon' => 'fa fa-align-center',
						],
						'right' => [
							'title' => __( 'Right', 'elysio-toolkit' ),
							'icon' => 'fa fa-align-right',
						],
					],
					'selectors' => [
					  '{{WRAPPER}}' => 'text-align: {{VALUE}}',
					  '{{WRAPPER}} input, {{WRAPPER}} textarea' => 'text-align: {{VALUE}}',
					],
					'default' => 'left',
				]
			);
			$this->end_controls_section();


			$this->start_controls_section(
				'section_style_form',
				[
					'label' => __( 'Form', 'elysio-toolkit' ),
					'tab'   => Controls_Manager::TAB_STYLE,
				]
			);
			$this->add_control(
				'all_color',
				[
					'label' => __( 'Input Text Color', 'elysio-toolkit' ),
					'type' => Controls_Manager::COLOR,
					'scheme' => [
						'type' => Scheme_Color::get_type(),
						'value' => Scheme_Color::COLOR_1,
					],
					'selectors' => [
						'{{WRAPPER}}, {{WRAPPER}} input[type=text], {{WRAPPER}} input[type=search], {{WRAPPER}} input[type=url], {{WRAPPER}} input[type=tel], {{WRAPPER}} input[type=number], {{WRAPPER}} input[type=range], {{WRAPPER}} input[type=date], {{WRAPPER}} input[type=month], {{WRAPPER}} input[type=week], {{WRAPPER}} input[type=time], {{WRAPPER}} input[type=datetime], {{WRAPPER}} input[type=datetime-local], {{WRAPPER}} input[type=color], {{WRAPPER}} input[type=email], {{WRAPPER}} input[type=file], {{WRAPPER}} select, {{WRAPPER}} textarea' => 'color: {{VALUE}} !important',
					],
					'default'	=> '#000000',
				]
			);
			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'all_typo',
					'label' => __( 'Typography', 'elysio-toolkit' ),
					'scheme' => Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}}, {{WRAPPER}} input, {{WRAPPER}} textarea',
				]
			);
			$this->add_responsive_control(
				'contactform_width',
				[
					'label'	=> __( 'Form Width', 'elysio-toolkit' ),
					'type'	=> Controls_Manager::SLIDER,
					'size_units'	=> [ 'px', '%' ],
					'default'	=> [
						'unit'	=> '%',
						'size'	=> 100,
					],
					'selectors'	=> [
						'{{WRAPPER}}' => 'width: {{SIZE}}{{UNIT}} !important;margin: 0 auto !important;',
					],
				]
			);
			$this->add_control(
				'paragraph_offset',
				[
					'label' => __( 'Paragraph Bottom Offset', 'elysio-toolkit' ),
					'type' => Controls_Manager::SLIDER,
					'size_units' => [ 'px', '%', 'em' ],
					'default' => [
						'unit' => 'em',
						'size' => 0.5,
					],
					'selectors' => [
						'{{WRAPPER}} p' => 'margin-bottom: {{SIZE}}{{UNIT}} !important;',
					],
				]
			);
			$this->end_controls_section();


			$this->start_controls_section(
				'section_style_label',
				[
					'label' => __( 'Labels', 'elysio-toolkit' ),
					'tab'   => Controls_Manager::TAB_STYLE,
				]
			);
			$this->add_control(
				'label_color',
				[
					'label' => __( 'Label Color', 'elysio-toolkit' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} label' => 'color: {{VALUE}}',
					],
					'default'	=> '#000000',
				]
			);
			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'label_typography',
					'label' => __( 'Label Typography', 'elysio-toolkit' ),
					'scheme' => Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} label',
				]
			);
				// $this->add_control(
				// 	'label_offset',
				// 	[
				// 		'label' => __( 'Label Bottom Offset', 'elysio-toolkit' ),
				// 		'type' => Controls_Manager::SLIDER,
				// 		'size_units' => [ 'px', '%', 'em' ],
				// 		'default' => [
				// 			'unit' => 'em',
				// 			'size' => 0.5,
				// 		],
				// 		'selectors' => [
				// 			'{{WRAPPER}} label' => 'margin-bottom: {{SIZE}}{{UNIT}} !important;',
				// 		],
				// 	]
				// );
			$this->end_controls_section();


			$this->start_controls_section(
				'section_style_inputfield',
				[
					'label' => __( 'Input Fields', 'elysio-toolkit' ),
					'tab'   => Controls_Manager::TAB_STYLE,
				]
			);
			$this->add_control(
				'inputfield_color',
				[
					'label' => __( 'Border Color', 'elysio-toolkit' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} input, {{WRAPPER}} textarea' => 'border-color: {{VALUE}} !important',
					],
					'default'	=> '#000000',
				]
			);
			$this->add_control(
				'textfield_bg_color',
				[
					'label' => __( 'Background', 'elysio-toolkit' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} input, {{WRAPPER}} textarea, {{WRAPPER}} .animated-row .floating-label' => 'background-color: {{VALUE}} !important',
					],
					'default'	=> 'rgba(0,0,0,0)',
				]
			);
			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'inputfield_typography',
					'label' => __( 'Typography', 'elysio-toolkit' ),
					'scheme' => Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} input, {{WRAPPER}} textarea',
				]
			);
			$this->add_control(
				'textfield_offset',
				[
					'label' => __( 'Offset', 'elysio-toolkit' ),
					'type' => Controls_Manager::SLIDER,
					'size_units' => [ 'px', '%', 'em' ],
					'default' => [
						'unit' => 'em',
						'size' => 0.5,
					],
					'selectors' => [
						'{{WRAPPER}} input, {{WRAPPER}} textarea' => 'margin: {{SIZE}}{{UNIT}} auto !important;',
					],
				]
			);
			$this->end_controls_section();

			$this->start_controls_section(
				'section_style_button',
				[
					'label' => __( 'Button', 'elysio-toolkit' ),
					'tab'   => Controls_Manager::TAB_STYLE,
				]
			);
		

				$this->add_group_control(
					Group_Control_Typography::get_type(),
					[
						'name' => 'btn_typo',
						'label' => __( 'Button Typography', 'elysio-toolkit' ),
						'scheme' => Scheme_Typography::TYPOGRAPHY_1,
						'selector' => '{{WRAPPER}} .wpcf7-submit',
					]
				);
				$this->add_group_control(
					Group_Control_Text_Shadow::get_type(),
					[
						'name' => 'button_shadow',
						'label' => __( 'Text Shadow', 'elysio-toolkit' ),
						'selector' => '{{WRAPPER}} .wpcf7-submit',
					]
				);
				$this->add_control(
					'button_color_scheme',
					[
						'label' => __( 'Color Scheme', 'elysio' ),
						'type' => \Elementor\Controls_Manager::SELECT,
						'default' => 'custom',
						'options' => elysio_button_colors(),
					]
				);

				$this->start_controls_tabs(
					'style_tabs'
				);
					$this->start_controls_tab(
						'style_normal_tab',
						[
							'label' => __( 'Normal', 'plugin-name' ),
							'condition' => [ 'button_color_scheme' => 'custom' ],
						]
					);
						$this->add_control(
							'btn_color',
							[
								'label' => __( 'Button Color', 'elysio-toolkit' ),
								'type' => Controls_Manager::COLOR,
								'scheme' => [
									'type' => Scheme_Color::get_type(),
									'value' => Scheme_Color::COLOR_1,
								],
								'selectors' => [
									'{{WRAPPER}} .wpcf7-submit' => 'color: {{VALUE}} !important',
								],
								'condition' => [ 'button_color_scheme' => 'custom' ],
							]
						);
						$this->add_control(
							'btn_bg_color',
							[
								'label' => __( 'Button Background', 'elysio-toolkit' ),
								'type' => Controls_Manager::COLOR,
								'scheme' => [
									'type' => Scheme_Color::get_type(),
									'value' => Scheme_Color::COLOR_1,
								],
								'selectors' => [
									'{{WRAPPER}} .wpcf7-submit' => 'background-color: {{VALUE}} !important',
								],
								'condition' => [ 'button_color_scheme' => 'custom' ],
							]
						);
					$this->end_controls_tab();


					$this->start_controls_tab(
						'style_hover_tab',
						[
							'label' => __( 'Hover', 'plugin-name' ),
							'condition' => [ 'button_color_scheme' => 'custom' ],
						]
					);
						$this->add_control(
							'btn_hover_color',
							[
								'label' => __( 'Button Hover Color', 'elysio-toolkit' ),
								'type' => Controls_Manager::COLOR,
								'scheme' => [
									'type' => Scheme_Color::get_type(),
									'value' => Scheme_Color::COLOR_1,
								],
								'selectors' => [
									'{{WRAPPER}} .wpcf7-submit:hover' => 'color: {{VALUE}} !important',
								],
								'condition' => [ 'button_color_scheme' => 'custom' ],
							]
						);
						$this->add_control(
							'btn_hover_bg_color',
							[
								'label' => __( 'Button Hover Background', 'elysio-toolkit' ),
								'type' => Controls_Manager::COLOR,
								'scheme' => [
									'type' => Scheme_Color::get_type(),
									'value' => Scheme_Color::COLOR_1,
								],
								'selectors' => [
									'{{WRAPPER}} .wpcf7-submit:hover' => 'background-color: {{VALUE}} !important;',
								],
								'condition' => [ 'button_color_scheme' => 'custom' ],
							]
						);
						$this->add_control(
							'btn_hover_border_color',
							[
								'label' => __( 'Button Hover Border Color', 'elysio-toolkit' ),
								'type' => Controls_Manager::COLOR,
								'scheme' => [
									'type' => Scheme_Color::get_type(),
									'value' => Scheme_Color::COLOR_1,
								],
								'selectors' => [
									'{{WRAPPER}} .wpcf7-submit:hover' => 'border-color: {{VALUE}} !important;',
								],
								'condition' => [ 'button_color_scheme' => 'custom' ],
							]
						);
					$this->end_controls_tab();
				$this->end_controls_tabs();

				$this->add_control(
					'btn_border_radius',
					[
						'label'	=> __( 'Button Border Radius', 'elysio-toolkit' ),
						'type'	=> Controls_Manager::DIMENSIONS,
						'size_units'	=> [ 'px', '%', 'em' ],
						'selectors'	=> [
							'{{WRAPPER}} input[type=submit].wpcf7-submit'	=> 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
						],
					]
				);
				$this->add_group_control(
					Group_Control_Border::get_type(),
					[
						'name'	=> 'btn_border',
						'label'	=> __( 'Button Border', 'elysio-toolkit' ),
						'selector'	=> '{{WRAPPER}} .wpcf7 input[type=submit].wpcf7-submit',
						'condition' => [ 'button_color_scheme' => 'custom' ],
					]
				);
				$this->add_group_control(
					Group_Control_Box_Shadow::get_type(),
					[
						'name' => 'box_shadow',
						'label' => __( 'Box Shadow', 'plugin-domain' ),
						'selector' => '{{WRAPPER}} .wpcf7 input[type=submit].wpcf7-submit',
					]
				);

				$this->add_control(
					'hr-buttons2',
					[
						'type' => Controls_Manager::DIVIDER,
					]
				);

				$this->add_control(
					'btn_width',
					[
						'label'	=> __( 'Buttton Width', 'elysio-toolkit' ),
						'type'	=> Controls_Manager::SELECT,
						'default'	=> 'full',
						'options'	=> [
							'auto'	=> __( 'Auto', 'elysio-toolkit' ),
							'full'	=> __( 'Full-width', 'elysio-toolkit' ),
						],
					]
				);
				$this->add_control(
					'btn_padding',
					[
						'label'	=> __( 'Button Padding', 'elysio-toolkit' ),
						'type'	=> Controls_Manager::DIMENSIONS,
						'size_units'	=> [ 'px', '%', 'em' ],
						'selectors'	=> [
							'{{WRAPPER}} input[type=submit].wpcf7-submit' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
						],
					]
				);
				$this->add_control(
					'btn_offset',
					[
						'label'	=> __( 'Button Offset', 'elysio-toolkit' ),
						'type'	=> Controls_Manager::SLIDER,
						'size_units'	=> [ 'px', '%', 'em' ],
						'default'	=> [
							'unit'	=> 'em',
							'size'	=> 1,
						],
						'selectors'	=> [
							'{{WRAPPER}} .wpcf7-submit' => 'margin-top: {{SIZE}}{{UNIT}} !important;',
						],
					]
				);
			$this->end_controls_section();

	}


	protected function render() {
		$settings = $this->get_settings();
		
		$html_class = 'wpcf7';
		
		if( $settings['btn_width'] == 'auto' ){
			$html_class .= ' button_width--auto';
		}
		if( $settings['button_color_scheme'] != 'custom' ){
			$html_class .= ' button-color-scheme-' . $settings['button_color_scheme'];
		}
		
		if( $settings['ctf7'] ){
			echo( do_shortcode('[contact-form-7 
				id="'. $settings['ctf7'] .'"
				html_class="' . $html_class. '"
			]') );
		}
	}
	protected function _content_template() {}
}
Plugin::instance()->widgets_manager->register_widget_type( new Elysio_Widget_Contact_Form() );