<?php
namespace Elementor;
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
class Elysio_Widget_GoogleMaps extends Widget_Base {
	public function get_name() {
		return 'elysio-google-maps';
	}
	public function get_title() {
		return __( 'Google Maps', 'elysio-toolkit' );
	}
	public function get_icon() {
		return 'eicon-google-maps';
	}
	public function get_categories() {
		return [ 'elysio-elements' ];
	}
	protected function _register_controls() {
		$this->start_controls_section(
			'section_map',
			[
				'label' => __( 'Map', 'elysio-toolkit' ),
			]
		);
		//$default_address = __( 'New York City, NY, United States', 'elysio-toolkit' );
		$default_latitude = 40.712776;
		$default_logitude = -74.005974;
		// $this->add_control(
		//  'address',
		//  [
		//    'label' => __( 'Map Address', 'elysio-toolkit' ),
		//    'type' => Controls_Manager::TEXT,
		//    'placeholder' => $default_address,
		//    'default' => $default_address,
		//    'label_block' => true,
		//  ]
		// );
		$this->add_control(
			'latitude',
			[
				'label' => __( 'Map Address : Latitude', 'elysio-toolkit' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => $default_latitude,
				'default' => $default_latitude,
				'label_block' => true,
			]
		);
		$this->add_control(
			'longitude',
			[
				'label' => __( 'Map Address : Longitude', 'elysio-toolkit' ),
								'description' => __( '<a href="http://www.latlong.net/" target="_blank">Find your Latitude & Longitude</a>', 'elysio-toolkit' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => $default_logitude,
				'default' => $default_logitude,
				'label_block' => true,
			]
		);
		$this->add_control(
			'zoom',
			[
				'label' => __( 'Zoom Level', 'elysio-toolkit' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 12,
				],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 20,
					],
				],
			]
		);
		$this->add_control(
				'style',
				[
						'label' => __( 'Style', 'elysio-toolkit' ),
						'type' => Controls_Manager::SELECT,
						'options' => [
								'standard' => __( 'Standard', 'elysio-toolkit' ),
								'ultra_light' => __( 'Ultra Light', 'elysio-toolkit' ),
								'light_dream' => __( 'Light Dream', 'elysio-toolkit' ),
								'shades_of_gray' => __( 'Shades of Gray', 'elysio-toolkit' ),
								'subtle_grayscale' => __( 'Subtle Grayscale', 'elysio-toolkit' ),
								'retro' => __( 'Retro', 'elysio-toolkit' ),
								'apple_esque' => __( 'Apple-esque', 'elysio-toolkit' ),
								'blue_essence' => __( 'Blue Essence', 'elysio-toolkit' ),
								'custom' => __( 'Custom', 'elysio-toolkit' ),
						],
						'default' => 'standard',
				]
		);

		$this->add_control(
			'custom_style',
			[
				'label' => __( 'Custom style', 'elysio-toolkit' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'description' => '<a href="https://snazzymaps.com/" target="_blank">Style map online</a>',
				'rows' => 10,
				'placeholder' => __( 'Paste JavaScript style array here:', 'elysio-toolkit' ),
				'conditions' => [
					'terms' => [
						[
							'name' => 'style',
							'operator' => '==',
							'value' => 'custom',
						],
					],
				],
			]
		);
		$this->add_control(
			'height',
			[
				'label' => __( 'Height', 'elysio-toolkit' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 400,
				],
				'range' => [
					'px' => [
						'min' => 40,
						'max' => 1440,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .elysio-map' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'prevent_scroll',
			[
				'label' => __( 'Prevent Scroll', 'elysio-toolkit' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',
				'label_on' => __( 'Yes', 'elysio-toolkit' ),
				'label_off' => __( 'No', 'elysio-toolkit' ),
			]
		);
		$this->add_control(
			'view',
			[
				'label' => __( 'View', 'elysio-toolkit' ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);
		$this->end_controls_section();
	}
	protected function render() {
		$settings = $this->get_settings();
		global $th_map_id;
		$map_id = 'elysio-map-' .  ++$th_map_id;
		if ( 0 === absint( $settings['zoom']['size'] ) ) $settings['zoom']['size'] = 12;
		//if ( '' === $settings['api'] ) $settings['api'] = 'AIzaSyDb-ldlvqnIEXdh6maZVaonnw05xVAttQw';
		if ( '' === $settings['latitude'] ) $settings['latitude'] = 49.293753;
		if ( '' === $settings['longitude'] ) $settings['longitude'] = -123.053398;
		// styles
		if( $settings['style'] == 'custom' ){
			$elysio_map_style = $settings['custom_style'];
		}else{
			switch ($settings['style']) {
				case 'ultra_light':
					$elysio_map_style =  '[{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]';
					break;
				case 'subtle_grayscale':
					$elysio_map_style = '[{"featureType":"administrative","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","elementType":"all","stylers":[{"saturation":-100},{"lightness":"50"},{"visibility":"simplified"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"lightness":"30"}]},{"featureType":"road.local","elementType":"all","stylers":[{"lightness":"40"}]},{"featureType":"transit","elementType":"all","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]},{"featureType":"water","elementType":"labels","stylers":[{"lightness":-25},{"saturation":-100}]}]';
					break;
				case 'shades_of_gray':
					$elysio_map_style = '[{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]';
					break;
				case 'light_dream':
					$elysio_map_style = '[{"featureType":"landscape","stylers":[{"hue":"#FFBB00"},{"saturation":43.400000000000006},{"lightness":37.599999999999994},{"gamma":1}]},{"featureType":"road.highway","stylers":[{"hue":"#FFC200"},{"saturation":-61.8},{"lightness":45.599999999999994},{"gamma":1}]},{"featureType":"road.arterial","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":51.19999999999999},{"gamma":1}]},{"featureType":"road.local","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":52},{"gamma":1}]},{"featureType":"water","stylers":[{"hue":"#0078FF"},{"saturation":-13.200000000000003},{"lightness":2.4000000000000057},{"gamma":1}]},{"featureType":"poi","stylers":[{"hue":"#00FF6A"},{"saturation":-1.0989010989011234},{"lightness":11.200000000000017},{"gamma":1}]}]';
					break;
				case 'retro':
					$elysio_map_style = '[{"elementType":"geometry","stylers":[{"color":"#ebe3cd"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#523735"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#f5f1e6"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#c9b2a6"}]},{"featureType":"administrative.land_parcel","elementType":"geometry.stroke","stylers":[{"color":"#dcd2be"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#ae9e90"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#dfd2ae"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#dfd2ae"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#93817c"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#a5b076"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#447530"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#f5f1e6"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#fdfcf8"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#f8c967"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#e9bc62"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry","stylers":[{"color":"#e98d58"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#db8555"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#806b63"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"color":"#dfd2ae"}]},{"featureType":"transit.line","elementType":"labels.text.fill","stylers":[{"color":"#8f7d77"}]},{"featureType":"transit.line","elementType":"labels.text.stroke","stylers":[{"color":"#ebe3cd"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#dfd2ae"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#b9d3c2"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#92998d"}]}]';
					break;
				case 'blue_essence':
					$elysio_map_style = '[{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#e0efef"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"hue":"#1900ff"},{"color":"#c0e8e8"}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":100},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"visibility":"on"},{"lightness":700}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#7dcdcd"}]}]';
					break;
				case 'apple_esque':
					$elysio_map_style = '[{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]}]';
				break;
					default:
					$elysio_map_style = false;
			}
		}
		?>


		<div class="elysio-map elysio-map" id="<?php echo $map_id ?>" data-map-latitude="<?php echo esc_attr( $settings['latitude'] ) ?>" data-map-longitude="<?php echo esc_attr( $settings['longitude'] ) ?>" data-map-zoom="<?php echo esc_attr( $settings['zoom']['size'] ) ?>" data-map-scroll="<?php echo ( $settings['prevent_scroll'] == 'yes' ? "false" : "true" ); ?>" data-map-style='<?php if( isset( $elysio_map_style ) ) echo $elysio_map_style; ?>'></div>


		<script type="text/javascript">
			jQuery(function ($) {
				setTimeout(function(){
					elysioSetupGoogleMaps();
				},0)
			});
		</script>

		<?php
	}
	protected function _content_template() {}
}
Plugin::instance()->widgets_manager->register_widget_type( new Elysio_Widget_GoogleMaps() );