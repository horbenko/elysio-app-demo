<?php

/**
 * Elysio Button
 * @author: Artem Horebenko
 * @version: 1.0
 *
 */

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Elysio_Widget_Button_Two extends Widget_Base {

	public function get_name() { return 'elysio-button-two'; }
	public function get_title() { return __( 'Elysio Two Button', 'elysio-toolkit' ); }
	public function get_icon() { return 'eicon-dual-button'; }
	public function get_categories() { return [ 'elysio-elements' ]; }

	protected function _register_controls() {


		$this->start_controls_section(
			'button1_content_section',
			[
				'label' => __( 'First Button', 'elysio-toolkit' ),
			]
		);

		$this->add_control(
			'button1_title',
			[
				'label' => __( 'Title', 'elysio' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Default title', 'elysio' ),
				'placeholder' => __( 'Type button title here', 'elysio' ),
				'condition' => [ 'buttons_styled' => 'custom' ],
			]
		);

		$this->add_control(
			'button1_link',
			[
				'label' => __( 'Link', 'elysio' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'elysio' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);

		
		$this->add_control(
			'button1_icon',
			[
				'label' => __( 'Icon', 'text-domain' ),
				'type' => \Elementor\Controls_Manager::ICONS,
				'default' => [
					'value' => 'fas fa-star',
					'library' => 'solid',
				],
				'condition' => [ 'buttons_styled' => 'custom' ],
			]
		);

		$this->add_control(
			'button1_icon_position',
			[
				'label' => __( 'Icon Position', 'elysio' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'before',
				'options' => [
					'before'  => __( 'Before', 'elysio' ),
					'after' => __( 'After', 'elysio' ),
				],
				'condition' => [ 'buttons_styled' => 'custom' ],
			]
		);

		$this->add_control(
			'button1_icon_spacing',
			[
				'label' => __( 'Icon Spacing', 'elysio' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 8,
				],
				'selectors' => [
					'{{WRAPPER}} .button-icon-before' => 'margin-right: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .button-icon-after' => 'margin-left: {{SIZE}}{{UNIT}};',
				],
				'condition' => [ 'buttons_styled' => 'custom' ],
			]
		);
 
		$this->add_control(
			'button1_id_divider',
			[
				'type' => \Elementor\Controls_Manager::DIVIDER,
			]
		);

		$this->add_control(
			'button1_id',
			[
				'label' => __( 'Button ID', 'elysio' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'description'	=> __( 'Please make sure the ID is unique and not used elsewhere on the page this form is displayed. This field allows A-z 0-9 & underscore chars without spaces.', 'elysio' ),
			]
		);

		$this->end_controls_section();




		$this->start_controls_section(
			'button2_content_section',
			[
				'label' => __( 'Second Button', 'elysio-toolkit' ),
			]
		);

		$this->add_control(
			'button2_title',
			[
				'label' => __( 'Title', 'elysio' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Default title', 'elysio' ),
				'placeholder' => __( 'Type button title here', 'elysio' ),
				'condition' => [ 'buttons_styled' => 'custom' ],
			]
		);

		$this->add_control(
			'button2_link',
			[
				'label' => __( 'Link', 'elysio' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'elysio' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);

		$this->add_control(
			'button2_icon',
			[
				'label' => __( 'Icon', 'text-domain' ),
				'type' => \Elementor\Controls_Manager::ICONS,
				'default' => [
					'value' => 'fas fa-star',
					'library' => 'solid',
				],
				'condition' => [ 'buttons_styled' => 'custom' ],
			]
		);

		$this->add_control(
			'button2_icon_position',
			[
				'label' => __( 'Icon Position', 'elysio' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'before',
				'options' => [
					'before'  => __( 'Before', 'elysio' ),
					'after' => __( 'After', 'elysio' ),
				],
				'condition' => [ 'buttons_styled' => 'custom' ],
			]
		);

		$this->add_control(
			'button2_icon_spacing',
			[
				'label' => __( 'Icon Spacing', 'elysio' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 8,
				],
				'selectors' => [
					'{{WRAPPER}} .button-icon-before' => 'margin-right: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .button-icon-after' => 'margin-left: {{SIZE}}{{UNIT}};',
				],
				'condition' => [ 'buttons_styled' => 'custom' ],
			]
		);
 
		$this->add_control(
			'button2_id_divider',
			[
				'type' => \Elementor\Controls_Manager::DIVIDER,
			]
		);

		$this->add_control(
			'button2_id',
			[
				'label' => __( 'Button ID', 'elysio' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'description'	=> __( 'Please make sure the ID is unique and not used elsewhere on the page this form is displayed. This field allows A-z 0-9 & underscore chars without spaces.', 'elysio' ),
			]
		);

		$this->end_controls_section();




		$this->start_controls_section(
			'button_style_section',
			[
				'label' => __( 'Buttons Style', 'elysio-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'buttons_styled',
			[
				'label' => __( 'Buttons Styled', 'elysio-toolkit' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'custom',
				'options' => [
					'branding'  => __( 'Branding Styles', 'elysio-toolkit' ),
					'custom' => __( 'Custom Styles', 'elysio-toolkit' ),
				],
			]
		);

		$this->add_responsive_control(
			'button_botder_radius',
			[
				'label' => __( 'Border Radius', 'elysio' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .elysio-button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition' => [ 'buttons_styled' => 'custom' ],
			]
		);

		$this->add_responsive_control(
			'button_padding',
			[
				'label' => __( 'Padding', 'elysio' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .elysio-button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition' => [ 'buttons_styled' => 'custom' ],
			]
		);

		$this->add_responsive_control(
			'buttons_offset_x',
			[
				'label' => __( 'Buttons Offset X', 'elysio-toolkit' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 2,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 16,
				],
				'desktop_default' => [
					'size' => 16,
					'unit' => 'px',
				],
				'tablet_default' => [
					'size' => 0,
					'unit' => 'px',
				],
				'mobile_default' => [
					'size' => 0,
					'unit' => 'px',
				],
				'selectors' => [
					'{{WRAPPER}} .elysio-button + .elysio-button' => 'margin-left: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .elysio-button--apps-branding + .elysio-button--apps-branding' => 'margin-left: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'buttons_offset_y',
			[
				'label' => __( 'Buttons Offset Y', 'elysio-toolkit' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 2,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 0,
				],
				'desktop_default' => [
					'size' => 0,
					'unit' => 'px',
				],
				'tablet_default' => [
					'size' => 16,
					'unit' => 'px',
				],
				'mobile_default' => [
					'size' => 16,
					'unit' => 'px',
				],
				'selectors' => [
					'{{WRAPPER}} .elysio-button + .elysio-button' => 'margin-top: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .elysio-button--apps-branding + .elysio-button--apps-branding' => 'margin-top: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'buttons_height',
			[
				'label' => __( 'Buttons Height', 'elysio-toolkit' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 2,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 48,
				],
				'selectors' => [
					'{{WRAPPER}} .elysio-button--apps-branding svg' => 'height: {{SIZE}}{{UNIT}};',
				],
				'condition' => [ 'buttons_styled' => 'branding' ],
			]
		);

		// $this->add_control(
		// 	'button_alignment',
		// 	[
		// 		'label' => __( 'Alignment', 'elysio-toolkit' ),
		// 		'type' => Controls_Manager::CHOOSE,
		// 		'label_block' => false,
		// 		'options' => [
		// 			'left' => [
		// 				'title' => __( 'Left', 'elysio-toolkit' ),
		// 				'icon' => 'eicon-text-align-left',
		// 			],
		// 			'center' => [
		// 				'title' => __( 'Center', 'elysio-toolkit' ),
		// 				'icon' => 'eicon-text-align-center',
		// 			],
		// 			'right' => [
		// 				'title' => __( 'Right', 'elysio-toolkit' ),
		// 				'icon' => 'eicon-text-align-right',
		// 			],
		// 			'justify' => [
		// 				'title' => __( 'Justified', 'elysio-toolkit' ),
		// 				'icon' => 'eicon-text-align-justify',
		// 			],
		// 		],
		// 		'selectors' => [
		// 			'{{WRAPPER}}' => '{{VALUE}}',
		// 		],
		// 		'selectors_dictionary' => [
		// 			'left' => 'text-align: left',
		// 			'center' => 'text-align: center',
		// 			'right' => 'text-align: right',
		// 			'justify' => 'text-align: center',
		// 		],
		// 		'default' => 'left',
		// 	]
		// );

		$this->end_controls_section();




		$this->start_controls_section(
			'button1_style_section',
			[
				'label' => __( 'First Button Style', 'elysio-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [ 'buttons_styled' => 'custom' ],
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'button1_typography',
				'label' => __( 'Typography', 'elysio' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .elysio-button-first',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'button1_text_shadow',
				'label' => __( 'Text Shadow', 'elysio' ),
				'selector' => '{{WRAPPER}} .elysio-button-first',
			]
		);

		$this->add_control(
			'button1_color_scheme',
			[
				'label' => __( 'Color Scheme', 'elysio' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'primary',
				'options' => elysio_button_colors(),
			]
		);

		$this->start_controls_tabs(
			'style_tabs1'
		);

		$this->start_controls_tab(
			'style_normal_tab1',
			[
				'label' => __( 'Normal', 'plugin-name' ),
				'condition' => [ 'button1_color_scheme' => 'custom' ],
			]
		);

		$this->add_control(
			'normal_color1',
			[
				'label' => __( 'Text Color', 'elysio' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .elysio-button-first' => 'color: {{VALUE}}',
				],
				'condition' => [ 'button1_color_scheme' => 'custom' ],
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Background::get_type(),
			[
				'name' => 'normal_background1',
				'label' => __( 'Background', 'elysio' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .elysio-button-first',
				'condition' => [ 'button1_color_scheme' => 'custom' ],
			]
		);

		$this->add_control(
			'normal_border_color1',
			[
				'label' => __( 'Border Color', 'elysio' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .elysio-button-first' => 'border-color: {{VALUE}}',
				],
				'condition' => [ 'button1_color_scheme' => 'custom' ],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'style_hover_tab1',
			[
				'label' => __( 'Hover', 'plugin-name' ),
				'condition' => [ 'button1_color_scheme' => 'custom' ],
			]
		);

		$this->add_control(
			'hover_color1',
			[
				'label' => __( 'Text Color', 'elysio' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .elysio-button-first:hover' => 'color: {{VALUE}}',
				],
				'condition' => [ 'button1_color_scheme' => 'custom' ],
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Background::get_type(),
			[
				'name' => 'hover_background1',
				'label' => __( 'Background', 'elysio' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .elysio-button-first:hover',
				'condition' => [ 'button1_color_scheme' => 'custom' ],
			]
		);

		$this->add_control(
			'hover_border_color1',
			[
				'label' => __( 'Border Color', 'elysio' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .elysio-button-first:hover' => 'border-color: {{VALUE}}',
				],
				'condition' => [ 'button1_color_scheme' => 'custom' ],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->add_control(
			'button_border_diveder1',
			[
				'type' => \Elementor\Controls_Manager::DIVIDER,
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Border::get_type(),
			[
				'name' => 'button_border1',
				'label' => __( 'Border', 'elysio' ),
				'selector' => '{{WRAPPER}} .elysio-button-first',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'button_box_shadow1',
				'label' => __( 'Box Shadow', 'elysio' ),
				'selector' => '{{WRAPPER}} .elysio-button-first',
			]
		);

		$this->end_controls_section();




		$this->start_controls_section(
			'button2_style_section',
			[
				'label' => __( 'Second Button Style', 'elysio-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [ 'buttons_styled' => 'custom' ],
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'button2_typography',
				'label' => __( 'Typography', 'elysio' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .elysio-button-second',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'button2_text_shadow',
				'label' => __( 'Text Shadow', 'elysio' ),
				'selector' => '{{WRAPPER}} .elysio-button-second',
			]
		);

		$this->add_control(
			'button2_color_scheme',
			[
				'label' => __( 'Color Scheme', 'elysio' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'secondary',
				'options' => elysio_button_colors(),
			]
		);

		$this->start_controls_tabs(
			'style_tabs2'
		);

		$this->start_controls_tab(
			'style_normal_tab2',
			[
				'label' => __( 'Normal', 'plugin-name' ),
				'condition' => [ 'button2_color_scheme' => 'custom' ],
			]
		);

		$this->add_control(
			'normal_color2',
			[
				'label' => __( 'Text Color', 'elysio' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .elysio-button-second' => 'color: {{VALUE}}',
				],
				'condition' => [ 'button2_color_scheme' => 'custom' ],
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Background::get_type(),
			[
				'name' => 'normal_background2',
				'label' => __( 'Background', 'elysio' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .elysio-button-second',
				'condition' => [ 'button2_color_scheme' => 'custom' ],
			]
		);

		$this->add_control(
			'normal_border_color2',
			[
				'label' => __( 'Border Color', 'elysio' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .elysio-button-second' => 'border-color: {{VALUE}}',
				],
				'condition' => [ 'button2_color_scheme' => 'custom' ],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'style_hover_tab2',
			[
				'label' => __( 'Hover', 'plugin-name' ),
				'condition' => [ 'button2_color_scheme' => 'custom' ],
			]
		);

		$this->add_control(
			'hover_color2',
			[
				'label' => __( 'Text Color', 'elysio' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .elysio-button-second:hover' => 'color: {{VALUE}}',
				],
				'condition' => [ 'button2_color_scheme' => 'custom' ],
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Background::get_type(),
			[
				'name' => 'hover_background2',
				'label' => __( 'Background', 'elysio' ),
				'types' => [ 'classic', 'gradient' ],
				'selector' => '{{WRAPPER}} .elysio-button-second:hover',
				'condition' => [ 'button2_color_scheme' => 'custom' ],
			]
		);

		$this->add_control(
			'hover_border_color2',
			[
				'label' => __( 'Border Color', 'elysio' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .elysio-button-second:hover' => 'border-color: {{VALUE}}',
				],
				'condition' => [ 'button2_color_scheme' => 'custom' ],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->add_control(
			'button_border_diveder2',
			[
				'type' => \Elementor\Controls_Manager::DIVIDER,
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Border::get_type(),
			[
				'name' => 'button_border2',
				'label' => __( 'Border', 'elysio' ),
				'selector' => '{{WRAPPER}} .elysio-button-second',
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'button_box_shadow2',
				'label' => __( 'Box Shadow', 'elysio' ),
				'selector' => '{{WRAPPER}} .elysio-button-second',
			]
		);

		$this->end_controls_section();

	}

	protected function render() {
		$settings = $this->get_settings();
	
		$target1 = $settings['button1_link']['is_external'] ? ' target="_blank"' : '';
		$nofollow1 = $settings['button1_link']['nofollow'] ? ' rel="nofollow"' : '';

		$target2 = $settings['button2_link']['is_external'] ? ' target="_blank"' : '';
		$nofollow2 = $settings['button2_link']['nofollow'] ? ' rel="nofollow"' : '';




		$elysio_button1_classes = 'elysio-button elysio-button-first';
		// if( $settings['button_alignment'] == 'justify' ){
		// 	$elysio_button_classes .= ' w-100';
		// }
		$elysio_button1_classes .= ' button-color-scheme-' . $settings['button1_color_scheme'];


		$elysio_button2_classes = 'elysio-button elysio-button-second';
		// if( $settings['button_alignment'] == 'justify' ){
		// 	$elysio_button_classes .= ' w-100';
		// }
		$elysio_button2_classes .= ' button-color-scheme-' . $settings['button2_color_scheme'];

			if( $settings['buttons_styled'] == 'custom' ){

				echo '<a class="' . $elysio_button1_classes . '" href="' . $settings['button1_link']['url'] . '"' . $target1 . $nofollow1 . '>';

				if( $settings['button1_icon_position'] == 'before' ){
					?>
					<span class="button-icon button-icon-before">
						<?php \Elementor\Icons_Manager::render_icon( $settings['button1_icon'], [ 'aria-hidden' => 'true' ] ); ?>
					</span>
					<?php
					echo $settings['button1_title'];
				}
				if( $settings['button1_icon_position'] == 'after' ){
					echo $settings['button1_title'];
					?>
					<span class="button-icon button-icon-after">
						<?php \Elementor\Icons_Manager::render_icon( $settings['button1_icon'], [ 'aria-hidden' => 'true' ] ); ?>
					</span>
					<?php
				}
				echo '</a>';

				echo '<a class="' . $elysio_button2_classes . '" href="' . $settings['button2_link']['url'] . '"' . $target2 . $nofollow2 . '>';

				if( $settings['button2_icon_position'] == 'before' ){
					?>
					<span class="button-icon button-icon-before">
						<?php \Elementor\Icons_Manager::render_icon( $settings['button2_icon'], [ 'aria-hidden' => 'true' ] ); ?>
					</span>
					<?php
					echo $settings['button2_title'];
				}
				if( $settings['button2_icon_position'] == 'after' ){
					echo $settings['button2_title'];
					?>
					<span class="button-icon button-icon-after">
						<?php \Elementor\Icons_Manager::render_icon( $settings['button2_icon'], [ 'aria-hidden' => 'true' ] ); ?>
					</span>
					<?php
				}
				echo '</a>';
			}

			if( $settings['buttons_styled'] != 'custom' ){
				?>
				<div class="d-flex align-items-center">

					<a class="d-block elysio-button--apps-branding elysio-button-first" <?php echo 'href="' . $settings['button1_link']['url'] . '"' . $target1 . $nofollow1 ; ?> ><svg viewbox="0 0 128 40" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M124 40H4c-2.2 0-4-1.8-4-4V4c0-2.2 1.8-4 4-4h120c2.2 0 4 1.8 4 4v32c0 2.2-1.8 4-4 4z" fill="#000"/><path d="M27 19.8c0-3.1 2.6-4.6 2.7-4.7-1.5-2.1-3.7-2.4-4.5-2.5-1.9-.2-3.8 1.1-4.7 1.1-1 0-2.5-1.1-4.1-1.1-2.1 0-4 1.2-5.1 3.1-2.2 3.8-.6 9.4 1.5 12.5 1.1 1.5 2.3 3.2 3.9 3.1 1.6-.1 2.2-1 4.1-1 1.9 0 2.4 1 4.1 1 1.7 0 2.8-1.5 3.8-3 1.2-1.7 1.7-3.4 1.7-3.5-.1-.1-3.4-1.3-3.4-5zM23.9 10.6c.8-1.1 1.4-2.5 1.3-4-1.2.1-2.8.8-3.6 1.9-.8.9-1.5 2.4-1.3 3.8 1.3.1 2.7-.7 3.6-1.7zM49.8 31.1h-2.2l-1.2-3.8h-4.2l-1.1 3.8H39l4.2-12.9h2.6l4 12.9zM46 25.7l-1.1-3.4c-.1-.3-.3-1.2-.7-2.4-.1.5-.3 1.4-.6 2.4l-1.1 3.4H46zM60.5 26.4c0 1.6-.4 2.8-1.3 3.8-.8.8-1.7 1.2-2.9 1.2s-2.1-.4-2.6-1.3V35h-2.1V25c0-1 0-2-.1-3.1h1.8l.1 1.5c.7-1.1 1.7-1.7 3.1-1.7 1.1 0 2 .4 2.7 1.3.9.8 1.3 1.9 1.3 3.4zm-2.1 0c0-.9-.2-1.7-.6-2.2-.4-.6-1-.9-1.8-.9-.5 0-1 .2-1.4.5-.4.3-.7.8-.8 1.3-.1.3-.1.5-.1.6v1.6c0 .7.2 1.2.6 1.7.4.5 1 .7 1.6.7.8 0 1.4-.3 1.8-.9.5-.6.7-1.4.7-2.4zM71.2 26.4c0 1.6-.4 2.8-1.3 3.8-.8.8-1.7 1.2-2.9 1.2s-2.1-.4-2.6-1.3V35h-2.1V25c0-1 0-2-.1-3.1H64l.1 1.5c.7-1.1 1.7-1.7 3.1-1.7 1.1 0 2 .4 2.7 1.3.9.8 1.3 1.9 1.3 3.4zm-2.1 0c0-.9-.2-1.7-.6-2.2-.4-.6-1-.9-1.8-.9-.5 0-1 .2-1.4.5-.4.3-.7.8-.8 1.3-.1.3-.1.5-.1.6v1.6c0 .7.2 1.2.6 1.7.4.5 1 .7 1.6.7.8 0 1.4-.3 1.8-.9.5-.6.7-1.4.7-2.4zM83.2 27.5c0 1.1-.4 2-1.1 2.7-.8.8-2 1.1-3.5 1.1-1.4 0-2.5-.3-3.3-.8l.5-1.7c.9.5 1.9.8 3 .8.8 0 1.4-.2 1.8-.5.4-.4.6-.8.6-1.4 0-.5-.2-1-.5-1.3-.4-.4-1-.7-1.8-1-2.3-.8-3.4-2.1-3.4-3.7 0-1.1.4-1.9 1.2-2.6.8-.7 1.8-1 3.2-1 1.2 0 2.1.2 2.9.6l-.5 1.7c-.7-.4-1.5-.6-2.5-.6-.7 0-1.3.2-1.7.5-.3.3-.5.7-.5 1.2s.2.9.6 1.3c.3.3 1 .6 1.9 1 1.1.4 1.9 1 2.5 1.6.3.5.6 1.2.6 2.1zM90 23.4h-2.3v4.5c0 1.1.4 1.7 1.2 1.7.4 0 .7 0 .9-.1l.1 1.6c-.4.2-.9.2-1.6.2-.8 0-1.5-.2-1.9-.7-.5-.5-.7-1.3-.7-2.5v-4.7h-1.4v-1.6h1.4v-1.7l2-.6v2.3H90v1.6zM100.3 26.4c0 1.4-.4 2.6-1.2 3.5-.9.9-2 1.4-3.4 1.4s-2.5-.5-3.3-1.4c-.8-.9-1.2-2-1.2-3.4s.4-2.6 1.3-3.5c.8-.9 2-1.4 3.4-1.4s2.5.5 3.3 1.4c.7.9 1.1 2 1.1 3.4zm-2.2.1c0-.9-.2-1.6-.6-2.2-.4-.7-1.1-1.1-1.9-1.1-.8 0-1.5.4-1.9 1.1-.4.6-.6 1.4-.6 2.2 0 .9.2 1.6.6 2.2.4.7 1.1 1.1 1.9 1.1.8 0 1.4-.4 1.9-1.1.4-.6.6-1.4.6-2.2zM107 23.6c-.2 0-.4-.1-.7-.1-.7 0-1.3.3-1.7.8-.3.5-.5 1.1-.5 1.8V31H102v-6.4c0-1.1 0-2-.1-2.9h1.8l.1 1.8h.1c.2-.6.6-1.1 1-1.5.5-.3 1-.5 1.5-.5h.5v2.1h.1zM116.2 26c0 .4 0 .7-.1.9h-6.2c0 .9.3 1.6.9 2.1.5.4 1.2.7 2 .7.9 0 1.8-.1 2.5-.4l.3 1.4c-.9.4-1.9.6-3.1.6-1.4 0-2.6-.4-3.4-1.3-.8-.8-1.2-2-1.2-3.4s.4-2.6 1.1-3.5c.8-1 1.9-1.5 3.3-1.5 1.3 0 2.4.5 3 1.5.7.8.9 1.8.9 2.9zm-1.9-.5c0-.6-.1-1.1-.4-1.6-.4-.6-.9-.9-1.6-.9-.7 0-1.2.3-1.6.8-.3.4-.5 1-.6 1.6h4.2v.1zM45.4 10.3c0 1.1-.3 2-1 2.6-.6.5-1.5.8-2.7.8-.6 0-1.1 0-1.5-.1V7.4c.5-.1 1.1-.1 1.8-.1 1.1 0 1.9.2 2.5.7.5.5.9 1.3.9 2.3zm-1.1 0c0-.7-.2-1.3-.6-1.7-.4-.4-1-.6-1.7-.6-.3 0-.6 0-.8.1v4.7h.7c.8 0 1.4-.2 1.8-.6.4-.4.6-1.1.6-1.9zM51 11.3c0 .7-.2 1.3-.6 1.7-.4.5-1 .7-1.7.7s-1.2-.2-1.6-.7c-.4-.4-.6-1-.6-1.7s.2-1.3.6-1.7c.4-.5 1-.7 1.7-.7s1.2.2 1.6.7c.4.4.6 1 .6 1.7zm-1 0c0-.4-.1-.8-.3-1.1-.2-.4-.5-.5-.9-.5s-.7.2-.9.5c-.2.3-.3.7-.3 1.1 0 .4.1.8.3 1.1.2.4.5.5.9.5s.7-.2.9-.6c.2-.2.3-.6.3-1zM58.7 9l-1.4 4.6h-.9l-.6-2c-.2-.5-.3-1-.4-1.5-.1.5-.2 1-.4 1.5l-.6 2h-.9L52 9h1l.5 2.2.3 1.5c.1-.4.2-.9.4-1.5l.6-2.2h.8l.6 2.1c.2.5.3 1 .4 1.5l.3-1.5.6-2.1h1.2zM63.9 13.6h-1V11c0-.8-.3-1.2-.9-1.2-.3 0-.5.1-.7.3-.2.2-.3.5-.3.8v2.7h-1v-3.3V9h.9v.7c.1-.2.3-.4.5-.6.3-.2.6-.3.9-.3.4 0 .8.1 1.1.4.4.3.5.8.5 1.5v2.9zM66.7 13.6h-1V6.9h1v6.7zM72.7 11.3c0 .7-.2 1.3-.6 1.7-.4.5-1 .7-1.7.7s-1.2-.2-1.6-.7c-.4-.4-.6-1-.6-1.7s.2-1.3.6-1.7c.4-.4 1-.7 1.7-.7s1.2.2 1.6.7c.4.4.6 1 .6 1.7zm-1 0c0-.4-.1-.8-.3-1.1-.2-.4-.5-.5-.9-.5s-.7.2-.9.5c-.2.3-.3.7-.3 1.1 0 .4.1.8.3 1.1.2.4.5.5.9.5s.7-.2.9-.6c.2-.2.3-.6.3-1zM77.6 13.6h-.9l-.1-.5c-.3.4-.8.6-1.3.6-.4 0-.8-.1-1-.4-.2-.3-.4-.6-.4-.9 0-.6.2-1 .7-1.3.5-.3 1.1-.4 2-.4v-.1c0-.6-.3-.9-.9-.9-.5 0-.8.1-1.2.3l-.2-.7c.4-.3.9-.4 1.6-.4 1.2 0 1.8.6 1.8 1.9v1.7c-.1.5-.1.8-.1 1.1zm-1-1.6v-.7c-1.1 0-1.7.3-1.7.9 0 .2.1.4.2.5.1.1.3.2.5.2s.4-.1.6-.2c.2-.1.3-.3.4-.5V12zM83.4 13.6h-.9v-.7c-.3.6-.8.8-1.5.8-.6 0-1-.2-1.4-.6-.4-.4-.5-1-.5-1.7s.2-1.3.6-1.8c.4-.4.9-.6 1.4-.6.6 0 1 .2 1.3.6V7h1V13.6zm-1-1.9v-.8-.3c-.1-.2-.2-.4-.4-.6-.2-.2-.4-.2-.7-.2-.4 0-.7.2-.9.5-.2.3-.3.7-.3 1.2s.1.8.3 1.1c.2.3.5.5.9.5.3 0 .6-.1.8-.4.2-.4.3-.7.3-1zM92.1 11.3c0 .7-.2 1.3-.6 1.7-.4.5-1 .7-1.7.7s-1.2-.2-1.6-.7c-.4-.4-.6-1-.6-1.7s.2-1.3.6-1.7c.4-.5 1-.7 1.7-.7s1.2.2 1.6.7c.4.4.6 1 .6 1.7zm-1.1 0c0-.4-.1-.8-.3-1.1-.2-.4-.5-.5-.9-.5s-.7.2-.9.5c-.2.3-.3.7-.3 1.1 0 .4.1.8.3 1.1.2.4.5.5.9.5s.7-.2.9-.6c.2-.2.3-.6.3-1zM97.6 13.6h-1V11c0-.8-.3-1.2-.9-1.2-.3 0-.5.1-.7.3-.2.2-.3.5-.3.8v2.7h-1v-3.3V9h.9v.7c.1-.2.3-.4.5-.6.3-.2.6-.3.9-.3.4 0 .8.1 1.1.4.4.3.5.8.5 1.5v2.9zM104.4 9.8h-1.1V12c0 .6.2.8.6.8h.5v.8c-.2.1-.5.1-.8.1-.4 0-.7-.1-.9-.4-.2-.2-.3-.7-.3-1.2V9.8h-.7V9h.7v-.8l1-.3V9h1.1v.8h-.1zM109.8 13.6h-1V11c0-.8-.3-1.2-.9-1.2-.5 0-.8.2-1 .7v3.1h-1V6.9h1v2.8c.3-.5.8-.8 1.4-.8.4 0 .8.1 1 .4.3.3.5.9.5 1.5v2.8zM115.3 11.1v.5h-3c0 .5.2.8.4 1 .3.2.6.3 1 .3.5 0 .9-.1 1.2-.2l.2.7c-.4.2-.9.3-1.5.3-.7 0-1.3-.2-1.7-.6-.4-.4-.6-1-.6-1.7s.2-1.3.6-1.7c.4-.5.9-.7 1.6-.7.7 0 1.2.2 1.5.7.2.3.3.8.3 1.4zm-.9-.3c0-.3-.1-.6-.2-.8-.2-.3-.4-.4-.8-.4-.3 0-.6.1-.8.4-.2.2-.3.5-.3.8h2.1z" fill="#fff"/></svg></a>

					<a class="d-block elysio-button--apps-branding elysio-button-second" <?php echo 'href="' . $settings['button2_link']['url'] . '"' . $target2 . $nofollow2 ; ?> ><svg viewbox="0 0 128 40" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M124 40H4c-2.2 0-4-1.8-4-4V4c0-2.2 1.8-4 4-4h120c2.2 0 4 1.8 4 4v32c0 2.2-1.8 4-4 4z" fill="#000"/><path fill-rule="evenodd" clip-rule="evenodd" d="M9.8 19.6V9.1c0-.6.5-1.1 1-1.1s.7.1 1 .3l19.3 10.6c.4.2.6.5.6.8 0 .3-.2.6-.6.8L11.8 31.1c-.2.1-.5.3-1 .3s-1-.5-1-1.1V19.6z" fill="url(#paint0_linear)"/><path fill-rule="evenodd" clip-rule="evenodd" d="M22.3 19.7L10.6 8h.2c.5 0 .7.1 1 .3L26 16.1l-3.7 3.6z" fill="url(#paint1_linear)"/><path fill-rule="evenodd" clip-rule="evenodd" d="M25.9 23.2l-3.6-3.6 3.7-3.7 5.1 2.8c.4.2.6.5.6.8 0 .3-.2.6-.6.8l-5.2 2.9z" fill="url(#paint2_linear)"/><path fill-rule="evenodd" clip-rule="evenodd" d="M10.7 31.2l11.6-11.6 3.6 3.6-14.1 7.7c-.3.2-.5.3-1.1.3.1 0 .1 0 0 0z" fill="url(#paint3_linear)"/><path fill-rule="evenodd" clip-rule="evenodd" d="M71.1 33.9c-.5-.4-.9-1.1-1.1-1.5l1.6-.7c.1.2.3.5.5.8.4.4 1 .8 1.6.8.6 0 1.3-.3 1.7-.8.3-.5.5-1 .5-1.7v-.6c-1.2 1.5-3.7 1.3-5.1-.3-1.5-1.6-1.5-4.3 0-5.9 1.5-1.5 3.7-1.7 5-.3V23h1.7v7.5c0 1.9-.7 3-1.6 3.7-.6.5-1.5.7-2.3.7-.9-.1-1.8-.4-2.5-1zm41.9.4l1.7-4-3-6.9h1.7l2.1 4.9 2.1-4.9h1.7l-4.6 10.9H113zm-8.1-3.8c-.5-.5-.7-1.2-.7-1.9 0-.6.2-1.2.6-1.6.7-.7 1.7-1 2.8-1 .7 0 1.3.1 1.8.4 0-1.2-1-1.7-1.8-1.7-.7 0-1.4.4-1.7 1.1l-1.5-.6c.3-.7 1.1-2 3.1-2 1 0 2 .3 2.6 1 .6.7.8 1.5.8 2.6V31h-1.7v-.7c-.2.3-.6.5-.9.7-.4.2-.9.3-1.4.3-.6-.1-1.5-.3-2-.8zm-53.3-3.6c0-2 1.5-4.2 4.2-4.2 2.6 0 4.2 2.2 4.2 4.2s-1.5 4.2-4.2 4.2-4.2-2.2-4.2-4.2zm9 0c0-2 1.5-4.2 4.2-4.2 2.6 0 4.2 2.2 4.2 4.2s-1.5 4.2-4.2 4.2c-2.6 0-4.2-2.2-4.2-4.2zm-20.3 2.2c-2.5-2.5-2.4-6.6.1-9.2 1.3-1.3 2.9-1.9 4.6-1.9 1.6 0 3.2.6 4.4 1.8l-1.2 1.3c-1.8-1.8-4.7-1.7-6.4.1-1.8 1.9-1.8 4.8 0 6.7 1.8 1.9 4.8 2 6.6.1.6-.6.8-1.4.9-2.2h-4.2V24H51c.1.4.1.9.1 1.4 0 1.5-.6 3-1.6 4-1.1 1.1-2.7 1.7-4.3 1.7-1.8-.1-3.6-.7-4.9-2zm42.3.7c-1.5-1.6-1.5-4.3 0-6 1.5-1.6 4-1.6 5.4 0 .5.5.8 1.2 1.1 1.9L83.6 28c.3.7 1 1.3 2 1.3.9 0 1.5-.3 2.1-1.2l1.5 1c-.2.2-.4.4-.5.6-1.7 1.7-4.6 1.7-6.1.1zM93 31V19.9h3.6c2.1 0 3.8 1.5 3.8 3.3 0 1.8-1.5 3.3-3.4 3.3h-2.2v4.4H93v.1zm8.4 0V19.9h1.7V31h-1.7zm-22.6-.2V18.4h1.8v12.4h-1.8zm30.5-3.1c-.4-.3-1-.4-1.6-.4-1.2 0-1.9.6-1.9 1.3 0 .7.7 1.1 1.4 1.1 1 0 2.1-.8 2.1-2zm-51.2-.8c0-1.2-.8-2.5-2.3-2.5-1.5 0-2.3 1.3-2.3 2.5s.8 2.5 2.3 2.5c1.4 0 2.3-1.3 2.3-2.5zm9 0c0-1.2-.8-2.5-2.3-2.5-1.5 0-2.3 1.3-2.3 2.5s.8 2.5 2.3 2.5c1.5 0 2.3-1.3 2.3-2.5zm8.9.7c0-.1 0-.2.1-.3v-.4-.5c0-.1-.1-.3-.1-.4-.3-1-1.2-1.7-2.1-1.7-1.2 0-2.2 1.2-2.2 2.5 0 1.4 1 2.5 2.3 2.5.8.1 1.6-.6 2-1.7zm7.3-.9l3.7-1.6c-.4-.7-1-.9-1.5-.9-1.5.1-2.4 1.7-2.2 2.5zm15.3-3.4c0-1-.8-1.7-1.9-1.7h-2v3.5h2.1c1 0 1.8-.8 1.8-1.8z" fill="#fff"/><path fill-rule="evenodd" clip-rule="evenodd" d="M114.3 32.3h-.1v-.1h.1-.1v.1h.1zM114.5 32.3c0-.1 0-.1 0 0-.1-.1-.1-.1 0 0-.1 0-.1 0 0 0zm0-.1c0 .1 0 .1 0 0 0 .1-.1.1 0 0 0 0-.1 0 0 0zM114.6 32.2h-.1.1v.1-.1z" fill="#fff"/><path d="M38.9 10.1c0-1.8 1.3-2.9 2.9-2.9 1.1 0 1.8.5 2.3 1.2l-.8.5c-.3-.4-.8-.7-1.5-.7-1.1 0-1.9.8-1.9 2s.8 2 1.9 2c.6 0 1.1-.3 1.3-.5v-.9h-1.6v-.9h2.7V12c-.5.6-1.3 1-2.3 1-1.7 0-3-1.2-3-2.9zM45.4 12.9V7.3h3.8v.9h-2.9v1.4h2.8v.9h-2.8v1.6h2.9v.9l-3.8-.1zM52 12.9V8.2h-1.7v-.9h4.4v.9H53v4.7h-1zM58.1 12.9V7.3h1v5.6h-1zM61.8 12.9V8.2h-1.7v-.9h4.4v.9h-1.7v4.7h-1zM67.7 10.1c0-1.7 1.2-2.9 2.9-2.9s2.9 1.2 2.9 2.9-1.2 2.9-2.9 2.9-2.9-1.3-2.9-2.9zm4.7 0c0-1.2-.7-2-1.9-2-1.1 0-1.9.9-1.9 2 0 1.2.7 2 1.9 2s1.9-.9 1.9-2zM78.5 12.9l-2.9-4v4h-1V7.3h1l2.9 3.9V7.3h1v5.6h-1z" fill="#fff"/><defs><linearGradient id="paint0_linear" x1="16.267" y1="6.574" x2="21.866" y2="29.564" gradientUnits="userSpaceOnUse"><stop stop-color="#006884"/><stop offset="1" stop-color="#8AD1D0"/></linearGradient><linearGradient id="paint1_linear" x1="9.441" y1="9.844" x2="24.697" y2="18.091" gradientUnits="userSpaceOnUse"><stop stop-color="#24BBB6"/><stop offset="1" stop-color="#DBE692"/></linearGradient><linearGradient id="paint2_linear" x1="26.994" y1="23.62" x2="26.994" y2="15.672" gradientUnits="userSpaceOnUse"><stop stop-color="#FCC072"/><stop offset="1" stop-color="#F58A5B"/></linearGradient><linearGradient id="paint3_linear" x1="12.395" y1="33.269" x2="24.446" y2="21.004" gradientUnits="userSpaceOnUse"><stop stop-color="#712B8F"/><stop offset="1" stop-color="#EA1D27"/></linearGradient></defs></svg></a>

				</div>

				<?php
			}

	}
	protected function _content_template() {}
}
Plugin::instance()->widgets_manager->register_widget_type( new Elysio_Widget_Button_Two() );
