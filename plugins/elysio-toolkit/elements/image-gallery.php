<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Elysio_Widget_Image_Gallery extends Widget_Base {

	public function get_name() {
		return 'themo-image-gallery';
	}

	public function get_title() {
		return __( 'Image Gallery', 'elysio-toolkit' );
	}

	public function get_icon() {
		return 'eicon-gallery-grid';
	}

    public function get_categories() {
        return [ 'elysio-elements' ];
    }

	protected function _register_controls() {
		$this->start_controls_section(
			'section_gallery',
			[
				'label' => __( 'Image Gallery', 'elysio-toolkit' ),
			]
		);

		$this->add_control(
			'wp_gallery',
			[
				'label' => __( 'Add Images', 'elysio-toolkit' ),
				'type' => Controls_Manager::GALLERY,
			]
		);

		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
                //'include' => [ 'thumbnail','medium','large','th_img_sm_landscape','th_img_sm_portrait','th_img_sm_square','th_img_sm_standard','th_img_md_landscape','th_img_md_portrait','th_img_md_square'],
				'default'	=> 'full',
                'name' => 'thumbnail',
				'exclude' => [ 'custom','themo-logo','th_img_xs','th_img_lg','th_img_xl','th_img_xxl','themo_team','themo_brands','full'],
			]
		);

		$gallery_columns = range( 1, 6 );
		$gallery_columns = array_combine( $gallery_columns, $gallery_columns );

		$this->add_control(
			'gallery_columns',
			[
				'label' => __( 'Columns', 'elysio-toolkit' ),
				'type' => Controls_Manager::SELECT,
				'default' => 4,
				'options' => $gallery_columns,
			]
		);

		$this->add_control(
			'gallery_link',
			[
				'label' => __( 'Link to', 'elysio-toolkit' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'file',
				'options' => [
					'file' => __( 'Media File', 'elysio-toolkit' ),
					'attachment' => __( 'Attachment Page', 'elysio-toolkit' ),
					'none' => __( 'None', 'elysio-toolkit' ),
				],
			]
		);

		$this->add_control(
			'open_lightbox',
			[
				'label' => __( 'Lightbox', 'elementor' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => [
					'default' => __( 'Default', 'elementor' ),
					'yes' => __( 'Yes', 'elementor' ),
					'no' => __( 'No', 'elementor' ),
				],
				'condition' => [
					'gallery_link' => 'file',
				],
			]
		);

		$this->add_control(
			'gallery_rand',
			[
				'label' => __( 'Ordering', 'elysio-toolkit' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'' => __( 'Default', 'elysio-toolkit' ),
					'rand' => __( 'Random', 'elysio-toolkit' ),
				],
				'default' => '',
			]
		);

        $this->add_control(
            'image_stretch',
            [
                'label' => __( 'Image Stretch', 'elysio-toolkit' ),
                'type' => Controls_Manager::SELECT,
                'default' => 'yes',
                'options' => [
                    'no' => __( 'No', 'elysio-toolkit' ),
                    'yes' => __( 'Yes', 'elysio-toolkit' ),
                ],
            ]
        );

		$this->add_control(
			'view',
			[
				'label' => __( 'View', 'elysio-toolkit' ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$this->end_controls_section();




		$this->start_controls_section(
			'section_image',
			[
				'label' => __( 'Image', 'elysio-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'images_offsetX',
			[
				'label' => __( 'Vertical Offset', 'elysio-toolkit' ),
				'desccription' => __( 'Left and right padding for each image', 'elysio-toolkit' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 24,
				],
				'selectors' => [
					'{{WRAPPER}} .gallery-item' => 'padding: 0 {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'images_offsetY',
			[
				'label' => __( 'Horizontal Offset', 'elysio-toolkit' ),
				'desccription' => __( 'Top and bottom margin for each image', 'elysio-toolkit' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 64,
				],
				'selectors' => [
					'{{WRAPPER}} figure.gallery-item' => 'margin: {{SIZE}}{{UNIT}} 0;',
				],
			]
		);


		$this->add_control(
			'images_hover_style',
			[
				'label' => __( 'Hover Style', 'elysio-toolkit' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'none',
				'options' => [
					'darkness' => __( 'Darkness', 'elysio-toolkit' ),
					'ligthness' => __( 'Ligthness', 'elysio-toolkit' ),
					'opacity'  => __( 'Opacity', 'elysio-toolkit' ),
					'grayscale'  => __( 'Grayscale', 'elysio-toolkit' ),
					'color-from-grayscale'  => __( 'Color from Grayscale', 'elysio-toolkit' ),
					'none' => __( 'None', 'elysio-toolkit' ),
				],
			]
		);

		$this->end_controls_section();




		$this->start_controls_section(
			'section_caption',
			[
				'label' => __( 'Caption', 'elysio-toolkit' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'gallery_display_caption',
			[
				'label' => __( 'Display', 'elysio-toolkit' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'none',
				'options' => [
					'initial' => __( 'Show', 'elysio-toolkit' ),
					'none' => __( 'Hide', 'elysio-toolkit' ),
				],
				'selectors' => [
					'{{WRAPPER}} .gallery-caption' => 'display: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'align',
			[
				'label' => __( 'Alignment', 'elysio-toolkit' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'elysio-toolkit' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'elysio-toolkit' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'elysio-toolkit' ),
						'icon' => 'fa fa-align-right',
					],
					'justify' => [
						'title' => __( 'Justified', 'elysio-toolkit' ),
						'icon' => 'fa fa-align-justify',
					],
				],
				'default' => 'center',
				'selectors' => [
					'{{WRAPPER}} .image-title' => 'text-align: {{VALUE}};',
					'{{WRAPPER}} .caption' => 'text-align: {{VALUE}};',
				],
				'condition' => [
					'gallery_display_caption' => '',
				],
			]
		);

		$this->add_control(
			'text_color',
			[
				'label' => __( 'Text Color', 'elysio-toolkit' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .image-title' => 'color: {{VALUE}};',
					'{{WRAPPER}} .icaption' => 'color: {{VALUE}};',
				],
				'condition' => [
					'gallery_display_caption' => '',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings();

		if ( ! $settings['wp_gallery'] ) {
			return;
		}

        $gallery_class = false;
        if ( 'none' != $settings['images_hover_style'] ) {
            $gallery_class = 'elysio-hover-style--' . $settings['images_hover_style'];
        }

		$ids = wp_list_pluck( $settings['wp_gallery'], 'id' );

		$this->add_render_attribute( 'shortcode', 'ids', esc_attr(implode( ',', $ids )) );
		$this->add_render_attribute( 'shortcode', 'size', esc_attr( $settings['thumbnail_size'] ) );

		if ( $settings['gallery_columns'] ) {
			$this->add_render_attribute( 'shortcode', 'columns', esc_attr( $settings['gallery_columns'] ) );
		}

		if ( $settings['gallery_link'] ) {
			$this->add_render_attribute( 'shortcode', 'link', esc_attr( $settings['gallery_link'] ) );
		}

		if ( ! empty( $settings['gallery_rand'] ) ) {
			$this->add_render_attribute( 'shortcode', 'orderby', esc_attr( $settings['gallery_rand'] ) );
		}
		?>

		<!-- <div class="elementor-image-gallery <?php echo esc_attr( $gallery_class ); ?>">
			<?php //echo do_shortcode( '[gallery ' . sanitize_text_field( $this->get_render_attribute_string( 'shortcode' ) ) . ']' ); ?>
		</div> -->


		<div class="elementor-image-gallery <?php echo esc_attr( $gallery_class ); ?>">
			<?php
			add_filter( 'wp_get_attachment_link', [ $this, 'add_lightbox_data_to_image_link' ], 10, 2 );

			echo do_shortcode( '[gallery ' . $this->get_render_attribute_string( 'shortcode' ) . ']' );

			remove_filter( 'wp_get_attachment_link', [ $this, 'add_lightbox_data_to_image_link' ] );
			?>
		</div>

		<?php
	}

	protected function _content_template() {}
}

Plugin::instance()->widgets_manager->register_widget_type( new Elysio_Widget_Image_Gallery() );
