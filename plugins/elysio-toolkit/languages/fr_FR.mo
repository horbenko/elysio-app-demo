��    �      l  �   �      �     �     �     �     �  
   �     �  	   �     �     �  
   �                         '     .     ;  
   I     T     [     m     {     �     �     �     �     �  	   �     �               $     1     =     O     ^     l     t     |     �     �     �     �     �     �     �     �     �     �     �                    *     3     B     O  
   T  
   _     j  	   v     �     �     �     �     �     �     �     �  
   �     �     �     �     �     
          %  	   6     @     L     ]     d     i     u  
   ~     �     �     �  
   �     �  
   �     �     �     �     �     �     �     �     �     �  
                   !     )     7     >     E     S     Y     _  	   m     w     |     �     �     �  	   �     �  
   �  
   �     �     �     �     �     �       
             &     6     ;     A  
   E     P     e     {  �                 !     *     0     C  
   J     U     k     p     ~     �     �     �     �     �     �     �     �     �               0     B  &   T  )   {  !   �     �     �     �     �               -     E     Y     k     ~     �     �     �     �     �     �     �     �     �               %     1     K     T  
   d     o     �  
   �     �     �     �     �     �     �     �     �          *     1     A     H     ^     s     �     �     �     �     �  	   �     �               )     0     B     K     \     a     i     �     �     �     �     �  	   �     �     �     �     �     �     �  
   �               !     *     =  
   D     O     `     g     n  
   |     �     �     �     �     �  
   �     �     �     �     �          #     )     5     <     M     Y     e     x     }     �     �     �     �     �         I                 `   
   :      ~          -       4   _   a   3                      P       s       5   L   1      E   6             O   �   Y                    i   K   r      T   �   C   2   A          /       ]   F         W      f   S      h   G   l   v      \      z   t   ^       �   8       n       (                  J   !   �   H   u       �           �       "   g   �       q   V       +         *   >       �              $           0       X       ;   <       c   b   =   	       ?   m   .   9   |   N             )   R              d       �       #       p       M   k   '   Q   }   {      w   Z   U           7          j       %   x   e   &   [   B                  D   �       �   y   o       @       ,    # $99 $99.99 Active Add Images After Alignment Attachment Page Auto Background Basic Before Blue Blue Essence Border Border Color Border Radius Box Shadow Button Button Background Button Border Button Border Radius Button Color Button Content Button Hover Background Button Hover Border Color Button Hover Color Button ID Button Link Button Offset Button Padding Button Style Button Text Button Typography Buttons Height Buttons Style Buy now Caption Card Background Center Choose Image Color Scheme Columns Content Custom Custom Styles Custom style Default Default title Description Description Color Display Fade animation Featured Features Style First Button Form Form Width Full-width Google Maps Grayscale Green Height Hide Highlight Background Horizontal Offset Hover Hover Style Icon Icon Color Icon Position Icon Spacing Image Image Gallery Image Stretch Input Fields Input Text Color Justified Label Color Label Typography Labels Left Light Dream Lightbox Line Color Link Link to List Item Title List Title Map Media File No Normal Offset Opacity Ordering Padding Premium Price Price 1 Price text Pricing Pricing Table Primary Primary Color Purple Random Repeater List Retro Right Second Button Secondary Show Space Between Standard Standart Style Sub Title Subtle Grayscale Text Align Text Color Text Indent Text Shadow Title Title #1 Title #2 Title Color Typography Ultra Light Vertical Offset View White Yes Zoom Level http://your-link.com https://your-link.com yes Plural-Forms: nplurals=2; plural=(n > 1);
Project-Id-Version: Elysio Toolkit
PO-Revision-Date: 2020-08-30 11:56+0300
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: index.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: 
Language: fr
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 # 99$ 99,99€ Actif Ajouter des images Après Alignement Page de pièce jointe Auto Arrière-plan Basique Avant Bleu Bleu pétrole Bordure Couleur de la bordure Rayon de la bordure Ombre de la boîte Bouton Arrière-plan du bouton Bordure de bouton Rayon de bordure de bouton Couleur du bouton Contenu du bouton Arrière-plan du bouton lors du survol Couleur de la bordure au survol du bouton Bouton couleur du texte au survol ID du bouton Lien du bouton Décalage du bouton Rembourrage du Bouton Style de bouton Texte du bouton Typographie des boutons Hauteur des boutons Style des boutons Acheter maintenant Légende Fond de carte Centre Choisir une image Jeu de couleurs Colonnes Contenu Personnalisé Styles personnalisés Style personnalisé Par défaut Titre par défaut Description Couleur de la description Afficher Animation Fondu En vedette Style des caractéristiques Premier bouton Formulaire Largeur du formulaire Pleine largeur Google Maps Echelle de gris Vert Hauteur Cacher Arrière-plan mis en avant Décalage horizontal Survol Style de survol Icône Couleur de l’icône Position de l'icône Espacement Icônes Image Galerie d'images Étirement de l’image Champs de saisie Saisir la Couleur de Texte Justifié Couleur de l'étiquette Typographie des libellés Étiquettes Gauche Rêve de lumière Lightbox Couleur de ligne Lien Lier à Titre de la liste d'articles Titre de la liste Carte Fichier média Non Normal Décalage Opacité Trier Marges intérieures Premium Prix Prix 1 Texte Prix Tarification Tableau de prix Primaire Couleur principale Violet Aléatoire Liste répéteur Rétro Droite Second bouton Secondaire Afficher Espace entre Standard Standard Style Sous-titre Grayscale Sous-titre Alignement du texte Couleur du texte Retrait du corps de texte Ombre du texte Titre Titre N° 1 Titre: Couleur du titre Typographie Ultra clair Décalage vertical Voir Blanc Oui Niveau de zoom http://votre-lien.com https://votre-lien.com oui 