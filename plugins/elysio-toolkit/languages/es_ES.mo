��    �      D  �   l      8     9     ;     ?     F  
   M     X  	   ^     h     x  
   }     �     �     �     �     �     �     �  
   �     �     �     �     �               5     O  	   b     l     x     �     �     �     �     �     �     �     �     �     �                          %     2     ;     C     Q     ]     o     w     �     �     �  
   �  
   �     �  	   �     �     �     �     �     �     �       
   
          #     0     6     D     R  	   c     m     y     �     �     �     �  
   �     �     �     �  
   �     �  
   �     �     �     �     �                         $     3  
   9     D     L     Z     b     p     w     ~     �     �  	   �     �     �     �     �     �  	   �     �  
   �  
   �     �     
               %     .  
   :     E     Q     a     f     l  
   p     {     �     �  �  �     G     I     M     T     [     m     v     �     �     �     �     �     �     �     �     �     �     �     �               #     >     O     h  .   �     �     �     �     �     �               3     D  
   R     ]     d     r     �  	   �     �     �     �  	   �     �     �     �     
           (  	   >     H  
   c     n     �     �     �     �     �     �     �     �     �     �     �     	          1     8     N     ]     x     �     �  	   �  	   �     �     �     �     �  	   �     �                     3     6     >     E     L  	   U     _     g     o          �     �     �     �     �     �  	   �     �     �     �  
   �          
  	     	   '     1  
   8     C     [     q     �     �     �  
   �  	   �     �     �     �     �     �          
               1     G     :   j   L       l      E   �           w   [      i       -              W   4   x   d   	          *   e       }   f   C       ^         g   <      )   Y   y   k       @   5                 6       N   v         _   |       .   \   I       (   1   Q   ~   �          G   O          V       m       !       Z   b   9   ?   8   o   K   3          >       �          H       {       B   P   0             X   2           /   M   t       u   %                $       F   7   
   p         r   J      n      +   q       '       T   c           S   z   �          h   &   R           �       ]      `   a                  s   A           U   =   "   ,   D   �               ;   #          # $99 $99.99 Active Add Images After Alignment Attachment Page Auto Background Basic Before Blue Blue Essence Border Border Color Border Radius Box Shadow Button Button Background Button Border Button Border Radius Button Color Button Hover Background Button Hover Border Color Button Hover Color Button ID Button Link Button Padding Button Style Button Text Button Typography Buttons Height Buttons Style Buy now Caption Center Choose Image Color Scheme Columns Content Custom Custom Styles Custom style Darkness Default Default title Description Description Color Display Fade animation Featured Features Style Form Form Width Full-width Google Maps Grayscale Green Height Hide Horizontal Offset Hover Hover Style Icon Icon Color Icon Position Icon Spacing Image Image Gallery Image Stretch Input Text Color Justified Label Color Label Typography Labels Left Light Dream Lightbox Line Color Link Link to List Item Title List Title Map Media File No None Normal Offset Opacity Ordering Padding Premium Prevent Scroll Price Price text Pricing Pricing Table Primary Primary Color Purple Random Retro Right Second Button Secondary Show Space Between Standard Standart Style Sub Title Subtle Grayscale Text Align Text Color Text Indent Text Shadow Title Title #1 Title #2 Title Color Typography Ultra Light Vertical Offset View White Yes Zoom Level http://your-link.com https://your-link.com yes Plural-Forms: nplurals=2; plural=(n != 1);
Project-Id-Version: Elysio Toolkit
PO-Revision-Date: 2020-08-30 11:53+0300
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: index.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: 
Language: es
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.min.js
 # $99 $99.99 Activo Añadir imágenes Después Alineación Página Adjuntos Automático Fondo Básico Antes Azul Esencia Azul Borde Color del borde Radio del borde Sombra de la caja Botón Fondo Botón Perfil del Botón Radio del borde del botón Color del botón Fondo del botón (hover) Color del borde Inferior Color del Botón al pasar el ratón por encima ID del Botón Enlace del botón Padding del botón Estilo del botón Texto del botón Tipografía del botón Altura de los botones Estilo de botón Comprar ahora Subtítulo Centro Elegir imagen Esquema de Color Columnas Contenido Personalizado Estilos Personalizados Estilo personalizado Oscuridad Por defecto Título predeterminado Descripción Color de descripción Mostrar Animación de fundido Destacado Estilo de características Formulario Ancho de formulario Ancho Completo Google Maps Escala de grises Verde Altura Ocultar Desplazamiento Horizontal Hover Estilo de hover Icono Color del icono Posición del Icono Espaciado del icono Imagen Galería de imágenes Estirar imagen Color del Entrada de Texto Justificado Color de la etiqueta Fuente Etiquetas Izquierda Sueño ligero Lightbox Color de linea Enlace Enlazar a Título de elemento da lista Titulo de la lista Mapa Archivo multimedia No Ninguno Normal Offset Opacidad Ordenando Relleno Premium Prevenir scroll Precio Texto de precio Precios Tabla de precios Primario Color primario Púrpura Aleatorio Retro Derecha Segundo botón Secundario Mostrar Espacio intermedio Estándar Estándar Estilo Subtítulo Subtle Escala de grises Alineación del texto Color del texto Guion de texto Sombra de texto Título Título #1 Titulo #2 Color del título Tipografía Ultra Light Desplazamiento Vertical Ver Blanco Sí Nivel de zoom http://tu-enlace.com https://tu-enlace.com sí 