<?php

class Elysio_Recent_Posts_Widget extends WP_Widget {

  /**
   * Sets up the widgets name etc
   */
  public function __construct() {
    $widget_ops = array( 
      'description' => 'Displays recent posts',
    );
    parent::__construct( 'elysio_recent_posts_widget', 'Elysio Recent Posts', $widget_ops );
  }

  /**
   * Outputs the options form on admin
   *
   * @param array $instance The widget options
   */
  public function form( $instance ) {
    // outputs the options form on admin
    $default = array(
      'title' => 'Recent posts',
      'count' => '6',
      'category' => '',
    );
    $instance = wp_parse_args( (array) $instance, $default );
    ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>">Title: </label>
      <input type="text" class="widefat"
             id="<?php echo $this->get_field_id( 'title' ); ?>"
             name="<?php echo $this->get_field_name( 'title' ); ?>"
             value="<?php echo esc_attr( $instance['title'] ); ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'count' ); ?>">Count: </label>
      <input type="number" min="1" class="widefat"
             id="<?php echo $this->get_field_id( 'count' ); ?>"
             name="<?php echo $this->get_field_name( 'count' ); ?>"
             value="<?php echo esc_attr( $instance['count'] ); ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'category' ); ?>">Category ID: </label>
      <input type="number" class="widefat"
             id="<?php echo $this->get_field_id( 'category' ); ?>"
             name="<?php echo $this->get_field_name( 'category' ); ?>"
             value="<?php echo esc_attr( $instance['category'] ); ?>">
    </p>
    <?php
  }

  /**
   * Processing widget options on save
   *
   * @param array $new_instance The new options
   * @param array $old_instance The previous options
   *
   * @return array
   */
  public function update( $new_instance, $old_instance ) {
    // processes widget options to be saved
    $instance = [];
    $instance['title'] = strip_tags( $new_instance['title'] );
    $instance['count'] = strip_tags( $new_instance['count'] );
    $instance['category'] = strip_tags( $new_instance['category'] );
    return $instance;
  }

  /**
   * Outputs the content of the widget
   *
   * @param array $args
   * @param array $instance
   */
  public function widget( $args, $instance ) {
    // outputs the content of the widget
    echo $args['before_widget'];
    if ( ! empty( $instance['title'] ) ) {
      echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
    }

    if ( ! empty( $instance['category'] ) ) {
      $cat = $instance['category'];
    }else{
      $cat = '';
    }

    if ( ! empty( $instance['count'] ) ) {
      $count = $instance['count'];
    }else{
      $count = 6;
    }


    $the_query = new WP_Query( [
      'posts_per_page' => $count,
      'cat' => $cat
    ]);

    ?>
    <ul class="elysio-recent-posts-widget">
    <?php
    while ($the_query -> have_posts()) : $the_query -> the_post(); 
      ?>
      <li>
        <div class="entry-content">
          <a class="elysio-recents-posts-widget__entry-title elysio-headline-color" href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a>
          <time><?php the_date(); ?></time>
        </div>

        <?php
        if( has_post_thumbnail( get_the_ID() ) ){
          echo '<a href="' . esc_url( get_permalink() ) .'" class="entry-thumb" rel="bookmark">' .get_the_post_thumbnail( get_the_ID(), 'thumbnail' ) .'</a>';
        }
        ?> 
      </li>
      <?php
    endwhile;
    ?>
    </ul>
    <?php
    wp_reset_postdata();


    echo $args['after_widget'];
  }
}
