<?php

class Elysio_Socials_Links_Widget extends WP_Widget {

  /**
   * Sets up the widgets name etc
   */
  public function __construct() {
    $widget_ops = array( 
      'description' => 'Displays socials link with icons list',
    );
    parent::__construct( 'elysio_socials-links_widget', 'Elysio Socials Link', $widget_ops );
  }

  /**
   * Outputs the options form on admin
   *
   * @param array $instance The widget options
   */
  public function form( $instance ) {
    // outputs the options form on admin
    $default = array(
      'title' => '',
      'facebook' => '',
      'twitter' => '',
      'linkedin' => '',
      'behance' => '',
      'dribbble' => '',
      'pinterest' => '',
    );
    $instance = wp_parse_args( (array) $instance, $default );
    ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>">Title: </label>
      <input type="text" class="widefat"
             id="<?php echo $this->get_field_id( 'title' ); ?>"
             name="<?php echo $this->get_field_name( 'title' ); ?>"
             value="<?php echo esc_attr( $instance['title'] ); ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'facebook' ); ?>">Facebook URL: </label>
      <input type="text" class="widefat"
             id="<?php echo $this->get_field_id( 'facebook' ); ?>"
             name="<?php echo $this->get_field_name( 'facebook' ); ?>"
             value="<?php echo esc_attr( $instance['facebook'] ); ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'twitter' ); ?>">Twitter URL: </label>
      <input type="text" class="widefat"
             id="<?php echo $this->get_field_id( 'twitter' ); ?>"
             name="<?php echo $this->get_field_name( 'twitter' ); ?>"
             value="<?php echo esc_attr( $instance['twitter'] ); ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'linkedin' ); ?>">LinkedIn URL: </label>
      <input type="text" class="widefat"
             id="<?php echo $this->get_field_id( 'linkedin' ); ?>"
             name="<?php echo $this->get_field_name( 'linkedin' ); ?>"
             value="<?php echo esc_attr( $instance['linkedin'] ); ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'behance' ); ?>">Behance URL: </label>
      <input type="text" class="widefat"
             id="<?php echo $this->get_field_id( 'behance' ); ?>"
             name="<?php echo $this->get_field_name( 'behance' ); ?>"
             value="<?php echo esc_attr( $instance['behance'] ); ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'dribbble' ); ?>">Dribbble URL: </label>
      <input type="text" class="widefat"
             id="<?php echo $this->get_field_id( 'dribbble' ); ?>"
             name="<?php echo $this->get_field_name( 'dribbble' ); ?>"
             value="<?php echo esc_attr( $instance['dribbble'] ); ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'pinterest' ); ?>">Pinterest URL: </label>
      <input type="text" class="widefat"
             id="<?php echo $this->get_field_id( 'pinterest' ); ?>"
             name="<?php echo $this->get_field_name( 'pinterest' ); ?>"
             value="<?php echo esc_attr( $instance['pinterest'] ); ?>">
    </p>
    <?php
  }

  /**
   * Processing widget options on save
   *
   * @param array $new_instance The new options
   * @param array $old_instance The previous options
   *
   * @return array
   */
  public function update( $new_instance, $old_instance ) {
    // processes widget options to be saved
    $instance = [];
    $instance['title'] = strip_tags( $new_instance['title'] );
    $instance['facebook'] = strip_tags( $new_instance['facebook'] );
    $instance['twitter'] = strip_tags( $new_instance['twitter'] );
    $instance['linkedin'] = strip_tags( $new_instance['linkedin'] );
    $instance['behance'] = strip_tags( $new_instance['behance'] );
    $instance['dribbble'] = strip_tags( $new_instance['dribbble'] );
    $instance['pinterest'] = strip_tags( $new_instance['pinterest'] );
    return $instance;
  }

  /**
   * Outputs the content of the widget
   *
   * @param array $args
   * @param array $instance
   */
  public function widget( $args, $instance ) {
    // outputs the content of the widget
    echo $args['before_widget'];
    if ( ! empty( $instance['title'] ) ) {
      echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
    }

    ?>

    <div class="socials-share-wrap m-0 p-0">
      <div class="socials-share-list m-0 p-0">
        <?php if ( ! empty( $instance['facebook'] ) ) { ?>
          <a class="socials-share-link is-facebook" href="<?php echo $instance['facebook']; ?>" target="_blank"><svg viewbox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12 2.04c-5.5 0-10 4.49-10 10.02 0 5 3.66 9.15 8.44 9.9v-7H7.9v-2.9h2.54V9.85c0-2.51 1.49-3.89 3.78-3.89 1.09 0 2.23.19 2.23.19v2.47h-1.26c-1.24 0-1.63.77-1.63 1.56v1.88h2.78l-.45 2.9h-2.33v7a10 10 0 008.44-9.9c0-5.53-4.5-10.02-10-10.02z" fill="#fff"></path></svg> Facebook</a>
        <?php } ?>
        <?php if ( ! empty( $instance['twitter'] ) ) { ?>
          <a class="socials-share-link is-twitter" href="<?php echo $instance['twitter']; ?>" target="_blank"><svg viewbox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M22.46 6c-.77.35-1.6.58-2.46.69.88-.53 1.56-1.37 1.88-2.38-.83.5-1.75.85-2.72 1.05C18.37 4.5 17.26 4 16 4c-2.35 0-4.27 1.92-4.27 4.29 0 .34.04.67.11.98C8.28 9.09 5.11 7.38 3 4.79c-.37.63-.58 1.37-.58 2.15 0 1.49.75 2.81 1.91 3.56-.71 0-1.37-.2-1.95-.5v.03c0 2.08 1.48 3.82 3.44 4.21a4.22 4.22 0 01-1.93.07 4.28 4.28 0 004 2.98 8.521 8.521 0 01-5.33 1.84c-.34 0-.68-.02-1.02-.06C3.44 20.29 5.7 21 8.12 21 16 21 20.33 14.46 20.33 8.79c0-.19 0-.37-.01-.56.84-.6 1.56-1.36 2.14-2.23z" fill="#fff"></path></svg> Twitter</a>
        <?php } ?>
        <?php if ( ! empty( $instance['linkedin'] ) ) { ?>
          <a class="socials-share-link is-linkedin" href="<?php echo $instance['linkedin']; ?>" target="_blank"><svg viewbox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M19 3a2 2 0 012 2v14a2 2 0 01-2 2H5a2 2 0 01-2-2V5a2 2 0 012-2h14zm-.5 15.5v-5.3a3.26 3.26 0 00-3.26-3.26c-.85 0-1.84.52-2.32 1.3v-1.11h-2.79v8.37h2.79v-4.93c0-.77.62-1.4 1.39-1.4a1.4 1.4 0 011.4 1.4v4.93h2.79zM6.88 8.56a1.68 1.68 0 001.68-1.68c0-.93-.75-1.69-1.68-1.69a1.69 1.69 0 00-1.69 1.69c0 .93.76 1.68 1.69 1.68zm1.39 9.94v-8.37H5.5v8.37h2.77z" fill="#fff"></path></svg> LinkedIn</a></div>
        <?php } ?>
        <?php if ( ! empty( $instance['behance'] ) ) { ?>
          <a class="socials-share-link is-behance" href="<?php echo $instance['behance']; ?>" data-pin-custom="true" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 40 40"><g transform="translate(-1250 -831)"><path data-name="behance" d="M1276.064 851.416a1.553 1.553 0 00-.5-1.128 1.628 1.628 0 00-1.1-.384 1.474 1.474 0 00-1.112.408 1.913 1.913 0 00-.5 1.1m5.08-.184a8.522 8.522 0 01.068 1.42h-5.2a1.724 1.724 0 00.752 1.5 1.838 1.838 0 001.032.272 1.505 1.505 0 001.016-.328 1.4 1.4 0 00.4-.5h1.9a2.263 2.263 0 01-.7 1.3 3.381 3.381 0 01-2.672 1.04 3.969 3.969 0 01-2.528-.876 3.516 3.516 0 01-1.072-2.856 3.816 3.816 0 01.984-2.84 3.423 3.423 0 012.552-.992 4.027 4.027 0 011.672.336 2.845 2.845 0 011.232 1.056 3.458 3.458 0 01.568 1.472m-9.872 1.624a1.08 1.08 0 00-.632-1.072 2.968 2.968 0 00-1-.184h-2.132v2.672h2.1a2.3 2.3 0 001.008-.176 1.248 1.248 0 00.656-1.24m-3.764-2.888h2.1a1.947 1.947 0 001.056-.248.927.927 0 00.4-.872.911.911 0 00-.528-.92 3.782 3.782 0 00-1.176-.152h-1.852m5.48 3.76a2.651 2.651 0 01.38 1.454 3.026 3.026 0 01-.44 1.592 2.711 2.711 0 01-1.832 1.28 6.985 6.985 0 01-1.4.136H1262v-9.958h4.8a3.022 3.022 0 012.584 1.064 2.534 2.534 0 01.456 1.52 2.226 2.226 0 01-.456 1.466 2.448 2.448 0 01-.76.568 2.265 2.265 0 011.152.88m6.672-4.08h-4.008v-1h4.008z" /></g></svg> Behance</a>
        <?php } ?>
        <?php if ( ! empty( $instance['dribbble'] ) ) { ?>
          <a class="socials-share-link is-dribbble" href="<?php echo $instance['dribbble']; ?>" data-pin-custom="true" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 40 40"><g transform="translate(-1198 -831)"><path data-name="dribbble" d="M1221.536 856.136a46 46 0 00-1.136-4.2 8.874 8.874 0 011.264-.088h.016a11.186 11.186 0 012.448.3 6.231 6.231 0 01-2.592 3.992m-3.536 1.1a6.175 6.175 0 01-3.712-1.232 9.091 9.091 0 011.456-1.776 8.524 8.524 0 013.24-1.968 31.452 31.452 0 011.232 4.568 6.1 6.1 0 01-2.216.408m-6.24-6.24v-.088c.176.008.408.008.68.008h.008a19.8 19.8 0 005.664-.808c.12.264.24.536.36.824a10.014 10.014 0 00-3.52 2.064 11.747 11.747 0 00-1.72 2 6.163 6.163 0 01-1.472-4m3.48-5.6a19.441 19.441 0 012.232 3.4 17.753 17.753 0 01-4.928.7h-.1c-.192 0-.36 0-.5-.008a6.241 6.241 0 013.296-4.092m2.76-.64a6.181 6.181 0 013.888 1.368 8.172 8.172 0 01-3.064 2.192 23.727 23.727 0 00-2.152-3.42 6.62 6.62 0 011.328-.14m4.9 2.384a6.259 6.259 0 011.332 3.556 12.257 12.257 0 00-2.552-.28h-.008a10.8 10.8 0 00-1.808.152c-.136-.336-.264-.656-.416-.968a9.722 9.722 0 003.452-2.46M1218 843a8 8 0 108 8 8 8 0 00-8-8z" /></g></svg> Dribbble</a>
        <?php } ?>
        <?php if ( ! empty( $instance['pinterest'] ) ) { ?>
          <a class="socials-share-link is-pinterest" href="<?php echo $instance['pinterest']; ?>" data-pin-custom="true" target="_blank"><svg viewbox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.04 21.54c.96.29 1.93.46 2.96.46A10 10 0 102 12c0 4.25 2.67 7.9 6.44 9.34-.09-.78-.18-2.07 0-2.96l1.15-4.94s-.29-.58-.29-1.5c0-1.38.86-2.41 1.84-2.41.86 0 1.26.63 1.26 1.44 0 .86-.57 2.09-.86 3.27-.17.98.52 1.84 1.52 1.84 1.78 0 3.16-1.9 3.16-4.58 0-2.4-1.72-4.04-4.19-4.04-2.82 0-4.48 2.1-4.48 4.31 0 .86.28 1.73.74 2.3.09.06.09.14.06.29l-.29 1.09c0 .17-.11.23-.28.11-1.28-.56-2.02-2.38-2.02-3.85 0-3.16 2.24-6.03 6.56-6.03 3.44 0 6.12 2.47 6.12 5.75 0 3.44-2.13 6.2-5.18 6.2-.97 0-1.92-.52-2.26-1.13l-.67 2.37c-.23.86-.86 2.01-1.29 2.7v-.03z" fill="#fff"></path></svg> Pinterest</a>
        <?php } ?>
    </div>


    <?php
    wp_reset_postdata();


    echo $args['after_widget'];
  }
}
