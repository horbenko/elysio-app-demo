



jQuery(function($){

  // Header Navigation Menu Popup
  if( document.querySelector('.site-header .navbar-toggler') ){
    
    $('.site-header .navbar-toggler').click(function(){
      if( $('#navbarNavDropdown').hasClass('show') ){
        $('body').removeClass('modal-open');
        $('.navbar-toggler-icon').removeClass('is-close-icon');
        jQuery('html').css('overflow-y', 'scroll')
      }else{
        window.scroll(0, 0);
        $('body').addClass('modal-open');
        $('.navbar-toggler-icon').addClass('is-close-icon');
        jQuery('html').css('overflow-y', 'hidden')
      }
    });

    // TODO: if linkscrolled on page section or empty or #
    // $(".site-header a[href*=#]").click(function(e) {
    //   e.preventDefault();
    // });

    // hide menu popup on resize for prevent issues
    window.addEventListener('resize', function(){
      if( $('#navbarNavDropdown').hasClass('show') ){
        $('#navbarNavDropdown').removeClass('show');
        $('body').removeClass('modal-open');
        $('.navbar-toggler-icon').removeClass('is-close-icon');
      }
    });

  }




  $('.animated-row input, .animated-row textarea').focus(function(){
    $(this).parents('.animated-row').addClass('is-filled');
  });
  $('.animated-row input, .animated-row textarea').blur(function(){
    if( $(this).val().trim().length == 0 ){
      $(this).parents('.animated-row').removeClass('is-filled');
    }
  });


});


//document.addEventListener('load', function(){
document.addEventListener('DOMContentLoaded', function(){


    // Header Search Button & Popup
    var element__hssm =  document.getElementById('hssm');
    var element__hscm =  document.getElementById('hscm');
    if (typeof(element__hssm) != 'undefined' && element__hssm != null)
    {
      hssm.onclick = function(){
        // headerSearch.style.display = 'flex';
        jQuery('#headerSearch').fadeIn('fast');
        document.body.classList.add('modal-open');
        setTimeout(function(){
          jQuery('#headerSearch #s').focus();
        }, 600);

        var bodyRect = document.body.getBoundingClientRect(),
            elemRect = element__hssm.getBoundingClientRect();

        element__hscm.style.top = elemRect.top + 'px';
        element__hscm.style.left = elemRect.left + 'px';
      }
      hscm.onclick = function(){
        // headerSearch.style.display = 'none';
        jQuery('#headerSearch').fadeOut('fast');
        document.body.classList.remove('modal-open');
      }
      document.onkeydown = function(evt) {
          evt = evt || window.event;
          var isEscape = false;
          if ("key" in evt) {
              isEscape = (evt.key === "Escape" || evt.key === "Esc");
          } else {
              isEscape = (evt.keyCode === 27);
          }
          if (isEscape) {
            // headerSearch.style.display = 'none';
            jQuery('#headerSearch').fadeOut('fast');
            document.body.classList.remove('modal-open');
          }
      };
    }

    
});




jQuery(function($){
  if( $('#preloader').length > 0 ){
    $('#preloader').fadeOut();
  }
});












let elysioGoogleMapInitialize, elysioSetupGoogleMaps;


jQuery(function ($) {
  elysioGoogleMapInitialize = function() {
    jQuery( '.elysio-map' ).each( function( index, element ) {

      var $this = jQuery(element);

      var uluru = { lat: parseFloat($this.data( 'map-latitude' )), lng: parseFloat($this.data( 'map-longitude' )) };

      var map = new google.maps.Map( document.getElementById($this.attr('id')), {
        zoom: $this.data( 'map-zoom' ),
        center: uluru,
        //disableDefaultUI: true,
        scrollwheel: $this.data( 'map-scroll' ),
        styles: $this.data( 'map-style' ),
      } );

      var marker = new google.maps.Marker({position: uluru, map: map});

    } );

  };

  elysioSetupGoogleMaps = function() {

    var mapAPI = gmapi;

    var mapsApiLoaded = typeof window.google !== 'undefined' && typeof window.google.maps !== 'undefined';
    if ( mapsApiLoaded ) {
      elysioGoogleMapInitialize();
    } else {
      var apiUrl = 'https://maps.googleapis.com/maps/api/js?callback=elysioGoogleMapInitialize';

      if ( mapAPI ) {
        apiUrl += '&key=' + mapAPI;
      }

      $( 'body' ).append( '<script async type="text/javascript" src="' + apiUrl + '">' );
    }
  };


});