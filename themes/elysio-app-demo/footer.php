<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package elysio-app
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'elysio_container_type' );
?>

<div class="site-footer">

	<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

	<div class="wrapper" id="wrapper-footer">

		<div class="<?php echo esc_attr( $container ); ?>">

			<div class="elysio-postfooter row justify-content-between align-items-center">
				<div class="col-md-4">
					<?php elysio_footer_logo(); ?>
				</div>
				<?php elysio_footer_socials(); ?>
			</div>

			<div class="row">

				<div class="col-md-12">

					<footer id="colophon">

						<div class="site-info">

							<?php elysio_site_info(); ?>

						</div><!-- .site-info -->

					</footer><!-- #colophon -->

				</div><!--col end -->

			</div><!-- row end -->

		</div><!-- container end -->

	</div><!-- wrapper end -->

</div><!-- site-footer end -->

</div><!-- #page we need this extra closing tag here -->


<script type="text/javascript">
	window.gmapi = '<?php echo get_theme_mod( 'google_maps_api' ); ?>';
</script>

<?php wp_footer(); ?>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.14.0/css/all.css" integrity="sha384-HzLeBuhoNPvSl5KYnjx0BT+WB0QEEqLprO+NBkkk5gbc67FTaL7XIGa2w1L0Xbgc" crossorigin="anonymous">

</body>

</html>