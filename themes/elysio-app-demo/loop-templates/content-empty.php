<?php
/**
 * Content empty partial template
 *
 * @package elysio-app
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

the_content();
