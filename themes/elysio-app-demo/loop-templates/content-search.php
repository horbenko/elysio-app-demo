<?php
/**
 * Search results partial template.
 *
 * @package elysio-app
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<article <?php post_class('col-12 col-sm-6 col-md-4'); ?> id="post-<?php the_ID(); ?>">

  <?php 
  if( has_post_thumbnail($post->ID) ){
    echo '<a href="' . esc_url( get_permalink() )  . '" rel="bookmark" class="entry-thumb">'  . get_the_post_thumbnail( $post->ID, 'medium' ) . '</a>';
  }
  ?>

  <header class="entry-header">

    <?php
    the_title(
      sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
      '</a></h2>'
    );
    ?>
    
    <div class="entry-meta">

      <?php elysio_posted_on(); ?>

    </div><!-- .entry-meta -->

  </header><!-- .entry-header -->

  <div class="entry-content">

    <?php the_excerpt(); ?>

  </div><!-- .entry-summary -->

</article><!-- #post-## -->
