<?php
/**
 * Single post partial template
 *
 * @package elysio-app
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<article <?php post_class('elysio-decoration-color'); ?> id="post-<?php the_ID(); ?>">

	<div class="row">
		<div class="col-3 col-md-4 pr-0 pr-md-3">
			<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark" class="entry-thumb">
				<?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>
			</a>
		</div>

		<div class="col">
			<header class="entry-header">

				<?php the_title(
			sprintf( '<h3 class="entry-title"><a class="elysio-headline-color" href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
			'</a></h3>'
		); ?>

				<div class="entry-meta elysio-secondary-text-color">

					<?php elysio_posted_on(); ?>

				</div><!-- .entry-meta -->

			</header><!-- .entry-header -->


			<div class="entry-content elysio-main-text-color">

				<?php the_excerpt(); ?>

			</div><!-- .entry-content -->

		</div>

	</div>

</article><!-- #post-## -->
