<?php
/**
 * Single post partial template
 *
 * @package elysio-app
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<article <?php post_class('single-article'); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header">

		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<div class="entry-meta elysio-secondary-text-color">

			<?php elysio_posted_on(); ?>

		</div><!-- .entry-meta -->

	</header><!-- .entry-header -->

	<div class="entry-thumb"><?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?></div>

	<div class="entry-content elysio-main-text-color">

		<?php the_content(); ?>

		<div class="clearfix"></div>

		<?php
		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'elysio' ),
				'after'  => '</div>',
			)
		);
		?>

		<div class="clearfix"></div>

	</div><!-- .entry-content -->
	
	<div class="clearfix"></div>

	<?php elysio_share_links(); ?>

	<footer class="entry-footer"><?php 
		//elysio_entry_footer();
		elysio_entry_categories();
		elysio_entry_tags();
	?></footer><!-- .entry-footer -->

</article><!-- #post-## -->
