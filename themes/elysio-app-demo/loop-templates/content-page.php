<?php
/**
 * Partial template for content in page.php
 *
 * @package elysio-app
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header">

		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

	</header><!-- .entry-header -->

	<div class="entry-thumb"><?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?></div>

	<div class="entry-content">

		<?php the_content(); ?>

		<div class="clearfix"></div>

		<?php
		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'elysio' ),
				'after'  => '</div>',
			)
		);
		?>

		<div class="clearfix"></div>

	</div><!-- .entry-content -->

	<div class="clearfix"></div>

	<?php elysio_share_links(); ?>

	<footer class="entry-footer">

		<?php edit_post_link( __( 'Edit', 'elysio' ), '<span class="edit-link">', '</span>' ); ?>

	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
