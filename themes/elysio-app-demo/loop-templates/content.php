<?php
/**
 * Post rendering content according to caller of get_template_part
 *
 * @package elysio-app
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$elysio_post_classes = 'elysio-decoration-color';

if( !has_post_thumbnail($post->ID) ){
	$elysio_post_classes .= ' is-nothumb';
}

?>



<article <?php post_class( $elysio_post_classes ); ?> id="post-<?php the_ID(); ?>">

	<?php
	if( has_post_thumbnail($post->ID) ){
	?>
		<a href="<?php echo esc_url( get_permalink() ); ?>" class="entry-thumb" rel="bookmark">
			<?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>
		</a>
	<?php
	}
	?>

	<header class="entry-header">

		<?php
		the_title(
			sprintf( '<h2 class="entry-title"><a class="elysio-headline-color" href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
			'</a></h2>'
		);
		?>

		<?php if ( 'post' === get_post_type() ) : ?>

			<div class="entry-meta elysio-secondary-text-color">
				<?php elysio_posted_on(); ?>
			</div><!-- .entry-meta -->

		<?php endif; ?>

	</header><!-- .entry-header -->

	<div class="entry-content elysio-main-text-color">

		<?php the_excerpt(); ?>

		<?php
		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'elysio' ),
				'after'  => '</div>',
			)
		);
		?>

	</div><!-- .entry-content -->

	<footer class="entry-footer elysio-secondary-text-color">

		<?php elysio_entry_footer(); ?>

	</footer><!-- .entry-footer -->

	<div class="read-more">
		<a href="<?php echo esc_url( get_permalink() ); ?>" class="btn btn-primary" rel="bookmark">Read more</a>
	</div>

</article><!-- #post-## -->
