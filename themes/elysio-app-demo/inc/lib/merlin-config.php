<?php

/**
 * Merlin WP configuration file.
 *
 * @package   Merlin WP
 * @version   @@pkg.version
 * @link      https://merlinwp.com/
 * @author    Rich Tabor, from ThemeBeans.com & the team at ProteusThemes.com
 * @copyright Copyright (c) 2018, Merlin WP of Inventionn LLC
 * @license   Licensed GPLv3 for Open Source Use
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}

if ( ! class_exists( 'Merlin' ) ) {
  return;
}
/**
 * Set directory locations, text strings, and settings.
 */
$wizard = new Merlin(
  $config = array(
    'directory'            => 'inc/lib/merlin', // Location / directory where Merlin WP is placed in your theme.
    'merlin_url'           => 'merlin', // The wp-admin page slug where Merlin WP loads.
    'parent_slug'          => 'themes.php', // The wp-admin parent page slug for the admin menu item.
    'capability'           => 'manage_options', // The capability required for this menu to be displayed to the user.
    'child_action_btn_url' => 'https://codex.wordpress.org/child_themes', // URL for the 'child-action-link'.
    'dev_mode'             => true, // Enable development mode for testing.
    'license_step'         => false, // EDD license activation step.
    'license_required'     => false, // Require the license activation step.
    'license_help_url'     => '', // URL for the 'license-tooltip'.
    'edd_remote_api_url'   => '', // EDD_Theme_Updater_Admin remote_api_url.
    'edd_item_name'        => '', // EDD_Theme_Updater_Admin item_name.
    'edd_theme_slug'       => '', // EDD_Theme_Updater_Admin item_slug.
    'ready_big_button_url' => get_site_url(), // Link for the big button on the ready step.
  ),
  $strings = array(
    'admin-menu'               => esc_html__( 'Theme Setup', 'elysio-architect' ),
    /* translators: 1: Title Tag 2: Theme Name 3: Closing Title Tag */
    'title%s%s%s%s'            => esc_html__( '%1$s%2$s Themes &lsaquo; Theme Setup: %3$s%4$s', 'elysio-architect' ),
    'return-to-dashboard'      => esc_html__( 'Return to the dashboard', 'elysio-architect' ),
    'ignore'                   => esc_html__( 'Disable this wizard', 'elysio-architect' ),
    'btn-skip'                 => esc_html__( 'Skip', 'elysio-architect' ),
    'btn-next'                 => esc_html__( 'Next', 'elysio-architect' ),
    'btn-start'                => esc_html__( 'Start', 'elysio-architect' ),
    'btn-no'                   => esc_html__( 'Cancel', 'elysio-architect' ),
    'btn-plugins-install'      => esc_html__( 'Install', 'elysio-architect' ),
    'btn-child-install'        => esc_html__( 'Install', 'elysio-architect' ),
    'btn-content-install'      => esc_html__( 'Install', 'elysio-architect' ),
    'btn-import'               => esc_html__( 'Import', 'elysio-architect' ),
    'btn-license-activate'     => esc_html__( 'Activate', 'elysio-architect' ),
    'btn-license-skip'         => esc_html__( 'Later', 'elysio-architect' ),
    /* translators: Theme Name */
    'license-header%s'         => esc_html__( 'Activate %s', 'elysio-architect' ),
    /* translators: Theme Name */
    'license-header-success%s' => esc_html__( '%s is Activated', 'elysio-architect' ),
    /* translators: Theme Name */
    'license%s'                => esc_html__( 'Enter your license key to enable remote updates and theme support.', 'elysio-architect' ),
    'license-label'            => esc_html__( 'License key', 'elysio-architect' ),
    'license-success%s'        => esc_html__( 'The theme is already registered, so you can go to the next step!', 'elysio-architect' ),
    'license-json-success%s'   => esc_html__( 'Your theme is activated! Remote updates and theme support are enabled.', 'elysio-architect' ),
    'license-tooltip'          => esc_html__( 'Need help?', 'elysio-architect' ),
    /* translators: Theme Name */
    'welcome-header%s'         => esc_html__( 'Welcome to %s', 'elysio-architect' ),
    'welcome-header-success%s' => esc_html__( 'Hi. Welcome back', 'elysio-architect' ),
    'welcome%s'                => esc_html__( 'This wizard will set up your theme, install plugins, and import content. It is optional & should take only a few minutes.', 'elysio-architect' ),
    'welcome-success%s'        => esc_html__( 'You may have already run this theme setup wizard. If you would like to proceed anyway, click on the "Start" button below.', 'elysio-architect' ),
    'child-header'             => esc_html__( 'Install Child Theme', 'elysio-architect' ),
    'child-header-success'     => esc_html__( 'You\'re good to go!', 'elysio-architect' ),
    'child'                    => esc_html__( 'Let\'s build & activate a child theme so you may easily make theme changes.', 'elysio-architect' ),
    'child-success%s'          => esc_html__( 'Your child theme has already been installed and is now activated, if it wasn\'t already.', 'elysio-architect' ),
    'child-action-link'        => esc_html__( 'Learn about child themes', 'elysio-architect' ),
    'child-json-success%s'     => esc_html__( 'Awesome. Your child theme has already been installed and is now activated.', 'elysio-architect' ),
    'child-json-already%s'     => esc_html__( 'Awesome. Your child theme has been created and is now activated.', 'elysio-architect' ),
    'plugins-header'           => esc_html__( 'Install Plugins', 'elysio-architect' ),
    'plugins-header-success'   => esc_html__( 'You\'re up to speed!', 'elysio-architect' ),
    'plugins'                  => esc_html__( 'Let\'s install some essential WordPress plugins to get your site up to speed.', 'elysio-architect' ),
    'plugins-success%s'        => esc_html__( 'The required WordPress plugins are all installed and up to date. Press "Next" to continue the setup wizard.', 'elysio-architect' ),
    'plugins-action-link'      => esc_html__( 'Advanced', 'elysio-architect' ),
    'import-header'            => esc_html__( 'Import Content', 'elysio-architect' ),
    'import'                   => esc_html__( 'Let\'s import content to your website, to help you get familiar with the theme.', 'elysio-architect' ),
    'import-action-link'       => esc_html__( 'Advanced', 'elysio-architect' ),
    'ready-header'             => esc_html__( 'All done. Have fun!', 'elysio-architect' ),
    /* translators: Theme Author */
    'ready%s'                  => esc_html__( 'Your theme has been all set up. Enjoy your new theme by %s.', 'elysio-architect' ),
    'ready-action-link'        => esc_html__( 'Extras', 'elysio-architect' ),
    'ready-big-button'         => esc_html__( 'View your website', 'elysio-architect' ),
    'ready-link-1'             => sprintf( '<a href="%1$s" target="_blank">%2$s</a>', 'https://wordpress.org/support/', esc_html__( 'Explore WordPress', 'elysio-architect' ) ),
    'ready-link-2'             => sprintf( '<a href="%1$s" target="_blank">%2$s</a>', 'https://themebeans.com/contact/', esc_html__( 'Get Theme Support', 'elysio-architect' ) ),
    'ready-link-3'             => sprintf( '<a href="%1$s">%2$s</a>', admin_url( 'customize.php' ), esc_html__( 'Start Customizing', 'elysio-architect' ) ),
  )
);




function prefix_merlin_local_import_files() {
    return array(
        array(
            'import_file_name'             => 'Elysio App Light',
            'local_import_file'            => get_parent_theme_file_path( '/inc/demo/elysioapp1.xml' ),
            'local_import_widget_file'     => get_parent_theme_file_path( '/inc/demo/app1.elysio.top-widgets.wie' ),
            'local_import_customizer_file' => get_parent_theme_file_path( '/inc/demo/elysioapp1-demo-export.dat' ),
            // 'import_preview_image_url'     => 'https://architect.elysio.top/wp-content/uploads/2019/07/Theme-Support@2x.png',
            // 'import_notice'                => __( 'After you import this demo, you will have to setup the slider separately.', 'elysio-architect' ),
            'preview_url'                  => 'https://app1.elysio.top',
        ),
        array(
            'import_file_name'             => 'Elysio App Dark',
            'local_import_file'            => get_parent_theme_file_path( '/inc/demo/elysioapp2.xml' ),
            'local_import_widget_file'     => get_parent_theme_file_path( '/inc/demo/app2.elysio.top-widgets.wie' ),
            'local_import_customizer_file' => get_parent_theme_file_path( '/inc/demo/elysioapp2-demo-export.dat' ),

            // 'import_preview_image_url'     => get_parent_theme_file_path( 'screenshot.png' ),
            // 'import_notice'                => __( 'A special note for this import.', 'elysio-architect' ),
            'preview_url'                  => 'https://app2.elysio.top',
        ),
    );
}
add_filter( 'merlin_import_files', 'prefix_merlin_local_import_files' );



/*function merlin_local_import_files() {
    return array(
        array(
            'import_file_name'             => 'Demo Import',
            'local_import_file'            => get_parent_theme_file_path( '/inc/demo/elysioarchitect.WordPress.2020-01-15.xml' ),
            'local_import_widget_file'     => get_parent_theme_file_path( '/inc/demo/widgets.wie' ),
            'local_import_customizer_file' => get_parent_theme_file_path( '/inc/demo/customizer.dat' ),
            'import_preview_image_url'     => 'https://architect.elysio.top/wp-content/uploads/2019/07/Theme-Support@2x.png',
            'import_notice'                => __( 'A special note for this import.', 'elysio-architect' ),
            'preview_url'                  => 'https://architect.elysio.top/',
        ),
    );
}
add_filter( 'merlin_import_files', 'merlin_local_import_files' );*/