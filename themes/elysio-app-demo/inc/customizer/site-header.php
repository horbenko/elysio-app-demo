<?php
/**
 * Elysio Theme Customizer: Site Header
 *
 * @package elysio-app
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;


if ( ! function_exists( 'elysio_theme_customize_header_register' ) ) {
	function elysio_theme_customize_header_register( $wp_customize ) {

		$section = 'elysio_header_options';

		$wp_customize->add_section(
			$section,
			array(
				'title'       => __( 'Site Header', 'elysio' ),
				'capability'  => 'edit_theme_options',
				'priority'    => apply_filters( 'elysio_theme_layout_options_priority', 140 ),
			)
		);


		$setting = 'elysio_header_background_color';

		$wp_customize->add_setting( $setting, [
			'transport'   => $transport
		] );

		$wp_customize->add_control(
			new WP_Customize_Color_Control( $wp_customize, $setting, [
				'label'    => __( 'Background Color', 'elysio' ),
				'section'  => $section,
				'settings' => $setting,
				'priority'	=>	1
			] )
		);


		$setting = 'elysio_header_link_color';

		$wp_customize->add_setting( $setting, [
			'transport'   => $transport
		] );

		$wp_customize->add_control(
			new WP_Customize_Color_Control( $wp_customize, $setting, [
				'label'    => __( 'Link Color', 'elysio' ),
				'section'  => $section,
				'settings' => $setting,
				'priority'	=>	1
			] )
		);


		$setting = 'elysio_header_submenu_background';

		$wp_customize->add_setting( $setting, [
			'transport'   => $transport
		] );

		$wp_customize->add_control(
			new WP_Customize_Color_Control( $wp_customize, $setting, [
				'label'    => __( 'Sub-Menu Background', 'elysio' ),
				'section'  => $section,
				'settings' => $setting,
				'priority'	=>	1
			] )
		);


		$setting = 'elysio_header_submenu_link_color';

		$wp_customize->add_setting( $setting, [
			'transport'   => $transport
		] );

		$wp_customize->add_control(
			new WP_Customize_Color_Control( $wp_customize, $setting, [
				'label'    => __( 'Sub-Menu Link Color', 'elysio' ),
				'section'  => $section,
				'settings' => $setting,
				'priority'	=>	1
			] )
		);

	}
}
add_action( 'customize_register', 'elysio_theme_customize_header_register' );




function customizer_header_style_tag(){

	$style = [];

	/* Header Background Color */
	$style[] = '.site-header .bg-primary { background-color: ' . get_theme_mod( 'elysio_header_background_color' ) . ' !important; }';
	$style[] = '.site-header .collapse:not(.show) .menu-item.button .nav-link { color: ' . get_theme_mod( 'elysio_header_background_color' ) . ' !important; }';
	$style[] = '.site-header .navbar-collapse.show { background-color: ' . get_theme_mod( 'elysio_header_background_color' ) . ' !important; }';

	/* Header Link Color */
	$style[] = '.site-header .navbar-dark .navbar-nav .nav-link { color: ' . get_theme_mod( 'elysio_header_link_color' ) . ' !important; }';
	$style[] = '.site-header .navbar-toggler-icon-svg { fill: ' . get_theme_mod( 'elysio_header_link_color' ) . ' !important; }';
	$style[] = '.site-header .collapse:not(.show) .menu-item.button { background-color: ' . get_theme_mod( 'elysio_header_link_color' ) . ' !important; }';
	$style[] = '@media (max-width: 767px){ .site-header .navbar-expand-md .navbar-nav .dropdown-menu .dropdown-item { color: ' . get_theme_mod( 'elysio_header_link_color' ) . ' !important; } }';

	/* Header SubMenu Background Color */
	$style[] = '@media (min-width: 768px){ .site-header .navbar-expand-md .navbar-nav .dropdown-menu { background-color: ' . get_theme_mod( 'elysio_header_submenu_background' ) . ' !important; } }';

	/* Header SubMenu Link Color */
	$style[] = '@media (min-width: 768px){ .site-header .navbar-expand-md .navbar-nav .dropdown-menu .dropdown-item { color: ' . get_theme_mod( 'elysio_header_submenu_link_color' ) . ' !important; } }';

	echo "<style>\n" . implode( "\n", $style ) . "\n</style>\n";
}

add_action( 'wp_head', 'customizer_header_style_tag' );