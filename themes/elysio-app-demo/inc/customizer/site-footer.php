<?php
/**
 * Elysio Theme Customizer: Site Footer
 *
 * @package elysio-app
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;


if ( ! function_exists( 'elysio_theme_customize_footer_register' ) ) {
	function elysio_theme_customize_footer_register( $wp_customize ) {

		$section = 'elysio_theme_footer_options';

		$wp_customize->add_section(
			$section,
			array(
				'title'       => __( 'Site Footer', 'elysio' ),
				'capability'  => 'edit_theme_options',
				'priority'    => apply_filters( 'elysio_theme_options_priority', 160 ),
			)
		);

		
		$setting = 'elysio_footer_logo_display';
		$wp_customize->add_setting( $setting, [
			'default'    =>  'true',
			'transport'  =>  $transport
		] );
		$wp_customize->add_control( $setting, [
			'section' => $section,
			'label'   => __( 'Show Logo in Footer', 'elysio' ),
			'type'    => 'checkbox',
		] );

		$setting = 'elysio_footer_logo';
		$wp_customize->add_setting( $setting, [
				'default'      => '',
				'transport'    => $transport
			]
		);
		$wp_customize->add_control(
			new WP_Customize_Image_Control( $wp_customize, $setting, [
				'label'    => __( 'Footer Logo', 'elysio' ),
				'settings' => $setting,
				'section'  => $section
			] )
		);


		$setting = 'socials_facebook_url';
		$wp_customize->add_setting( $setting, [
			'default'            => '',
			'sanitize_callback'  => 'sanitize_text_field',
			'transport'          => $transport
		] );
		$wp_customize->add_control( $setting, [
			'section'  => $section,
			'label'    => __( 'Facebook URL', 'elysio' ),
			'type'     => 'text'
		] );

		$setting = 'socials_twitter_url';
		$wp_customize->add_setting( $setting, [
			'default'            => '',
			'sanitize_callback'  => 'sanitize_text_field',
			'transport'          => $transport
		] );
		$wp_customize->add_control( $setting, [
			'section'  => $section,
			'label'    => __( 'Twitter URL', 'elysio' ),
			'type'     => 'text'
		] );

		$setting = 'socials_instagram_url';
		$wp_customize->add_setting( $setting, [
			'default'            => '',
			'sanitize_callback'  => 'sanitize_text_field',
			'transport'          => $transport
		] );
		$wp_customize->add_control( $setting, [
			'section'  => $section,
			'label'    => __( 'Instagram URL', 'elysio' ),
			'type'     => 'text'
		] );

		$setting = 'socials_dribbble_url';
		$wp_customize->add_setting( $setting, [
			'default'            => '',
			'sanitize_callback'  => 'sanitize_text_field',
			'transport'          => $transport
		] );
		$wp_customize->add_control( $setting, [
			'section'  => $section,
			'label'    => __( 'Dribbble URL', 'elysio' ),
			'type'     => 'text'
		] );

		$setting = 'socials_behance_url';
		$wp_customize->add_setting( $setting, [
			'default'            => '',
			'sanitize_callback'  => 'sanitize_text_field',
			'transport'          => $transport
		] );
		$wp_customize->add_control( $setting, [
			'section'  => $section,
			'label'    => __( 'Behance URL', 'elysio' ),
			'type'     => 'text'
		] );

		$setting = 'socials_pinterest_url';
		$wp_customize->add_setting( $setting, [
			'default'            => '',
			'sanitize_callback'  => 'sanitize_text_field',
			'transport'          => $transport
		] );
		$wp_customize->add_control( $setting, [
			'section'  => $section,
			'label'    => __( 'Pinterest URL', 'elysio' ),
			'type'     => 'text'
		] );
		

		$setting = 'footer_copyright_text';
		$wp_customize->add_setting( $setting, [
			'default'            => 'Proudly powered by WordPress',
			'sanitize_callback'  => 'sanitize_text_field',
			'transport'          => $transport
		] );
		$wp_customize->add_control( $setting, [
			'section'  => $section,
			'label'    => 'Copyright',
			'type'     => 'text'
		] );
	}
}
add_action( 'customize_register', 'elysio_theme_customize_footer_register' );