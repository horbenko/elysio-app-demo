<?php
/**
 * Elysio Theme Customizer: Colors
 *
 * @package elysio-app
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;


if ( ! function_exists( 'elysio_theme_customize_color_register' ) ) {
	function elysio_theme_customize_color_register( $wp_customize ) {

		$setting = 'primary_color';
		$wp_customize->add_setting( $setting, [
			'default'     => '#2979FF',
			'transport'   => $transport
		] );
		$wp_customize->add_control(
			new WP_Customize_Color_Control( $wp_customize, $setting, [
				'label'    => __( 'Primary Color', 'elysio' ),
				'description'	=> __( 'For links, buttons and primary accent.', 'elysio' ),
				'section'  => 'colors',
				'settings' => $setting,
				'priority'	=>	1
			] )
		);

		$setting = 'accent_color';
		$wp_customize->add_setting( $setting, [
			//'default'     => '#2979FF',
			'transport'   => $transport
		] );
		$wp_customize->add_control(
			new WP_Customize_Color_Control( $wp_customize, $setting, [
				'label'    => __( 'Accent Color', 'elysio' ),
				'description'	=> __( 'For some hover, focus and secondary accent.', 'elysio' ),
				'section'  => 'colors',
				'settings' => $setting,
				'priority'	=>	1
			] )
		);

		$setting = 'headline_color';
		$wp_customize->add_setting( $setting, [
			'default'     => '#333',
			'transport'   => $transport
		] );
		$wp_customize->add_control(
			new WP_Customize_Color_Control( $wp_customize, $setting, [
				'label'    => __( 'Headline Color', 'elysio' ),
				'description'	=> __( 'For headlines and titles.', 'elysio' ),
				'section'  => 'colors',
				'settings' => $setting,
				'priority'	=>	1
			] )
		);

		$setting = 'text_color';
		$wp_customize->add_setting( $setting, [
			'default'     => '#333',
			'transport'   => $transport
		] );
		$wp_customize->add_control(
			new WP_Customize_Color_Control( $wp_customize, $setting, [
				'label'    => __( 'Main Text Color', 'elysio' ),
				'description'	=> __( 'For paragraphs and main content copies.', 'elysio' ),
				'section'  => 'colors',
				'settings' => $setting,
				'priority'	=>	1
			] )
		);

		$setting = 'secondary_text_color';
		$wp_customize->add_setting( $setting, [
			'default'     => '#888',
			'transport'   => $transport
		] );
		$wp_customize->add_control(
			new WP_Customize_Color_Control( $wp_customize, $setting, [
				'label'    => __( 'Secondary Text Color', 'elysio' ),
				'description'	=> __( 'For helpers and secondary content copies.', 'elysio' ),
				'section'  => 'colors',
				'settings' => $setting,
				'priority'	=>	1
			] )
		);

		$setting = 'decoration_color';
		$wp_customize->add_setting( $setting, [
			'default'     => '#D6D6D6',
			'transport'   => $transport
		] );
		$wp_customize->add_control(
			new WP_Customize_Color_Control( $wp_customize, $setting, [
				'label'    => __( 'Decorations Color', 'elysio' ),
				'description'	=> __( 'For decorations like lines.', 'elysio' ),
				'section'  => 'colors',
				'settings' => $setting,
				'priority'	=>	1
			] )
		);

		
		$setting = 'footer_background';
		$wp_customize->add_setting( $setting, [
			'default'     => '#D6D6D6',
			'transport'   => $transport
		] );
		$wp_customize->add_control(
			new WP_Customize_Color_Control( $wp_customize, $setting, [
				'label'    => __( 'Footer Background', 'elysio'),
				'section'  => 'colors',
				'settings' => $setting,
				'priority'	=>	1
			] )
		);


	}
}
add_action( 'customize_register', 'elysio_theme_customize_color_register' );




function customizer_color_style_tag(){

	$style = [];


	/* Background color */

	$style[] = '.preloader { background-color: #' . get_theme_mod( 'background_color' ) . ' !important; }';
	$style[] = '.instagram-hover-color:after { background-color: #' . get_theme_mod( 'background_color' ) . ' !important; }';




	/* Primary color */

	$style[] = 'a { color: ' . get_theme_mod( 'primary_color' ) . '; }';
	$style[] = '.site-header .collapse:not(.show) .menu-item.button .nav-link { color: ' . get_theme_mod( 'primary_color' ) . '; }';
	$style[] = '.bg-primary { background-color: ' . get_theme_mod( 'primary_color' ) . ' !important; }';
	$style[] = '.btn-primary { background-color: ' . get_theme_mod( 'primary_color' ) . '; border-color: ' . get_theme_mod( 'primary_color' ) . '; }';
	$style[] = '.site-header .navbar-collapse.show { background-color: ' . get_theme_mod( 'primary_color' ) . '; }';
	$style[] = '.elysio-button.button-color-scheme-primary { background-color: ' . get_theme_mod( 'primary_color' ) . '; color: ' . get_theme_mod( 'text_color' ) . ' !important; }';
	$style[] = '.elysio-button.button-color-scheme-white { color: ' . get_theme_mod( 'primary_color' ) . ' !important; }';
	$style[] = '.wpcf7.button-color-scheme-primary input[type=submit] { background-color: ' . get_theme_mod( 'primary_color' ) . ' !important; color: ' . get_theme_mod( 'text_color' ) . ' !important; }';
	$style[] = '.wpcf7.button-color-scheme-white input[type=submit] { color: ' . get_theme_mod( 'primary_color' ) . ' !important; }';




	/* Accent color */
	$style[] = 'a:hover { color: ' . get_theme_mod( 'accent_color' ) . '; }';
	$style[] = '.btn-primary:hover, .btn-primary:active { background-color: ' . get_theme_mod( 'accent_color' ) . '; border-color: ' . get_theme_mod( 'accent_color' ) . '; }';
	$style[] = '.btn-primary:not(:disabled):not(.disabled).active, .btn-primary:not(:disabled):not(.disabled):active, .elysio-button:not(:disabled):not(.disabled).active, .elysio-button:not(:disabled):not(.disabled):active { background-color: ' . get_theme_mod( 'accent_color' ) . '; border-color: ' . get_theme_mod( 'accent_color' ) . '; }';
	$style[] = 'body.archive .entry-content a:hover, body.archive .entry-footer a:hover, body.archive .entry-meta a:hover, body.home.blog .entry-content a:hover, body.home.blog .entry-footer a:hover, body.home.blog .entry-meta a:hover { color: ' . get_theme_mod( 'accent_color' ) . '; }';
	$style[] = 'body.post-template-default.single-post .single-article .entry-meta a:hover { color: ' . get_theme_mod( 'accent_color' ) . '; }';
	$style[] = '.elysio-button.button-color-scheme-secondary { background-color: ' . get_theme_mod( 'accent_color' ) . ';  color: ' . get_theme_mod( 'primary_color' ) . ' !important; }';
	$style[] = '.wpcf7.button-color-scheme-secondary input[type=submit] { background-color: ' . get_theme_mod( 'accent_color' ) . ' !important;  color: ' . get_theme_mod( 'primary_color' ) . ' !important; }';

	$style[] = 'form.form-rounded .input-group:focus-within { border-color: ' . get_theme_mod( 'accent_color' ) . '!important ;}';
	$style[] = 'form.form-rounded .input-group .btn.btn-primary:hover svg path { fill: ' . get_theme_mod( 'accent_color' ) . '!important ;}';

	



	/* Headline color */

	$style[] = '.elysio-headline-color { color: ' . get_theme_mod( 'headline_color' ) . ' !important; }';
	$style[] = 'body:not(.elementor-page) h1, body:not(.elementor-page) h2, body:not(.elementor-page) h3, body:not(.elementor-page) h4, body:not(.elementor-page) h5, body:not(.elementor-page) h6 { color: ' . get_theme_mod( 'headline_color' ) . ' !important; }';




	/* Main Text color */

	$style[] = '.elysio-main-text-color, .elysio-text-color a { color: ' . get_theme_mod( 'text_color' ) . ' !important; }';

	$style[] = '.elysio-text-color, .elysio-text-color a { color: ' . get_theme_mod( 'text_color' ) . ' !important; }';
	$style[] = 'body:not(.elementor-page), body:not(.elementor-page) caption { color: ' . get_theme_mod( 'text_color' ) . ' !important; }';

	$style[] = '.site-footer .site-info { color: ' . get_theme_mod( 'text_color' ) . '; }';
	$style[] = '.site-footer .site-info a { color: ' . get_theme_mod( 'text_color' ) . '; }';
	$style[] = '.site-footer .site-info a:hover { color: ' . get_theme_mod( 'primary_color' ) . '; }';




	/* Secondary Text Color */ 

	$style[] = '.elysio-secondary-text-color, .elysio-secondary-color a { color: ' . get_theme_mod( 'secondary_text_color' ) . ' !important; }';

	$style[] = 'body.archive .widget-area .widget_recent_entries .post-date, body.attachment .widget-area .widget_recent_entries .post-date, body.home.blog .widget-area .widget_recent_entries .post-date, body.page-template-default .widget-area .widget_recent_entries .post-date { color: ' . get_theme_mod( 'secondary_text_color' ) . '; }';
	$style[] = 'body.archive .widget-area .widget_archive ul li, body.archive .widget-area .widget_categories ul li, body.attachment .widget-area .widget_archive ul li, body.attachment .widget-area .widget_categories ul li, body.home.blog .widget-area .widget_archive ul li, body.home.blog .widget-area .widget_categories ul li, body.page-template-default .widget-area .widget_archive ul li, body.page-template-default .widget-area .widget_categories ul li { color: ' . get_theme_mod( 'secondary_text_color' ) . '; }';
	$style[] = 'body.post-template-default.single-post .single-article .entry-meta, body.post-template-default.single-post .single-article .entry-meta a { color: ' . get_theme_mod( 'secondary_text_color' ) . '; }';
	$style[] = 'body.archive .widget-area .widget_calendar .wp-calendar-table, body.attachment .widget-area .widget_calendar .wp-calendar-table, body.home.blog .widget-area .widget_calendar .wp-calendar-table, body.page-template-default .widget-area .widget_calendar .wp-calendar-table { color: ' . get_theme_mod( 'secondary_text_color' ) . '; }';
	$style[] = 'body.archive .entry-content a, body.archive .entry-footer a, body.archive .entry-meta a, body.home.blog .entry-content a, body.home.blog .entry-footer a, body.home.blog .entry-meta a { color: ' . get_theme_mod( 'secondary_text_color' ) . '; }';
	$style[] = '.elysio-blog-list a { color: ' . get_theme_mod( 'secondary_text_color' ) . '; }';
	$style[] = '.comment-metadata, .comment-metadata a { color: ' . get_theme_mod( 'secondary_text_color' ) . '; }';
	$style[] = 'body.post-template-default.single-post .single-article .tags-links a { color: ' . get_theme_mod( 'text_color' ) . '; }';




	/* Decoration color */

	$style[] = '.elysio-decoration-color { border-color: ' . get_theme_mod( 'decoration_color' ) . ' !important; color: #' . get_theme_mod( 'background_color' ) . ' !important; }';
	$style[] = 'body.post-template-default.single-post .single-article .tags-links a { border-color: ' . get_theme_mod( 'decoration_color' ) . ' !important; background-color: ' . get_theme_mod( 'decoration_color' ) . ' !important; }';
	$style[] = 'body.post-template-default.single-post .single-article .entry-footer, body.post-template-default.single-post .single-article .entry-title, .comment-reply-title, .comments-title { border-color: ' . get_theme_mod( 'decoration_color' ) . ' !important; }';
	$style[] = 'body.archive .widget-area aside.widget, body.attachment .widget-area aside.widget, body.home.blog .widget-area aside.widget, body.page-template-default .widget-area aside.widget { border-color: ' . get_theme_mod( 'decoration_color' ) . ' !important; }';
	$style[] = '#wrapper-footer-full { background-color: ' . get_theme_mod( 'decoration_color' ) . ' !important; }';
	$style[] = '.elysio-recent-posts-widget li { border-color: ' . get_theme_mod( 'decoration_color' ) . ' !important; }';
	$style[] = '.socials-share-wrap .socials-share-list .socials-share-link { border-color: ' . get_theme_mod( 'decoration_color' ) . ' !important; }';
	$style[] = '.socials-share-wrap .socials-share-list .socials-share-link.is-email svg path { fill: ' . get_theme_mod( 'decoration_color' ) . ' !important; }';





	/* Forms */

	$style[] = 'form { color: ' . get_theme_mod( 'text_color' ) . ' !important; }';
	$style[] = 'form.form-rounded .input-group { border-color: ' . get_theme_mod( 'decoration_color' ) . ' !important; }';
	$style[] = '.site-footer #wrapper-footer-full .widget_archive select, .site-footer #wrapper-footer-full .widget_categories select, body.archive .widget-area .widget_archive select, body.archive .widget-area .widget_categories select, body.attachment .widget-area .widget_archive select, body.attachment .widget-area .widget_categories select, body.home.blog .widget-area .widget_archive select, body.home.blog .widget-area .widget_categories select, body.page-template-default .widget-area .widget_archive select, body.page-template-default .widget-area .widget_categories select { border-color: ' . get_theme_mod( 'decoration_color' ) . ' !important; }';
	$style[] = '.form-control, .widget_archive select, .widget_categories select, .wpcf7 .wpcf7-validation-errors, .wpcf7 input[type=color], .wpcf7 input[type=date], .wpcf7 input[type=datetime-local], .wpcf7 input[type=datetime], .wpcf7 input[type=email], .wpcf7 input[type=file], .wpcf7 input[type=month], .wpcf7 input[type=number], .wpcf7 input[type=range], .wpcf7 input[type=search], .wpcf7 input[type=submit], .wpcf7 input[type=tel], .wpcf7 input[type=text], .wpcf7 input[type=time], .wpcf7 input[type=url], .wpcf7 input[type=week], .wpcf7 select, .wpcf7 textarea, .wpcf7 form.form-rounded input.btn-primary[type=submit], form.form-rounded .btn.btn-primary, form.form-rounded .wpcf7 input.btn-primary[type=submit] { background-color: #' . get_theme_mod( 'background_color' ) . ' !important; color: ' . get_theme_mod( 'text_color' ) . ' !important; border-color: ' . get_theme_mod( 'decoration_color' ) . ' !important; }';
	$style[] = '.wpcf7 form.form-rounded input.btn-primary[type=submit] svg path, form.form-rounded .btn.btn-primary svg path, form.form-rounded .wpcf7 input.btn-primary[type=submit] svg path { fill: ' . get_theme_mod( 'text_color' ) . ' !important; }';




	/* Pagination */

	$style[] = '.pagination .page-item.active .page-link { color: ' . get_theme_mod( 'secondary_text_color' ) . '; border-color: ' . get_theme_mod( 'secondary_text_color' ) . '; }';
	$style[] = '.pagination .page-link { color: ' . get_theme_mod( 'secondary_text_color' ) . '; opacity: 0.8; }';
	$style[] = '.pagination .page-link:hover { color: ' . get_theme_mod( 'text_color' ) . '; opacity: 1; }';
	$style[] = '.pagination .page-link svg { fill: ' . get_theme_mod( 'secondary_text_color' ) . '; }';
	$style[] = '.pagination .page-link:hover svg{ fill: ' . get_theme_mod( 'text_color' ) . '; }';




	/* Footer Socials */

	$style[] = '.site-footer .elysio-postfooter .footer-socials li { border-color: ' . get_theme_mod( 'decoration_color' ) . '; }';
	$style[] = '.footer-socials path { fill: ' . get_theme_mod( 'secondary_text_color' ) . '; }';


	echo "<style>\n" . implode( "\n", $style ) . "\n</style>\n";
}

add_action( 'wp_head', 'customizer_color_style_tag' );