<?php
/**
 * AJAX Search for site header search popup
 *
 * @package elysio-app
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

// the ajax function
add_action('wp_ajax_data_fetch' , 'elysio_search_data_fetch');
add_action('wp_ajax_nopriv_data_fetch','elysio_search_data_fetch');

function elysio_search_data_fetch(){

    $the_query = new WP_Query( array( 'posts_per_page' => -1, 's' => esc_attr( $_POST['keyword'] ) ) );
    if( $the_query->have_posts() ){
        while( $the_query->have_posts() ): $the_query->the_post(); ?>

            <h2><a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_title();?></a></h2>

        <?php endwhile;
        wp_reset_postdata();  
    }else{
      ?>
        <h2>Nothing Found</h2>
      <?php
    }

    die();
}
