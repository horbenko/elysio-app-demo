<?php
/**
 * Elysio Theme Customizer
 *
 * @package elysio-app
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
if ( ! function_exists( 'elysio_customize_register' ) ) {
	/**
	 * Register basic customizer support.
	 *
	 * @param object $wp_customize Customizer reference.
	 */
	function elysio_customize_register( $wp_customize ) {
		$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
		$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
		$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	}
}
add_action( 'customize_register', 'elysio_customize_register' );

if ( ! function_exists( 'elysio_theme_customize_register' ) ) {
	/**
	 * Register individual settings through customizer's API.
	 *
	 * @param WP_Customize_Manager $wp_customize Customizer reference.
	 */
	function elysio_theme_customize_register( $wp_customize ) {

		/**
		 * Select sanitization function
		 *
		 * @param string               $input   Slug to sanitize.
		 * @param WP_Customize_Setting $setting Setting instance.
		 * @return string Sanitized slug if it is a valid choice; otherwise, the setting default.
		 */
		function elysio_theme_slug_sanitize_select( $input, $setting ) {

			// Ensure input is a slug (lowercase alphanumeric characters, dashes and underscores are allowed only).
			$input = sanitize_key( $input );

			// Get the list of possible select options.
			$choices = $setting->manager->get_control( $setting->id )->choices;

			// If the input is a valid key, return it; otherwise, return the default.
			return ( array_key_exists( $input, $choices ) ? $input : $setting->default );

		}


		// Theme style settings.
		$wp_customize->add_section(
			'elysio_theme_options',
			array(
				'title'       => __( 'Theme Options', 'elysio' ),
				'capability'  => 'edit_theme_options',
				'priority'    => apply_filters( 'elysio_theme_options_priority', 160 ),
			)
		);


		$section = 'elysio_theme_options';

		$setting = 'elysio_container_type';
		$wp_customize->add_setting(
			$setting,
			array(
				'default'           => 'container',
				'type'              => 'theme_mod',
				'sanitize_callback' => 'elysio_theme_slug_sanitize_select',
				'capability'        => 'edit_theme_options',
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				$setting,
				array(
					'label'       => __( 'Container Width', 'elysio' ),
					'section'     => $section,
					'settings'    => $setting,
					'type'        => 'select',
					'choices'     => array(
						'container'       => __( 'Fixed width container', 'elysio' ),
						'container-fluid' => __( 'Full width container', 'elysio' ),
					),
					'priority'    => apply_filters( 'elysio_container_type_priority', 10 ),
				)
			)
		);

		$setting = 'elysio_sidebar_position';
		$wp_customize->add_setting(
			$setting,
			array(
				'default'           => 'right',
				'type'              => 'theme_mod',
				'sanitize_callback' => 'sanitize_text_field',
				'capability'        => 'edit_theme_options',
			)
		);
		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				$setting,
				array(
					'label'             => __( 'Sidebar Positioning', 'elysio' ),
					'section'           => $section,
					'settings'          => $setting,
					'type'              => 'select',
					'sanitize_callback' => 'elysio_theme_slug_sanitize_select',
					'choices'           => array(
						'right' => __( 'Right sidebar', 'elysio' ),
						'left'  => __( 'Left sidebar', 'elysio' ),
						'none'  => __( 'No sidebar', 'elysio' ),
					),
					'priority'          => apply_filters( 'elysio_sidebar_position_priority', 10 ),
				)
			)
		);

		$setting = 'elysio_preloader';
		$wp_customize->add_setting( $setting, [
			'default'    =>  'true',
			'transport'  =>  $transport
		] );
		$wp_customize->add_control( $setting, [
			'section' => $section,
			'label'   => __( 'Show Site Preloader', 'elysio' ),
			'type'    => 'checkbox',
		] );

		$setting = 'elysio_theme_font_link';
		$wp_customize->add_setting( $setting, [
			'default'            => 'https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,400;0,500;0,700;1,400&display=swap',
			'sanitize_callback'    => 'sanitize_text_field',
			'transport'          => $transport
		] );
		$wp_customize->add_control( $setting, [
			'section'  => $section,
			'label'    => 'Google Fonts Link',
			'type'     => 'text',
			'description'	=> 'Check <a href="https://fonts.google.com/" target="_blnk">fonts.google.com</a> for more fonts. Example how it should be: https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,400;0,500;0,700;1,400&display=swap'
		] );

		$setting = 'elysio_theme_font';
		$wp_customize->add_setting( $setting, [
			'default'            => 'Poppins',
			'sanitize_callback'  => 'sanitize_text_field',
			'transport'          => $transport
		] );
		$wp_customize->add_control( $setting, [
			'section'  => $section,
			'label'    => __( 'Font Family', 'elysio' ),
			'type'     => 'text'
		] );

		$setting = 'google_maps_api';
		$wp_customize->add_setting( $setting, [
			'default'            => '',
			'sanitize_callback'  => 'sanitize_text_field',
			'transport'          => $transport
		] );
		$wp_customize->add_control( $setting, [
			'section'  => $section,
			'label'    => __('Google Map API Key', 'elysio' ),
			'type'     => 'text'
		] );


	}
} // End of if function_exists( 'elysio_theme_customize_register' ).
add_action( 'customize_register', 'elysio_theme_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
if ( ! function_exists( 'elysio_customize_preview_js' ) ) {
	/**
	 * Setup JS integration for live previewing.
	 */
	function elysio_customize_preview_js() {
		wp_enqueue_script(
			'elysio_customizer',
			get_template_directory_uri() . '/js/customizer.js',
			array( 'customize-preview' ),
			'20130508',
			true
		);
	}
}
add_action( 'customize_preview_init', 'elysio_customize_preview_js' );


function customizer_style_tag(){

	/* Fonts */

	if( $elysio_theme_font_link = get_theme_mod( 'elysio_theme_font_link' ) ){
		wp_enqueue_style( 'elysio-gfonts', $elysio_theme_font_link, false, false );
	}else{
		wp_enqueue_style( 'elysio-gfonts', 'https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,400;0,500;0,700;1,400&display=swap', false, false );
	}

	$style = [];

	$body_styles = [];

	if( get_theme_mod( 'elysio_theme_font' ) ){
		$body_styles[] = 'font-family: ' . get_theme_mod( 'elysio_theme_font' );
	}else{
		$body_styles[] = 'font-family: "Poppins"';
	}

	$style[] = 'body { '. implode( ' ', $body_styles ) .' }';


	echo "<style>\n" . implode( "\n", $style ) . "\n</style>\n";
}

add_action( 'wp_head', 'customizer_style_tag' );


// https://www.pinterest.com/elysio_team/
// https://www.facebook.com/elysio.theme/