<?php
/**
 * Custom hooks
 *
 * @package elysio-app
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'elysio_site_info' ) ) {
	/**
	 * Add site info hook to WP hook library.
	 */
	function elysio_site_info() {
		do_action( 'elysio_site_info' );
	}
}

add_action( 'elysio_site_info', 'elysio_add_site_info' );
if ( ! function_exists( 'elysio_add_site_info' ) ) {
	/**
	 * Add site info content.
	 */
	function elysio_add_site_info() {
		$the_theme = wp_get_theme();

		if( ! $site_info = get_theme_mod( 'footer_copyright_text' ) ){

			$site_info = sprintf(
				'<a href="%1$s">%2$s</a><span class="sep"> | </span>%3$s',
				esc_url( __( 'http://wordpress.org/', 'elysio' ) ),
				sprintf(
					/* translators: WordPress */
					esc_html__( 'Proudly powered by %s', 'elysio' ),
					'WordPress'
				),
				sprintf( // WPCS: XSS ok.
					/* translators: 1: Theme name, 2: Theme author */
					esc_html__( 'Theme: %1$s by %2$s.', 'elysio' ),
					$the_theme->get( 'Name' ),
					'<a href="' . esc_url( __( 'https://elysio.top', 'elysio' ) ) . '">elysio.top</a>'
				)
			);
			
		}

		echo apply_filters( 'elysio_site_info_content', $site_info ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	}
}
