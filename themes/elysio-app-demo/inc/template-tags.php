<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package elysio-app
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'elysio_posted_on' ) ) {
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function elysio_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s"> (%4$s) </time>';
		}
		$time_string = sprintf(
			$time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);
		$posted_on   = apply_filters(
			'elysio_posted_on',
			sprintf(
				'<span class="posted-on">%1$s <a href="%2$s" rel="bookmark">%3$s</a></span>',
				esc_html_x( 'Posted on', 'post date', 'elysio' ),
				esc_url( get_permalink() ),
				apply_filters( 'elysio_posted_on_time', $time_string )
			)
		);
		$byline      = apply_filters(
			'elysio_posted_by',
			sprintf(
				'<span class="byline"> %1$s<span class="author vcard"> <a class="url fn n" href="%2$s">%3$s</a></span></span>',
				$posted_on ? esc_html_x( 'by', 'post author', 'elysio' ) : esc_html_x( 'Posted by', 'post author', 'elysio' ),
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
				esc_html( get_the_author() )
			)
		);
		echo $posted_on . $byline; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	}
}

if ( ! function_exists( 'elysio_entry_footer' ) ) {
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function elysio_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ', ', 'elysio' ) );
			if ( $categories_list && elysio_categorized_blog() ) {
				/* translators: %s: Categories of current post */
				printf( '<span class="cat-links">' . esc_html__( 'Posted in %s', 'elysio' ) . '</span>', $categories_list ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			}
			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', esc_html__( ', ', 'elysio' ) );
			if ( $tags_list ) {
				/* translators: %s: Tags of current post */
				printf( '<span class="tags-links">' . esc_html__( 'Tagged %s', 'elysio' ) . '</span>', $tags_list ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			}
		}
		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			comments_popup_link( esc_html__( 'Leave a comment', 'elysio' ), esc_html__( '1 Comment', 'elysio' ), esc_html__( '% Comments', 'elysio' ) );
			echo '</span>';
		}
		edit_post_link(
			sprintf(
				/* translators: %s: Name of current post */
				esc_html__( 'Edit %s', 'elysio' ),
				the_title( '<span class="sr-only">"', '"</span>', false )
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
}




if ( ! function_exists( 'elysio_categorized_blog' ) ) {
	/**
	 * Returns true if a blog has more than 1 category.
	 *
	 * @return bool
	 */
	function elysio_categorized_blog() {
		$all_the_cool_cats = get_transient( 'elysio_categories' );
		if ( false === $all_the_cool_cats ) {
			// Create an array of all the categories that are attached to posts.
			$all_the_cool_cats = get_categories(
				array(
					'fields'     => 'ids',
					'hide_empty' => 1,
					// We only need to know if there is more than one category.
					'number'     => 2,
				)
			);
			// Count the number of categories that are attached to the posts.
			$all_the_cool_cats = count( $all_the_cool_cats );
			set_transient( 'elysio_categories', $all_the_cool_cats );
		}
		if ( $all_the_cool_cats > 1 ) {
			// This blog has more than 1 category so elysio_categorized_blog should return true.
			return true;
		} else {
			// This blog has only 1 category so elysio_categorized_blog should return false.
			return false;
		}
	}
}

add_action( 'edit_category', 'elysio_category_transient_flusher' );
add_action( 'save_post', 'elysio_category_transient_flusher' );

if ( ! function_exists( 'elysio_category_transient_flusher' ) ) {
	/**
	 * Flush out the transients used in elysio_categorized_blog.
	 */
	function elysio_category_transient_flusher() {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}
		// Like, beat it. Dig?
		delete_transient( 'elysio_categories' );
	}
}




if ( ! function_exists( 'elysio_body_attributes' ) ) {
	/**
	 * Displays the attributes for the body element.
	 */
	function elysio_body_attributes() {
		/**
		 * Filters the body attributes.
		 *
		 * @param array $atts An associative array of attributes.
		 */
		$atts = array_unique( apply_filters( 'elysio_body_attributes', $atts = array() ) );
		if ( ! is_array( $atts ) || empty( $atts ) ) {
			return;
		}
		$attributes = '';
		foreach ( $atts as $name => $value ) {
			if ( $value ) {
				$attributes .= sanitize_key( $name ) . '="' . esc_attr( $value ) . '" ';
			} else {
				$attributes .= sanitize_key( $name ) . ' ';
			}
		}
		echo trim( $attributes ); // phpcs:ignore WordPress.Security.EscapeOutput
	}
}




if ( ! function_exists( 'elysio_entry_tags' ) ) {
	function elysio_entry_tags() {
		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', '' );
		if ( $tags_list ) {
			/* translators: %s: Tags of current post */
			echo '<div class="tags-title elysio-headline-color">Tags</div>';
			echo '<div class="tags-links">' . $tags_list . '</div>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		}
	}
}




if ( ! function_exists( 'elysio_entry_categories' ) ) {
	function elysio_entry_categories() {

		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'elysio' ) );
		if ( $categories_list && elysio_categorized_blog() ) {
			/* translators: %s: Categories of current post */
			printf( '<div class="cat-links elysio-headline-color">' . esc_html__( 'Posted in %s', 'elysio' ) . '</div>', $categories_list ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		}
	}
}




if ( ! function_exists( 'elysio_header_search' ) ) {

	/*  Site Header Search Button & Popup -------------------------------- */

	function elysio_header_search() {

		//if( get_theme_mod( 'header_show_search' ) || get_theme_mod( 'header_show_search_mobile' ) ){
			?>

			<div class="header_search-button" id="hssm"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"/><path d="M0 0h24v24H0z" fill="none"/></svg></div>

			<?php
		//}

		// add the ajax fetch js
		add_action( 'wp_footer', 'header_search_ajax_fetch' );
		
		function header_search_ajax_fetch() {
			?>
			<div class="header_search-popup" id="headerSearch">
				<div class="header_search-popup_content">
					<div class="row">
						<div class="col-12">
							<div id="hscm" class="header_search-close_button"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"/><path d="M0 0h24v24H0z" fill="none"/></svg></div>

							<?php get_search_form(); ?>

							<div id="headerSearchResult" class="header_search-result"></div>
						</div>
					</div>
				</div>
			</div>
			<script type="text/javascript">
				document.querySelector('#headerSearch input').onkeyup = header_search_ajax_fetch;
				document.querySelector('#headerSearch input').onfocus = header_search_ajax_fetch;
				document.querySelector('#headerSearch input').onblur = header_search_ajax_fetch;
				function header_search_ajax_fetch(){
					if( jQuery('#headerSearch input').val().trim().length > 0 ){
						jQuery('#headerSearchResult').show()
						jQuery.ajax({
							url: '<?php echo admin_url('admin-ajax.php'); ?>',
							type: 'post',
							data: { action: 'data_fetch', keyword: jQuery('#headerSearch input').val().trim() },
							success: function(data) {
								jQuery('#headerSearchResult').html( data );
							}
						});
					}else{
						jQuery('#headerSearchResult').hide()
						jQuery('#headerSearchResult').html('');
					}
				}
			</script>
			<?php
		}

	}
}




if ( ! function_exists( 'elysio_share_links' ) ) {

	/*  Socials Share links (w/o JS) -------------------------------- */
	/* https://crunchify.com/how-to-create-social-sharing-button-without-any-plugin-and-script-loading-wordpress-speed-optimization-goal/ */

	function elysio_share_links(){

		global $post;

		$content = '';

		// Get current page URL 
		$shareURL = urlencode(get_permalink());
 
		// Get current page title
		$shareTitle = htmlspecialchars(urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')), ENT_COMPAT, 'UTF-8');
		// $shareTitle = str_replace( ' ', '%20', get_the_title());
		
		// Get Post Thumbnail for pinterest
		$shareThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
 
		// Construct sharing URL without using any script
		$twitterURL = 'https://twitter.com/intent/tweet?text='.$shareTitle.'&amp;url='.$shareURL;
		$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$shareURL;
		$linkedInURL = 'https://www.linkedin.com/shareArticle?mini=true&url='.$shareURL.'&amp;title='.$shareTitle;
 
		// Based on popular demand added Pinterest too
		$pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$shareURL.'&amp;media='.$shareThumbnail[0].'&amp;description='.$shareTitle;
 
		// Add sharing button at the end of page/page content

		$content .= '<div class="socials-share-wrap">';
			$content .= '<div class="socials-share-title elysio-headline-color">Share</div>';
			$content .= '<div class="socials-share-list">';
				$content .= '<a class="socials-share-link is-facebook" href="'.$facebookURL.'" target="_blank"><svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12 2.04c-5.5 0-10 4.49-10 10.02 0 5 3.66 9.15 8.44 9.9v-7H7.9v-2.9h2.54V9.85c0-2.51 1.49-3.89 3.78-3.89 1.09 0 2.23.19 2.23.19v2.47h-1.26c-1.24 0-1.63.77-1.63 1.56v1.88h2.78l-.45 2.9h-2.33v7a10 10 0 008.44-9.9c0-5.53-4.5-10.02-10-10.02z" fill="#fff"/></svg> Facebook</a>';
				$content .= '<a class="socials-share-link is-pinterest" href="'.$pinterestURL.'" data-pin-custom="true" target="_blank"><svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.04 21.54c.96.29 1.93.46 2.96.46A10 10 0 102 12c0 4.25 2.67 7.9 6.44 9.34-.09-.78-.18-2.07 0-2.96l1.15-4.94s-.29-.58-.29-1.5c0-1.38.86-2.41 1.84-2.41.86 0 1.26.63 1.26 1.44 0 .86-.57 2.09-.86 3.27-.17.98.52 1.84 1.52 1.84 1.78 0 3.16-1.9 3.16-4.58 0-2.4-1.72-4.04-4.19-4.04-2.82 0-4.48 2.1-4.48 4.31 0 .86.28 1.73.74 2.3.09.06.09.14.06.29l-.29 1.09c0 .17-.11.23-.28.11-1.28-.56-2.02-2.38-2.02-3.85 0-3.16 2.24-6.03 6.56-6.03 3.44 0 6.12 2.47 6.12 5.75 0 3.44-2.13 6.2-5.18 6.2-.97 0-1.92-.52-2.26-1.13l-.67 2.37c-.23.86-.86 2.01-1.29 2.7v-.03z" fill="#fff"/></svg> Pin It</a>';
				$content .= '<a class="socials-share-link is-twitter" href="'. $twitterURL .'" target="_blank"><svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M22.46 6c-.77.35-1.6.58-2.46.69.88-.53 1.56-1.37 1.88-2.38-.83.5-1.75.85-2.72 1.05C18.37 4.5 17.26 4 16 4c-2.35 0-4.27 1.92-4.27 4.29 0 .34.04.67.11.98C8.28 9.09 5.11 7.38 3 4.79c-.37.63-.58 1.37-.58 2.15 0 1.49.75 2.81 1.91 3.56-.71 0-1.37-.2-1.95-.5v.03c0 2.08 1.48 3.82 3.44 4.21a4.22 4.22 0 01-1.93.07 4.28 4.28 0 004 2.98 8.521 8.521 0 01-5.33 1.84c-.34 0-.68-.02-1.02-.06C3.44 20.29 5.7 21 8.12 21 16 21 20.33 14.46 20.33 8.79c0-.19 0-.37-.01-.56.84-.6 1.56-1.36 2.14-2.23z" fill="#fff"/></svg> Twitter</a>';
				$content .= '<a class="socials-share-link is-linkedin" href="'.$linkedInURL.'" target="_blank"><svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M19 3a2 2 0 012 2v14a2 2 0 01-2 2H5a2 2 0 01-2-2V5a2 2 0 012-2h14zm-.5 15.5v-5.3a3.26 3.26 0 00-3.26-3.26c-.85 0-1.84.52-2.32 1.3v-1.11h-2.79v8.37h2.79v-4.93c0-.77.62-1.4 1.39-1.4a1.4 1.4 0 011.4 1.4v4.93h2.79zM6.88 8.56a1.68 1.68 0 001.68-1.68c0-.93-.75-1.69-1.68-1.69a1.69 1.69 0 00-1.69 1.69c0 .93.76 1.68 1.69 1.68zm1.39 9.94v-8.37H5.5v8.37h2.77z" fill="#fff"/></svg> LinkedIn</a>';
				$content .= '<a class="socials-share-link is-email" href="mailto:?subject='.$shareTitle.'&body='.$shareURL.'" target="_blank"><svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm-.4 4.25l-7.07 4.42c-.32.2-.74.2-1.06 0L4.4 8.25a.85.85 0 11.9-1.44L12 11l6.7-4.19a.85.85 0 11.9 1.44z" fill="#888"/></svg> Email</a>';
			$content .= '</div>';
		$content .= '</div>';

		echo $content;

	}
}




if ( ! function_exists( 'elysio_recent_posts' ) ) {

	/*  Recent Posts for Single Post -------------------------------- */
	function elysio_recent_posts(){
		global $post;
		$categories = get_the_category();
		//print_r($categories);
		$cat_link = get_category_link( $categories[0]->term_id );

		$rp_query = new WP_Query(array(
			'posts_per_page'        => '3',
			'post__not_in'          => array( $post->ID ),
			//'cat'                   => $categories[0]->term_id,
			'orderby'        => 'rand',
		));

		if( $rp_query->have_posts() ){
			echo '<div class="elysio-blog-list related-articles">';
				echo '<h2 class="related-articles__title elysio-headline-color">Recent Posts</h2>';
				while( $rp_query->have_posts() ){
					$rp_query->the_post();
					get_template_part( 'loop-templates/content', 'recent' );
				}
				if (  $rp_query->max_num_pages > 1 ){
					echo '<a href="'.$cat_link.'" class="btn btn-primary loadmore js-loadmore">View more</a>';
				}
			echo '</div>';
		}
		wp_reset_postdata();
	}
}




if ( ! function_exists( 'elysio_preloader' ) ) {
	function elysio_preloader(){
		if( get_theme_mod('elysio_preloader', 'true') ){
			echo '<div class="preloader" id="preloader"><div class="preloader__item-1"></div><div class="preloader__item-2"></div><div class="preloader__item-3"></div><div class="preloader__item-4"></div><div class="preloader__item-5"></div></div>';
		}
	}
}




if ( ! function_exists( 'elysio_footer_logo' ) ) {
	function elysio_footer_logo(){
		if( get_theme_mod('elysio_footer_logo_display', true) == true ){
			echo '<a href="/" class="footer-logo">';
				if( $footer_logo = get_theme_mod( 'elysio_footer_logo' ) ){
					echo "<img src='". $footer_logo ."''>";
				}else{
					echo "<img src='". get_stylesheet_directory_uri() . "/assets/elysio-logo.png'>";
				}
			echo '</a>';
		}
	}
}





if ( ! function_exists( 'elysio_footer_socials' ) ) {
	function elysio_footer_socials(){
		if( get_theme_mod( 'socials_facebook_url' ) || get_theme_mod( 'socials_twitter_url' ) || get_theme_mod( 'socials_instagram_url' ) || get_theme_mod( 'socials_dribbble_url' ) || get_theme_mod( 'socials_behance_url' ) ){
			echo '
			<div class="col-md-6">
				<ul class="footer-socials d-flex justify-content-end">' .
					( ( $socials_facebook_url = get_theme_mod( 'socials_facebook_url' ) ) ? '
						<li class="facebook-hover-color"><a href="' . $socials_facebook_url . '" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40"><g transform="translate(-1042 -831)"><path data-name="facebook" d="M1066 843v3.2h-1.6c-.552 0-.8.648-.8 1.2v2h2.4v3.2h-2.4v6.4h-3.2v-6.4h-2.4v-3.2h2.4v-3.2a3.2 3.2 0 013.2-3.2z" /></g></svg> Facebook</a></li>' : '' ) .
					( ( $socials_twitter_url = get_theme_mod( 'socials_twitter_url' ) ) ? '
						<li class="twitter-hover-color"><a href="' . $socials_twitter_url .'" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40"><g transform="translate(-1094 -831)"><path data-name="twitter" d="M1122 846.53a6.553 6.553 0 01-1.881.528 3.288 3.288 0 001.441-1.821 6.609 6.609 0 01-2.08.8 3.231 3.231 0 00-2.42-1.037 3.277 3.277 0 00-3.266 3.281 3.41 3.41 0 00.084.75 9.326 9.326 0 01-6.761-3.431 3.268 3.268 0 001.017 4.371 3.245 3.245 0 01-1.491-.382v.023a3.284 3.284 0 002.631 3.22 3.217 3.217 0 01-1.476.054 3.273 3.273 0 003.062 2.278 6.517 6.517 0 01-4.08 1.408 6.67 6.67 0 01-.78-.046 9.334 9.334 0 0014.371-7.863c0-.145 0-.283-.008-.428a6.556 6.556 0 001.637-1.705z" /></g></svg> Twitter</a></li>' : '' ) .
					( ( $socials_instagram_url = get_theme_mod( 'socials_instagram_url' ) ) ? '
						<li class="instagram-hover-color"><a href="' . $socials_instagram_url . '" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40">
							<defs>
							  <linearGradient id="instagram-gradient" x1="0%" y1="0%" x2="100%" y2="0%">
							    <stop stop-color="#fdf497" offset="0" />
							    <stop stop-color="#fdf497" offset="0.05" />
							    <stop stop-color="#fd5949" offset="0.45" />
							    <stop stop-color="#d6249f" offset="0.6" />
							    <stop stop-color="#285AEB" offset="0.9" />
							  </linearGradient>
							</defs>
							<g transform="translate(-1146 -831)"><path data-name="instagram" d="M1162.64 843h6.72a4.643 4.643 0 014.64 4.64v6.72a4.64 4.64 0 01-4.64 4.64h-6.72a4.643 4.643 0 01-4.64-4.64v-6.72a4.64 4.64 0 014.64-4.64m-.16 1.6a2.88 2.88 0 00-2.88 2.88v7.04a2.878 2.878 0 002.88 2.88h7.04a2.88 2.88 0 002.88-2.88v-7.04a2.878 2.878 0 00-2.88-2.88h-7.04m7.72 1.2a1 1 0 11-1 1 1 1 0 011-1m-4.2 1.2a4 4 0 11-4 4 4 4 0 014-4m0 1.6a2.4 2.4 0 102.4 2.4 2.4 2.4 0 00-2.4-2.4z" /></g></svg> Instagram</a></li>' : '' ) .
					( ( $socials_dribbble_url = get_theme_mod( 'socials_dribbble_url' ) ) ? '
							<li class="dribbble-hover-color"><a href="' . $socials_dribbble_url .'" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40"><g transform="translate(-1198 -831)"><path data-name="dribbble" d="M1221.536 856.136a46 46 0 00-1.136-4.2 8.874 8.874 0 011.264-.088h.016a11.186 11.186 0 012.448.3 6.231 6.231 0 01-2.592 3.992m-3.536 1.1a6.175 6.175 0 01-3.712-1.232 9.091 9.091 0 011.456-1.776 8.524 8.524 0 013.24-1.968 31.452 31.452 0 011.232 4.568 6.1 6.1 0 01-2.216.408m-6.24-6.24v-.088c.176.008.408.008.68.008h.008a19.8 19.8 0 005.664-.808c.12.264.24.536.36.824a10.014 10.014 0 00-3.52 2.064 11.747 11.747 0 00-1.72 2 6.163 6.163 0 01-1.472-4m3.48-5.6a19.441 19.441 0 012.232 3.4 17.753 17.753 0 01-4.928.7h-.1c-.192 0-.36 0-.5-.008a6.241 6.241 0 013.296-4.092m2.76-.64a6.181 6.181 0 013.888 1.368 8.172 8.172 0 01-3.064 2.192 23.727 23.727 0 00-2.152-3.42 6.62 6.62 0 011.328-.14m4.9 2.384a6.259 6.259 0 011.332 3.556 12.257 12.257 0 00-2.552-.28h-.008a10.8 10.8 0 00-1.808.152c-.136-.336-.264-.656-.416-.968a9.722 9.722 0 003.452-2.46M1218 843a8 8 0 108 8 8 8 0 00-8-8z" /></g></svg> Dribbble</a></li>' : '' ) .
					( ( $socials_behance_url = get_theme_mod( 'socials_behance_url' ) ) ? '
							<li class="behance-hover-color"><a href="' . $socials_behance_url . '" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40"><g transform="translate(-1250 -831)"><path data-name="behance" d="M1276.064 851.416a1.553 1.553 0 00-.5-1.128 1.628 1.628 0 00-1.1-.384 1.474 1.474 0 00-1.112.408 1.913 1.913 0 00-.5 1.1m5.08-.184a8.522 8.522 0 01.068 1.42h-5.2a1.724 1.724 0 00.752 1.5 1.838 1.838 0 001.032.272 1.505 1.505 0 001.016-.328 1.4 1.4 0 00.4-.5h1.9a2.263 2.263 0 01-.7 1.3 3.381 3.381 0 01-2.672 1.04 3.969 3.969 0 01-2.528-.876 3.516 3.516 0 01-1.072-2.856 3.816 3.816 0 01.984-2.84 3.423 3.423 0 012.552-.992 4.027 4.027 0 011.672.336 2.845 2.845 0 011.232 1.056 3.458 3.458 0 01.568 1.472m-9.872 1.624a1.08 1.08 0 00-.632-1.072 2.968 2.968 0 00-1-.184h-2.132v2.672h2.1a2.3 2.3 0 001.008-.176 1.248 1.248 0 00.656-1.24m-3.764-2.888h2.1a1.947 1.947 0 001.056-.248.927.927 0 00.4-.872.911.911 0 00-.528-.92 3.782 3.782 0 00-1.176-.152h-1.852m5.48 3.76a2.651 2.651 0 01.38 1.454 3.026 3.026 0 01-.44 1.592 2.711 2.711 0 01-1.832 1.28 6.985 6.985 0 01-1.4.136H1262v-9.958h4.8a3.022 3.022 0 012.584 1.064 2.534 2.534 0 01.456 1.52 2.226 2.226 0 01-.456 1.466 2.448 2.448 0 01-.76.568 2.265 2.265 0 011.152.88m6.672-4.08h-4.008v-1h4.008z" /></g></svg> Behance</a></li>' : '' ) .
					( ( $socials_pinterest_url = get_theme_mod( 'socials_pinterest_url' ) ) ? '
							<li class="pinterest-hover-color"><a href="' . $socials_pinterest_url . '" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 511.977 511.977" width="16" height="16"><path d="M262.948 0C122.628 0 48.004 89.92 48.004 187.968c0 45.472 25.408 102.176 66.08 120.16 6.176 2.784 9.536 1.6 10.912-4.128 1.216-4.352 6.56-25.312 9.152-35.2.8-3.168.384-5.92-2.176-8.896-13.504-15.616-24.224-44.064-24.224-70.752 0-68.384 54.368-134.784 146.88-134.784 80 0 135.968 51.968 135.968 126.304 0 84-44.448 142.112-102.208 142.112-31.968 0-55.776-25.088-48.224-56.128 9.12-36.96 27.008-76.704 27.008-103.36 0-23.904-13.504-43.68-41.088-43.68-32.544 0-58.944 32.224-58.944 75.488 0 27.488 9.728 46.048 9.728 46.048S144.676 371.2 138.692 395.488c-10.112 41.12 1.376 107.712 2.368 113.44.608 3.168 4.16 4.16 6.144 1.568 3.168-4.16 42.08-59.68 52.992-99.808 3.968-14.624 20.256-73.92 20.256-73.92 10.72 19.36 41.664 35.584 74.624 35.584 98.048 0 168.896-86.176 168.896-193.12C463.62 76.704 375.876 0 262.948 0" /></svg> Pinterest</a></li>' : '' ) .
				'</ul>
			</div>
			';
		}
	}
}
