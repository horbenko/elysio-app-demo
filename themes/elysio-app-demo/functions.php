<?php
/**
 * Elysio functions and definitions
 *
 * @package elysio-app
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$elysio_includes = array(
	'/theme-settings.php',                  // Initialize theme default settings.
	'/setup.php',                           // Theme setup and custom theme supports.
	'/widgets.php',                         // Register widget area.
	'/enqueue.php',                         // Enqueue scripts and styles.
	'/template-tags.php',                   // Custom template tags for this theme.
	'/pagination.php',                      // Custom pagination for this theme.
	'/hooks.php',                           // Custom hooks.
	'/extras.php',                          // Custom functions that act independently of the theme templates.
	'/customizer.php',                      // Customizer additions.
	'/customizer/colors.php',          		// Customizer Colors.
	'/customizer/site-header.php',          // Customizer Header.
	'/customizer/site-footer.php',          // Customizer Footer.
	'/search.php',                      	// AJAX Search for site header search popup
	'/custom-comments.php',                 // Custom Comments file.
	'/jetpack.php',                         // Load Jetpack compatibility file.
	'/lib/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker. 
	'/editor.php',                          // Load Editor functions.
	'/lib/tgm.php'							// TGM Plugin
);

foreach ( $elysio_includes as $file ) {
	require_once get_template_directory() . '/inc' . $file;
}




add_action('get_header', 'remove_admin_bar_weird_css');
function remove_admin_bar_weird_css() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}

// https://make.wordpress.org/themes/2019/03/29/addition-of-new-wp_body_open-hook/
if ( ! function_exists( 'wp_body_open' ) ) {
    function wp_body_open() {
        do_action( 'wp_body_open' );
    }
}





// Merlin WP
// require_once get_parent_theme_file_path( '/inc/lib/merlin/vendor/autoload.php' );
// require_once get_parent_theme_file_path( '/inc/lib/merlin/class-merlin.php' );
// require_once get_parent_theme_file_path( '/inc/lib/merlin-config.php' );


// remove_filter( 'wp_import_post_meta', 'Elementor\Compatibility::on_wp_import_post_meta');
// remove_filter( 'wxr_importer.pre_process.post_meta', 'Elementor\Compatibility::on_wxr_importer_pre_process_post_meta');


function ocdi_import_files() {
  return array(
    array(
      'import_file_name'             => 'Demo Import 1',
      'categories'                   => array( 'Category 1', 'Category 2' ),
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/demo/elysioapp1.xml',
      'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/demo/app1.elysio.top-widgets.wie',
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'inc/demo/elysioapp1-demo-export.dat',
      'import_preview_image_url'     => 'http://www.your_domain.com/ocdi/preview_import_image1.jpg',
      'import_notice'                => __( 'After you import this demo, you will have to setup the slider separately.', 'your-textdomain' ),
      'preview_url'                  => 'https://app1.elysio.top',
    ),
    array(
      'import_file_name'             => 'Demo Import 2',
      'categories'                   => array( 'New category', 'Old category' ),
      'local_import_file'            => trailingslashit( get_template_directory() ) . 'ocdi/demo-content2.xml',
      'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'ocdi/widgets2.json',
      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'ocdi/customizer2.dat',
      'local_import_redux'           => array(
        array(
          'file_path'   => trailingslashit( get_template_directory() ) . 'ocdi/redux.json',
          'option_name' => 'redux_option_name',
        ),
        array(
          'file_path'   => trailingslashit( get_template_directory() ) . 'ocdi/redux2.json',
          'option_name' => 'redux_option_name_2',
        ),
      ),
      'import_preview_image_url'     => 'http://www.your_domain.com/ocdi/preview_import_image2.jpg',
      'import_notice'                => __( 'A special note for this import.', 'your-textdomain' ),
      'preview_url'                  => 'http://www.your_domain.com/my-demo-2',
    ),
  );
}
add_filter( 'pt-ocdi/import_files', 'ocdi_import_files' );