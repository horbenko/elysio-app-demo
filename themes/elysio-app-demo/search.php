<?php
/**
 * The template for displaying search results pages
 *
 * @package elysio-app
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'elysio_container_type' );

?>

<div class="wrapper" id="search-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<div class="col-md content-area" id="primary">


				<main class="site-main" id="main">

					<?php if ( have_posts() ) : ?>

						<header class="page-header text-center">

							<h1 class="page-title">
							<?php
							printf(
								/* translators: %s: query term */
								esc_html__( 'Search Results for: %s', 'elysio' ),
								'<span>' . get_search_query() . '</span>'
							);
							?>
							</h1>

							<?php
							get_search_form();
							?>

						</header><!-- .page-header -->

						<div class="elysio-blog-list">
							<?php /* Start the Loop */ ?>
							<?php
							while ( have_posts() ) :
								the_post();

								/*
								 * Run the loop for the search to output the results.
								 * If you want to overload this in a child theme then include a file
								 * called content-search.php and that will be used instead.
								 */

								//get_template_part( 'loop-templates/content', 'search' );
								get_template_part( 'loop-templates/content', 'recent' );

							endwhile;
							?>
						</div>

					<?php else : ?>

						<?php get_template_part( 'loop-templates/content', 'none' ); ?>

					<?php endif; ?>

				</main><!-- #main -->

				<!-- The pagination component -->
				<?php elysio_pagination(); ?>
				
			</div>

		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- #search-wrapper -->

<?php
get_footer();
